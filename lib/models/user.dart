//class representing user
class User{
  String name;
  String email;
  String uuid;
  String token;
  String pic;
  String contact;
  String description;
  String password;
  String dob;
  List<dynamic> groups;

  User({this.groups,this.dob,this.name,this.email,this.uuid,this.token,this.pic,this.contact,this.description,this.password});

  Map<String,dynamic> toJson() => {
    "name" : this.name,
    "email" : this.email,
    "uuid" : this.uuid,
    "token" : this.token,
    "pic" : this.pic,
    "contact" : this.contact,
    "description" : this.description,
    "password" : this.password,
    "dob":this.dob,
    "groups":this.groups
  };

  User.fromJson(Map<String,dynamic> map):
        name = map["name"], email = map["email"],
        uuid = map["uuid"], token = map["token"],
        pic = map["pic"], contact = map["contact"],
        description = map["description"],password = map["password"],
        dob = map["dob"],groups = map["groups"];

}