
import 'package:senti/blocs/signup_bloc/signup_page_state.dart';
import 'package:validate/validate.dart';

String validateEmail(String value) {
  // If empty value, the isEmail function throw a error.
  // So I changed this function with try and catch.
  try {
    Validate.isEmail(value);
  } catch (e) {
    return 'The E-mail Address must be a valid email address.';
  }

  if(value.trim() != ""){
    return null;
  }else{
    return 'The E-mail cannot be empty.';
  }

}

String validatePassword(String value) {
  if (value.length < 8) {
    return 'The Password must be at least 8 characters.';
  }

  return null;
}


String validateName(String name){
  if(name.trim().isEmpty){
    return "Cannot be empty !";
  }
  return null;
}


String validatePasswordRepeat(String passRepeat,String pass){
  if(passRepeat !=  pass){
    return "Passwords do not match !!";
  }
  return null;
}

String validateContact(String contact){
  if(contact.trim() == ""){
    return "Contact cannot be empty";
  }

  if (contact.length != 9 || contact[0] != "7") {
    return 'Enter valid phone number';
  }

  return null;
  return null;
}

String validateNumber(String str){
  if(str.trim() == "") return "Cannot be empty";
  double amount = 0;
  try{
    amount = double.parse(str);
  }catch(e){
    return "Enter a valid number";
  }
  return null;
}