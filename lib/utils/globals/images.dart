import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:senti/utils/size_config.dart';

Future<File> getImage(String src) async{
  File image;

  if(src == "camera"){
    if(await checkAndRequestCameraPermissions()){
      image = await ImagePicker.pickImage(source: ImageSource.camera);
    }
  }else{
    //check for necessary permissions
    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.mediaLibrary);
    if(permission.value == PermissionStatus.granted.value){
      image = await ImagePicker.pickImage(source: ImageSource.gallery);
    }
  }

  return image;
}

//choose image source
Future<String> chooseImageSource(BuildContext context) async{
  String choice ;
  await showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title:  Text("Select Source of image"),
        content: Container(
          child: Center(
            heightFactor: 1,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children:  <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child:  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.camera),
                      SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                      Text("Camera")
                    ],
                  ),
                  onPressed: () {
                    choice = "camera";
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child:  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.image),
                      SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                      Text("Gallery")
                    ],
                  ),
                  onPressed: () {
                    choice = "gallery";
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        ),
      );
    },
  );

  return choice;
}

Future<bool> checkAndRequestCameraPermissions() async {
  PermissionStatus permission =
  await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);
  if (permission != PermissionStatus.granted) {
    Map<PermissionGroup, PermissionStatus> permissions =
    await PermissionHandler().requestPermissions([PermissionGroup.camera]);
    return permissions[PermissionGroup.camera] == PermissionStatus.granted;
  } else {
    return true;
  }
}
