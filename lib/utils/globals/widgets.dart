
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';

Widget buildLoader(context){
  return Container(
    width: SizeConfig.blockSizeHorizontal * 100,
    height: SizeConfig.blockSizeVertical * 100,
    color: Colors.grey.withOpacity(0.1),
    child: Center(
      child: CircularProgressIndicator(valueColor:AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor)),
    ),
  );
}

Widget joinGroupFirst(BuildContext context){
  return buildGroupWidget(context, "Join Group first");
}


Widget buildGroupNotActive(BuildContext context){
  return buildGroupWidget(context, "Group not activated");
}

Widget buildGroupWidget(BuildContext context,String title){
  return Scaffold(
    backgroundColor: Colors.white,
    body: Container(
      color: Colors.white,
      width: SizeConfig.blockSizeHorizontal * 100,
      height: SizeConfig.blockSizeVertical * 100,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.group,color: Theme.of(context).primaryColor,size: SizeConfig.blockSizeHorizontal * 30,),
            Text(title,style: TextStyle(color: Theme.of(context).primaryColor),)
          ],
        ),
      ),
    ),
  );
}