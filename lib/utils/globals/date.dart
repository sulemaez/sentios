//get month from int
final List<String> months = [
  "January","February","March","April","May","June","July",
  "August","September","October","November","December"
];

String getMonthFromInt(int index){
  return months[index-1];
}

final Map days = {
  "short":[
    "MON","TUE","WED","THURS","FRI","SAT","SUN"
  ],
  "long":[
    "Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"
  ]
};

//get week day
String getWeekDay(int day,{short = true}){
  if(short)return days["short"][day-1];
  return days["lonng"][day-1];
}