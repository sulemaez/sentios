import 'package:senti/pages/base_page.dart';
import 'package:senti/services/auth.dart';

//is group admin
bool isGroupAdmin(group,uuid){
  return group["group_admin"]["uuid"] == uuid ? true : false;
}

//State to hold tmp group details on create
Map<String,dynamic> groupBeingCreated = initializeGroupCreated();

Map<String,dynamic> initializeGroupCreated(){
  return {
    "name":"",
    "purpose":"",
    "location":"",
    "targetAmount":"",
    "minimumContribution":"",
    "loanTerms":<Map<String,dynamic>>[],
    "penalties":<Map<String,dynamic>>[],
    "constitution":"",
    "admin":{
      "name":Auth.user.name,
      "email":Auth.user.email,
      "contact":Auth.user.contact,
      "pic":Auth.user.pic,
      "description":Auth.user.description
    },
    "members":"0"
  };
}

//home base page
HomeBaseState homeBaseState;