

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';

void showCustomAlertDialog(BuildContext context,{String message}){
  //success message
  showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          content: Container(
            child: Center(
              heightFactor: 1,
              child:Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(message ?? "",style: TextStyle(color: Theme.of(context).primaryColor,),textAlign: TextAlign.center,),
                  SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                  MaterialButton(
                    minWidth: SizeConfig.blockSizeHorizontal * 20,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                    color: Theme.of(context).primaryColor,
                    child: Text(
                      "ok",
                      style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                    ),
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ),
            ),
          ),
        );
      }
  );
}

Future<bool> showCustomConfirmDialog(BuildContext contxt,{String title,String okMessage,String cancelMessage}) async{
  bool ans = false;

  await showDialog(
      context: contxt,
      builder: (BuildContext context){
    return AlertDialog(
      title: title != null ? Text(title,style: TextStyle(color: Theme.of(context).primaryColor),textAlign: TextAlign.center,) : null,
      content: Container(
        child: Center(
          heightFactor: 1,
          child:Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              MaterialButton(
                minWidth: SizeConfig.blockSizeHorizontal * 20,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                color: Colors.redAccent,
                child: Text(
                  cancelMessage,
                  style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                ),
                onPressed: (){
                  Navigator.of(context).pop();
                },
              ),
              SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
              MaterialButton(
                minWidth: SizeConfig.blockSizeHorizontal * 20,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                color: Theme.of(context).primaryColor,
                child: Text(
                  okMessage,
                  style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                ),
                onPressed: (){
                 ans = true;
                  Navigator.of(context).pop();
                },
              )
            ],
          ),
        ),
      ),
    );
  });
  return ans;
}