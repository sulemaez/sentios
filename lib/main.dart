import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:senti/pages/auth/splash_screen.dart';
import 'package:senti/pages/base_page.dart';

void main(){
  //force app to always be potrait
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]).then((_){
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Senti',
      theme: ThemeData(
       //TODO fontFamily: '',
        primarySwatch: Colors.blue,
        primaryColor: Color(0xff610083),
        secondaryHeaderColor:Color(0xffa580b9),
        backgroundColor: Colors.white,
      ),
      //start application with splash screen
      home: SplashScreen(),
      routes: {
           '/home' : (context) => HomeBase()
      },
    );
  }
}