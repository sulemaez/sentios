
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'auth.dart';

class Http{
  static Dio _dio = Dio();


  static Future<Response> doPost({data,url,}) async{

   return await _dio.post(url, data: data ,options: Options(
        contentType: Headers.jsonContentType,
        responseType: ResponseType.json,
        followRedirects: false,
        validateStatus: (status) { return status < 500; },
        headers: {
          'Content-type':'application/json',
          'Accept': 'application/json',
          'Authorization' : Auth.user == null ? '': "Bearer ${Auth.user.token}"
        }
    ));
  }

  static Future<Response> doGet(url) async{
    return _dio.get(url,options: Options(
        responseType: ResponseType.json,
        followRedirects: false,
        validateStatus: (status) { return status < 500; },
        headers: {
          'Content-type':'application/json',
          'Accept': 'application/json',
          'Authorization' : Auth.user == null ? '': "Bearer ${Auth.user.token}"
        }
    ));
  }

  static doPut(data,url) async{
   return await _dio.put(url, data: data,options: Options(
        contentType: Headers.jsonContentType,
        responseType: ResponseType.json,
        followRedirects: false,
        validateStatus: (status) { return status < 500; },
        headers: {
          'Content-type':'application/json',
          'Accept': 'application/json',
          'Authorization' : Auth.user == null ? '': "Bearer ${Auth.user.token}"
        }
    ));
  }
}

String apiUrl = "https://sentidev.zoopio.co.ke/api/v1";
String baseUrl = "https://sentidev.zoopio.co.ke/";

//checks the internet
Future<bool> checkNet() async{
  print("LOOlup");
  try{
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    print("dome not");
  }catch(e){
    print("ERROR $e");
  }

  Fluttertoast.showToast(
      msg: "There is not internet access !",
      textColor: Colors.white,
      backgroundColor: Colors.black
  );
  return false;
}
