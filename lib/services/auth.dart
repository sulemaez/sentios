import 'dart:convert';
import 'dart:core' ;
import 'package:dio/dio.dart';
import 'package:senti/blocs/signup_bloc/signup_page_state.dart';
import 'package:senti/models/user.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/shared_preferences.dart';

class Auth{
  static User user;
  static int activeGroupIndex = 0;

  //login method
  static Future<Map<String,dynamic>> login(String email,String password) async{
    try{

      if(! await checkNet())return { "internet" : false};

      Response resp =  await Http.doPost( data : {
        'email' : email,
        'password':password
      },url:"$apiUrl/auth/login");

      if(!(resp.data["message"] == "You are logged In")) return { "internet" : true , "logged" : false};

      //create an user
      user = User(
          email: email,
          uuid: resp.data['user']['uuid'],
          name: resp.data['user']['name'],
          contact: resp.data['user']['profile']['contacts'][0]['contact'],
          description: resp.data['user']['profile']['description'],
          pic: "$baseUrl/${resp.data['user']['profile']['profile_picture']}",
          token: resp.data['senti_token'],
          groups:resp.data['user']['groups'],
          dob : resp.data['user']['profile']['dob']
      );

      //save user details in shared preferences
      setPreference(jsonEncode(user));

      return { "internet" : true , "logged" : true};
    }catch(e){
      print(e);
      return { "internet" : true , "logged" : false};
    }
  }

  static Future<Map<String,dynamic>>  signUp(SignUpPageState state,{bool registerMember = false,group}) async{
    try{
      print("Register");
      //check if there is internet connection
      if(! await checkNet())return {'created':false,'internet':false};

      print("OLO");

      //crate from data containing all required fields
      FormData formData = new FormData.fromMap({
        "name" : state.name,
        "email":state.email,
        "password":state.password,
        "password_confirmation":state.confirmPassword,
        "dob" : "${state.yearValue}/${state.monthValue}/${state.dayValue}",
        "gender" : state.gender == 0 ? "male" : "female",
        "profile_picture" :  await MultipartFile.fromFile(state.picImage.path,filename: "profilePic.jpg"),
        "id_image_front" :  await MultipartFile.fromFile(state.frontImage.path,filename: "frontPic.jpg"),
        "id_image_back" :  await MultipartFile.fromFile(state.backImage.path,filename: "baackPic.jpg"),
        "phone_number" :"254${state.contact}",
        "group_uuid" : user == null ? "": user.groups[activeGroupIndex]["uuid"],
        "uuid" : user == null ? "" : user.uuid,
      });

      print(formData.fields);


      //send url encoded
      //TODO finish up add user
      Dio _dio = Dio();
      String url = registerMember ?"$apiUrl/group/members/user/create" :"$apiUrl/auth/register" ;

      Response resp = await _dio.post( url, data: formData ,options: Options(
          responseType: ResponseType.json,
          followRedirects: false,
          validateStatus: (status) { return status < 500; },
          headers: {
            'Accept': 'application/json',
            'Authorization': user != null ? 'Bearer ${user.token}' : ""
          }
      ));

      //check if the status code returned is success
      if(resp.statusCode == 200 || resp.statusCode == 201){
        return {'created':true};
      }

      //else if request not successful return the errors
      return {'created':false, "data" : resp.data};
    }catch(e){
      print("$e => signup auth");
    }
    return {'created':false};
  }

  static Future<bool> logout() async{
     try{
      Dio dio = new Dio();
      if(!await checkNet())return false;

      Response response =  await dio.post("$apiUrl/auth/logout", data:{
         "uuid" :user.uuid
       },options: Options(
           responseType: ResponseType.json,
           followRedirects: false,
           validateStatus: (status) { return status < 500; },
           headers: {
             'Accept': 'application/json',
             'Authorization': "Bearer ${user.token}"
           }
       ));

      //check for errors
     if(!(response.statusCode == 200)){
       return false;
     }
      //clear the shared preferences
      removePreference("user");
     return true;
     }catch(e){
       print(e);
     }
     return false;
  }

  static Future<Map<String,dynamic>> createUser(SignUpPageState state) {

  }


}

