import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/alert_dialog.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';

class JoinRequestsPage extends StatefulWidget {
  final String groupUuid;

  JoinRequestsPage({this.groupUuid});

  @override
  _JoinRequestsPageState createState() => _JoinRequestsPageState();
}

class _JoinRequestsPageState extends State<JoinRequestsPage> {

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.blockSizeVertical * 100,
      color: Colors.white,
      padding:EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3) ,
      child:FutureBuilder(
        builder: (BuildContext context,snapshot){
          if (snapshot.connectionState == ConnectionState.none &&
              snapshot.hasData == null || snapshot.data == null) {
            return buildLoader(context);
          }

          if(snapshot.data.length < 1){
             return Center(
               child: Column(
                  children: <Widget>[
                    SizedBox(height: SizeConfig.blockSizeVertical * 20,),
                    IconButton(
                      icon: Icon(Icons.refresh,color: Theme.of(context).primaryColor,size: SizeConfig.safeBlockHorizontal * 15,),
                      onPressed: (){setState(() {});},
                    ),
                    SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                    Text("No Requests currently !",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.safeBlockHorizontal * 4),)
                  ],
               ),
             );
          }

          return ListView.builder(
            shrinkWrap: true,
            itemBuilder: (context,index){
              return _buildListItem(snapshot.data[index]);
            },
            itemCount: snapshot.data.length,
          ) ;
        },
        future:  _getRequest(),
      ),
    );
  }

  Future<List> _getRequest() async{
    try{
      Response response = await Http.doGet("$apiUrl/group/join/requests/${widget.groupUuid}");
      if(response.statusCode == 200)return response.data["data"];
    }catch(e){
       print(e);
    }
    return [];
  }

  Widget _buildListItem(data) {

     return Column(
        children: <Widget>[
          Container(
            width: SizeConfig.blockSizeHorizontal * 100,
            padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 1,bottom: SizeConfig.blockSizeVertical * 1),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CircleAvatar(backgroundImage:Image.network(
                      "$baseUrl/${data["user_profile"]["profile_picture"]}",
                    ).image,radius: SizeConfig.blockSizeHorizontal * 5,),
                    SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("${data["name"]}",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 5),),
                        Text("${data["user_profile"]["contacts"][0]["contact"]}",style: TextStyle(color: Colors.grey,)),
                        Text("${data["email"]}",style: TextStyle(color: Colors.grey))
                      ],
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    MaterialButton(
                      minWidth: SizeConfig.blockSizeHorizontal * 20,
                      height: SizeConfig.blockSizeVertical * 4,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                      color: Color.fromARGB(255, 	0, 154, 0),
                      child: Text("ACCEPT", style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                      onPressed: (){
                        _approveRequest(data["uuid"],data["name"]);
                      },
                    ),
                    SizedBox(width: SizeConfig.blockSizeHorizontal * 3,),
                    MaterialButton(
                      minWidth: SizeConfig.blockSizeHorizontal * 20,
                      height: SizeConfig.blockSizeVertical * 4,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                      color: Color.fromARGB(255, 	190, 0, 0),
                      child: Text("DECLINE", style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                      onPressed: (){
                          _declineRequest(data["uuid"],data["name"]);
                      },
                    )
                  ],
                )
              ],
            ),
          ),
          Divider()
        ],
     );
  }


  //action done when approve button clicked
  void  _approveRequest(String userUid,String name) async{
    bool res = await showCustomConfirmDialog(context, title: "Accept request by $name",okMessage: "Yes",cancelMessage: "No");

    if(res){
      Response response = await Http.doPost(data: {
        "user_uuid":userUid,
        "group_uuid":widget.groupUuid
      },url: "$apiUrl/group/join/request/approve");

      //reload state
      if(response.statusCode == 200 || response.statusCode == 201){
        setState(() {});
      }

    }
  }

  //actio done when declined
  void  _declineRequest(String userUuid,String name) async{
    bool res = await showCustomConfirmDialog(context, title: "Decline request by $name",okMessage: "Yes",cancelMessage: "No");
    if(res){
      Response response = await Http.doPost(data: {
        "user_uuid":userUuid,
        "group_uuid":widget.groupUuid
      },url: "$apiUrl/group/join/request/reject");

      if(response.statusCode == 200 || response.statusCode == 201){
        showCustomAlertDialog(context,message: "$name's request has been declined ");
        setState(() {});
      }
    }
  }



}
