import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/text/text_list.dart';

class LeaveGroupPage extends StatefulWidget {
  @override
  State createState() => LeaveGroupPageState();
}

class LeaveGroupPageState extends State<LeaveGroupPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyBar("Leave Group",context),
      body: Padding(
         padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 5),
         child: Column(
           crossAxisAlignment: CrossAxisAlignment.start,
           children: <Widget>[
              AutoSizeText("There are a few formalities one has to match before leaving the group.",style: TextStyle(color: Colors.black54),),
              ListText(list:["You must have no outstanding loan","You will forfeit 20% of the total contribution"],textColor: Colors.black54,),
              SizedBox(height: SizeConfig.blockSizeVertical * 2,),
              AutoSizeText(
                  "Turpis egestas integer eget aliquet nibh praesent tristique. Augue interdum velit euismod in pellentesque massa placerat. Nibh tellus molestie nunc non blandit massa enim nec dui. Diam vulputate ut pharetra sit amet aliquam id diam. Mauris rhoncus aenean vel elit scelerisque. Pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus. Egestas pretium aenean pharetra magna ac. Dapibus ultrices in iaculis nunc sed augue lacus. Ornare arcu odio ut sem. Arcu bibendum at varius vel pharetra. Quam pellentesque nec nam aliquam sem et tortor consequat id.",
                  style: TextStyle(color: Colors.black54,fontSize:SizeConfig.blockSizeHorizontal * 4 ),
              ),
             SizedBox(height: SizeConfig.blockSizeVertical * 10,),
             Center(
               child: MaterialButton(
                 minWidth: SizeConfig.blockSizeHorizontal * 50,
                 height: SizeConfig.blockSizeVertical * 6,
                 shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                 color: Theme.of(context).primaryColor,
                 child: Text(
                   "LEAVE GROUP",
                   style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                 ),
                 onPressed: (){

                 },
               ),
             )
           ],
         ),
      )
    );
  }
}
