
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/display/circle_rectangular_label.dart';
import 'package:senti/widgets/text/text_list.dart';

class GroupPreviewPage extends StatelessWidget {

  final bool isHome;

  const GroupPreviewPage({Key key, this.isHome = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Preview Group",context),
      body: Container(
        child: ListView(
          shrinkWrap: true,
          children: _buildPage(context),
        ),
      ),
    );
  }

  List<Widget> _buildPage(BuildContext context) {
    List<Widget> finalList = [];

    ////build first row
    List<Widget> tmpList = [];
    //group name
    tmpList.addAll([
      Text(groupBeingCreated["name"],style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5,color: Colors.black54),),
      SizedBox(height: SizeConfig.blockSizeVertical * 1,)
    ]);
    //row
    Card targetCard = Card(
      elevation: 3,
      child: Container(
        width: SizeConfig.blockSizeHorizontal * 40,
        padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
        child: Center(
          child: Stack(
            children: <Widget>[
              Image.asset("assets/target_eye.png",color: Theme.of(context).primaryColor.withOpacity(.1),width: SizeConfig.blockSizeHorizontal * 40,height: SizeConfig.blockSizeHorizontal * 20,),
              Align(
                alignment:Alignment.center,
                child:  Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //target amount
                    Text(groupBeingCreated["targetAmount"],style: TextStyle(color: Theme.of(context).primaryColor,fontWeight: FontWeight.bold,fontSize: SizeConfig.blockSizeHorizontal * 5),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 3,),
                    Text("Saving Target")
                  ],
                ),
              )
            ],
          ),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(6))
        ),
      ),
    );

    Column infoColumn = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        CircleThenRectangularLabel(
          circleColor: Color.fromARGB(255, 121, 102, 254),
          circleValue: groupBeingCreated["members"],
          rectangleColor:  Color.fromARGB(255,237,236,255),
          rectangleValue: "Members",
        ),

        CircleThenRectangularLabel(
          circleColor: Color.fromARGB(255, 121, 102, 254),
          circleValue: groupBeingCreated["minimumContribution"],
          rectangleColor:  Color.fromARGB(255,237,236,255),
          rectangleValue: "Contribution",
        )
      ],
    );

    //create loan term string
    String loanTermString = "";
    if(groupBeingCreated["loanTerms"] != null)groupBeingCreated["loanTerms"].forEach((map){
       loanTermString += "${map["months"]},";
    });


    tmpList.addAll([Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        targetCard,
        infoColumn,
      ],
     ),
      SizedBox(height: SizeConfig.blockSizeVertical * 2,),
      Divider(color: Colors.grey,thickness: 0.3,),
      SizedBox(height: SizeConfig.blockSizeVertical * 2,),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Location :",style: TextStyle(color : Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 4),),
          Text(groupBeingCreated["location"],style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4),)
        ],
      ),
      SizedBox(height: SizeConfig.blockSizeVertical * 1,),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Loan Terms (Months):",style: TextStyle(color : Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 4),),
          AutoSizeText(loanTermString,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4),)
        ],
      ),
      SizedBox(height: SizeConfig.blockSizeVertical * 2,),
    ]);

    //add first main row to final list
    finalList.add(Padding(
      padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 4),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: tmpList,
      ),
    ));

    //penalties setup
    finalList.addAll([
       Container(
         width: SizeConfig.blockSizeHorizontal * 100,
         padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 4 ,top: SizeConfig.blockSizeVertical * 1,bottom: SizeConfig.blockSizeVertical * 1 ),
         child: Text("Penalties Set Up",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 5),),
         color: Color.fromARGB(255, 244, 244, 244),
       ),
       Padding(
         padding: EdgeInsets.only(right:SizeConfig.blockSizeHorizontal * 4,left:SizeConfig.blockSizeHorizontal * 4),
         child: Column(
           children: _setupPenalties(),
         ),
       ),
        SizedBox(height: SizeConfig.blockSizeVertical * 2,)
    ]);


    //loan interest setup
    //penalties setup
    finalList.addAll([
      Container(
        width: SizeConfig.blockSizeHorizontal * 100,
        padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 4 ,top: SizeConfig.blockSizeVertical * 1,bottom: SizeConfig.blockSizeVertical * 1 ),
        child: Text("Loan Terms",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 5),),
        color: Color.fromARGB(255, 244, 244, 244),
      ),
      Padding(
        padding: EdgeInsets.only(right:SizeConfig.blockSizeHorizontal * 4,left:SizeConfig.blockSizeHorizontal * 4),
        child: Column(
          children: _setupLoan(),
        ),
      ),
      SizedBox(height: SizeConfig.blockSizeVertical * 2,)
    ]);

    //constittution
    finalList.addAll([
      Container(
        color: Theme.of(context).primaryColor,
        width: SizeConfig.blockSizeHorizontal * 100,
        padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 5,top: SizeConfig.blockSizeVertical * 1,bottom: SizeConfig.blockSizeVertical * 1),
        child: Text("Group Consituition",style: TextStyle(color: Colors.white,fontSize: SizeConfig.blockSizeHorizontal * 5),),
      ),
      SizedBox(height: SizeConfig.blockSizeVertical * 2,),
      Padding(
        padding: EdgeInsets.only(right:SizeConfig.blockSizeHorizontal * 4,left:SizeConfig.blockSizeHorizontal * 4),
        child: AutoSizeText(
          groupBeingCreated["constitution"],
          style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4),
        ),
      ),
      SizedBox(height: SizeConfig.blockSizeVertical * 2,)
    ]);

    //group leader info
    finalList.addAll([
      Container(
        color: Theme.of(context).primaryColor,
        width: SizeConfig.blockSizeHorizontal * 100,
        padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 5,top: SizeConfig.blockSizeVertical * 1,bottom: SizeConfig.blockSizeVertical * 1),
        child: Text("Group Leader Info",style: TextStyle(color: Colors.white,fontSize: SizeConfig.blockSizeHorizontal * 5),),
      ),
      SizedBox(height: SizeConfig.blockSizeVertical * 2,),
      Padding(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image(
                  image: Image.network("${groupBeingCreated["admin"]["pic"]}").image,
                  width: SizeConfig.blockSizeHorizontal * 35,
                  height: SizeConfig.blockSizeHorizontal * 35,
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("${groupBeingCreated["admin"]["name"]}",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 5),),
                      SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                      Row(
                        children: <Widget>[
                          Icon(FontAwesomeIcons.envelope,color: Colors.grey,size: SizeConfig.blockSizeHorizontal * 4,),
                          SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
                          Text("${groupBeingCreated["admin"]["email"]}",style: TextStyle(color: Colors.grey))
                        ],
                      ),
                      SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                      Row(
                        children: <Widget>[
                          Icon(Icons.local_phone,color: Colors.grey,size: SizeConfig.blockSizeHorizontal * 4),
                          SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
                          Text("${groupBeingCreated["admin"]["contact"]}",style: TextStyle(color: Colors.grey))
                        ],
                      ),
                      SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                      Row(
                        children: <Widget>[
                          Icon(Icons.location_on,color: Colors.grey,size: SizeConfig.blockSizeHorizontal * 4),
                          SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
                          Text("${groupBeingCreated["location"]}",style: TextStyle(color: Colors.grey),)
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: SizeConfig.blockSizeVertical * 1,),
            AutoSizeText(
              "${groupBeingCreated["admin"]["description"]}",
              style: TextStyle(color: Colors.grey,height: 1.5
              ),
            )
          ],
        ),
        padding:  EdgeInsets.only(right:SizeConfig.blockSizeHorizontal * 4,left:SizeConfig.blockSizeHorizontal * 4),)
    ]);

    //button
   finalList.addAll(
      [
        SizedBox(height: SizeConfig.blockSizeVertical * 2,),
        !isHome ? Align(
          alignment: Alignment.center,
          child:  MaterialButton(
            minWidth: SizeConfig.blockSizeHorizontal * 40,
            height: SizeConfig.blockSizeVertical * 6,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
            color: Theme.of(context).primaryColor,
            child: Text(
              "Create Group",
              style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
            ),
            onPressed: (){
                Navigator.of(context).pop(true);
            },
          ),
        ) : Text(""),
        SizedBox(height: SizeConfig.blockSizeVertical * 6,),
      ]
    );
    return finalList;
  }

  //creates the penalties list
  List<Widget> _setupPenalties(){

    List<String> penList = [];
    List<Widget> valueList = [];
    print(groupBeingCreated["penalties"]);
    groupBeingCreated["penalties"].forEach((map){
      penList.add(map.keys.first);
      valueList.add(Text("${map[map.keys.first]}",style: TextStyle(fontWeight: FontWeight.bold),));
    });

    List<Widget> list = [
      ListText(list: penList,
        textColor: Colors.grey,
        extras: valueList,
      ),
    ];
    return list;
  }

  //creates the penalties list
  List<Widget> _setupLoan(){

    List<String> penList = [];
    List<Widget> valueList = [];
    groupBeingCreated["loanTerms"].forEach((map){
      penList.add("${map["months"]} Months");
      valueList.add(Text("${map["interest"]}%",style: TextStyle(fontWeight: FontWeight.bold,fontSize: SizeConfig.blockSizeHorizontal * 5),));
    });

    List<Widget> list = [
      ListText(list: penList,
        textColor: Colors.grey,
        extras: valueList,
      ),
    ];
    return list;
  }

}
