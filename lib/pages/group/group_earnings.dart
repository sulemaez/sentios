import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/functional/InfoTableWidget.dart';
import 'package:senti/widgets/my_info_bar.dart';

class GroupEarnings extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: <Widget>[
            MyInfoBar( Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("\$6506",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 6,fontWeight: FontWeight.bold),)
                ,SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                Text("Total Earnings")
              ],
            ), context),
            Expanded(
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  _buildTableOne(),
                  Padding(
                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                    child: Text("Total Income : \$6400",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold,fontSize: SizeConfig.blockSizeHorizontal * 5),),
                  ),
                  SizedBox(height: SizeConfig.blockSizeVertical * 3,),
                  _buildTableTwo(),
                  Padding(
                    padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                    child: Text("Total Penalty : \$6400",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold,fontSize: SizeConfig.blockSizeHorizontal * 5),),
                  ),
                ],
              )
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTableOne(){
    return InfoTableWidget(
      headings: ["Sr No.","No. of Loans","Rate of Interest","Income"],
      data: [
        {
          "No. of Loans":"1 Month",
          "Rate of Interest":"4",
          "Income":"\$1500"
        },
        {
          "No. of Loans":"1 Month",
          "Rate of Interest":"4",
          "Income":"\$1500"
        }
      ],
      columnWidths: {0:FractionColumnWidth(.2)},
    );
  }

  Widget _buildTableTwo(){
    return InfoTableWidget(
      headings: ["Sr No.","Penalty","Charges","Income"],
      data: [
        {
          "Penalty":"Late Contribtion Penalty",
          "Charges":"\$5.00",
          "Income":"\$450"
        },
      ],
      columnWidths: {0:FractionColumnWidth(.15),2:FractionColumnWidth(.2),3:FractionColumnWidth(.2)},
    );
  }
}
