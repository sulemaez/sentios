
import 'package:flutter/material.dart';
import 'package:senti/pages/group/add_new_member.dart';
import 'package:senti/pages/group/join_request.dart';
import 'package:senti/utils/size_config.dart';

class AddNewMemberBase extends StatefulWidget {
  final String groupUid;

  AddNewMemberBase({this.groupUid});

  @override
  _AddNewMemberBaseState createState() => _AddNewMemberBaseState();
}

class _AddNewMemberBaseState extends State<AddNewMemberBase> with TickerProviderStateMixin{
  TabController _tabController ;

  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    _tabController = TabController(length: 2, vsync: this,initialIndex: 0);
    _tabController.addListener((){
      setState(() {
      });
    });

  }

  void _newMemberTap(){
    setState(() {
      _tabController.index = 0;
    });
  }

  void _joinRequestTap(){
    setState(() {
      _tabController.index = 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
           child: Container(
             color: Colors.white,
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.center,
               children: <Widget>[
                 Padding( padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 1),),
                 Align(
                   alignment: Alignment.topLeft,
                   child: IconButton(
                     icon: Icon(Icons.arrow_back_ios,color: Theme.of(context).primaryColor,),
                     onPressed: (){
                       Navigator.of(context).pop();
                     },
                   ),
                 ),
                 Padding( padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 1),),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   children: <Widget>[
                     GestureDetector(
                         child: Container(
                             child:  Text("Add Member",style: TextStyle(color: _tabController.index == 0 ? Theme.of(context).primaryColor : Theme.of(context).secondaryHeaderColor,fontSize: 20,fontWeight: FontWeight.bold),)
                         ),
                         onTap: _newMemberTap
                     ),
                     GestureDetector(
                         child:Container(
                             child:  Text("Join Requests",style: TextStyle(color: _tabController.index == 1 ? Theme.of(context).primaryColor : Theme.of(context).secondaryHeaderColor,fontSize: 20,fontWeight: FontWeight.bold),)
                         ),
                         onTap: _joinRequestTap
                     ),
                   ],
                 ),
                 Expanded(
                   child: TabBarView(
                       controller: _tabController,
                       children: [
                         AddNewMember(),
                         JoinRequestsPage(groupUuid: widget.groupUid,)
                       ]),
                 )
               ],
             ),
           ),
        )
    );
  }
}
