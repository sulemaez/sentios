
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/display/loan_info.dart';
import 'package:senti/widgets/display/personal_info.dart';

class GroupLoanDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: MyBar("Loan Detail",context),
      body: Container(
        width: SizeConfig.blockSizeHorizontal * 100,
        height:  SizeConfig.blockSizeVertical * 100,
        child: Padding(
          padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3,right: SizeConfig.blockSizeHorizontal * 3),
          child: ListView(
            children: <Widget>[
                  PersonalInfoWidget(),
                  SizedBox(height: SizeConfig.blockSizeVertical * 4,),
                  LoanInfoWidget()
              ],
          ),
        ),
      ),
    );
  }
}
