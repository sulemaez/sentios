
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/display/percentage_progress_linear.dart';

class SavingTargetPage extends StatefulWidget {
  @override
  _SavingTargetPageState createState() => _SavingTargetPageState();
}

class _SavingTargetPageState extends State<SavingTargetPage> {

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Saving Target",context),
      body: Padding(
        padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 4,right: SizeConfig.blockSizeHorizontal * 4,),
        child: Column(
          children: <Widget>[
            Card(
              elevation: 3,
              child: Container(
                width: SizeConfig.blockSizeHorizontal * 100,
                  padding:EdgeInsets.all(SizeConfig.blockSizeHorizontal * 2),
                  child:Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: Text("Started on : 16 Feb 2016"),
                      ),
                      SizedBox(height: SizeConfig.safeBlockVertical * 1,),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text("Saving target",style: TextStyle(color: Colors.grey),),
                      ),
                      PercentageProgressIndicator(
                        amountReached: 30000,
                        fullAmount: 50000,
                        indicatorColor: Color.fromARGB(255, 172, 147, 255),
                        mainColor: Color.fromARGB(255, 228, 228, 228),
                        height: SizeConfig.blockSizeVertical * 4,
                        overlay: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Amount collected",style: TextStyle(color: Colors.white),),
                            Text("\$50,000")
                          ],
                        ),
                      ),
                      SizedBox(height: SizeConfig.safeBlockVertical * 3,),
                    ],
                  ),
              ),
            ),
            SizedBox(height: SizeConfig.safeBlockVertical * 3,),
            Expanded(
              child: ListView(
                 children: _getChildren(),
              ),
            )
          ],
        ),
      )
    );
  }

  List<Widget> _getChildren(){
    List<Widget> list = [];
    [1,2,3,4,5,6,7,8].forEach((e){
      list.addAll([Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
         children: <Widget>[
           Row(
             children: <Widget>[
               CircleAvatar(
                 backgroundImage: Image.asset("assets/me.jpg").image,
               ),
               SizedBox(width: SizeConfig.blockSizeHorizontal * 3,),
               Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: <Widget>[
                   Text("Williamson Omera"),
                   Container(
                     width: SizeConfig.blockSizeHorizontal * 60,
                     child: PercentageProgressIndicator(
                       amountReached: 10000,
                       fullAmount: 50000,
                       indicatorColor: Color.fromARGB(255, 172, 147, 255),
                       mainColor: Color.fromARGB(255, 228, 228, 228),
                       height: 2,
                     ),
                   )
                 ],
               )
             ],
           ),
           Text("\$1550",style: TextStyle(fontWeight: FontWeight.bold),)
         ],
      ),
        SizedBox(height: SizeConfig.safeBlockVertical * 2,),
      ]);
    });
    return list;
  }
}
