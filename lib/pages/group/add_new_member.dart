
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senti/blocs/signup_bloc/signup_page_bloc.dart';
import 'package:senti/blocs/signup_bloc/signup_page_events.dart';
import 'package:senti/blocs/signup_bloc/signup_page_state.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/functional/register_widget.dart';

class AddNewMember extends StatefulWidget {
  @override
  _AddNewMemberState createState() => _AddNewMemberState();
}

class _AddNewMemberState extends State<AddNewMember> {

  SignUpPageBloc _signUpPageBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
    _signUpPageBloc = SignUpPageBloc(memberRegistration: true);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
       bloc: _signUpPageBloc,
       builder: (BuildContext context,SignUpPageState state){
         if(state.isSuccess)_signUpPageBloc.add(SuccessEvent(widget, context));
         return Stack(
            children: <Widget>[
              ListView(
                children: <Widget>[
                  RegisterWidget(bloc: _signUpPageBloc,state: state,),
                  SizedBox(height: SizeConfig.blockSizeVertical * 6,)
                ],
              ),
              state.isLoading ? buildLoader(context) : Text("")
            ],
         );
       },
    );
  }

}
