
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';

class ActiveMembersPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
       backgroundColor: Colors.white,
       appBar: MyBar("Active Members",context),
       body: FutureBuilder(
         builder: (BuildContext context,AsyncSnapshot snapshot){
           if (snapshot.connectionState == ConnectionState.none &&
               snapshot.hasData == null || snapshot.data == null) {
             return buildLoader(context);
           }

           if(snapshot.data.length < 1){
             return Center(
               child: Column(
                 children: <Widget>[
                   SizedBox(height: SizeConfig.blockSizeVertical * 20,),
                   Icon(Icons.group,color: Theme.of(context).primaryColor,size: SizeConfig.safeBlockHorizontal * 15,),
                   SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                   Text("No Members !",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.safeBlockHorizontal * 4),)
                 ],
               ),
             );
           }

           return Padding(
             padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 4),
             child: ListView.builder(
               shrinkWrap: true,
               itemBuilder: (context,index){
                 return _buildListItem(snapshot.data[index]);
               },
               itemCount: snapshot.data.length,
             ),
           );
         },
         future: _getList(),
       ),
    );
  }


  Future<List<dynamic>> _getList() async{
    try{
      Response response = await Http.doGet("$apiUrl/group/members/${Auth.user.groups[Auth.activeGroupIndex]["uuid"]}");

      if(response.statusCode == 200 || response.statusCode == 201)return response.data["data"];
    }catch(e){
      print("E $e");
    }
    return [];
  }

  Widget _buildListItem(data) {
    return Padding(
       padding:EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 3,bottom: SizeConfig.blockSizeVertical * 1),
       child: Row(
         children: <Widget>[
           CircleAvatar(
             backgroundImage: Image.network("$baseUrl${data["profile"]["profile_picture"]}").image,
             minRadius: SizeConfig.blockSizeHorizontal * 6,
           ),
           SizedBox(width: SizeConfig.blockSizeHorizontal * 3,),
           Text(data["name"],style: TextStyle(color:Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 4,fontWeight:FontWeight.bold),)
         ],
       ),
    );
  }
}
