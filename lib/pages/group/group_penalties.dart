import 'package:flutter/material.dart';
import 'package:senti/pages/group/group_penalty_detail.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/functional/InfoTableWidget.dart';
import 'package:senti/widgets/my_info_bar.dart';

class GroupPenalties extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            MyInfoBar( Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _getList(),
            ), context),
            Expanded(
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    child: _buildTable(context),
                  )
                ],
              ),
            )
          ],
        )
      ),
    );
  }


  Widget _buildTable(BuildContext context){
    return InfoTableWidget(
      headings: ["Sr No.","Name","Date","Penalty"],
      data: [
        {
          "Name":"Albert",
          "Date": "21 Jul, 2019",
          "Penalty":"\13.00"
        },
      ],
      columnWidths: {0:FractionColumnWidth(.2)},
      action: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => GroupPenaltyDetailPage())),
    );
  }

  List<Widget> _getList(){
    List<Widget> itemList = [];
    [
      {
        "title":"Total Amount from Penalties" ,
        "value": "\$356"
      },
      {
        "title":"Penalties Paid" ,
        "value": "\$62"
      },
      {
        "title":"Pending Penalties" ,
        "value": "\$356"
      }
    ].forEach((item){
      itemList.addAll([
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text("${item["title"]}: ",style: TextStyle(color:Colors.black54,fontSize: SizeConfig.blockSizeHorizontal* 4),),
            Text(item["value"],style: TextStyle(color:Colors.black54,fontSize: SizeConfig.blockSizeHorizontal* 5,fontWeight: FontWeight.bold),)
          ],
        ),
        SizedBox(height: SizeConfig.blockSizeVertical * 1,)
      ]);
    });

    return itemList;
  }
}
