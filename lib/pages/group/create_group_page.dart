import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senti/blocs/create_group_bloc/create_group_bloc.dart';
import 'package:senti/blocs/create_group_bloc/create_group_event.dart';
import 'package:senti/blocs/create_group_bloc/create_group_state.dart';
import 'package:senti/pages/base_page.dart';
import 'package:senti/pages/group/group_preview.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/utils/validators.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/functional/loan_terms.dart';
import 'package:senti/widgets/text/text_form_widgets.dart';

class CreateGroupPage extends StatefulWidget {
  @override
  _CreateGroupPageState createState() => _CreateGroupPageState();
}

class _CreateGroupPageState extends State<CreateGroupPage> {
  CreateGroupBloc _bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc = CreateGroupBloc();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar:MyBar("Create Group",context),
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.only(top:SizeConfig.blockSizeVertical * 2,),
        child:BlocBuilder(
          bloc: _bloc,
          builder: (BuildContext context,CreateGroupState state){
            //on success got to the group profile preview page
            if(state.isSuccess){
               groupBeingCreated = initializeGroupCreated();
               Timer(Duration(milliseconds: 1000),(){
                 //go back
                 Navigator.of(context).pushAndRemoveUntil(
                     MaterialPageRoute(builder: (context) => HomeBase(newGroup:true)), (Route<dynamic> r) => false);
               });
            }

            return Container(
              child: Stack(
                children: <Widget>[
                  Form(
                    key: state.formKey,
                    child: ListView(
                        shrinkWrap: true,
                        children: _buildForm(context,state)
                    ),
                  ),
                  state.isLoading ? buildLoader(context) : Text("")
                ],
              ),
              height: SizeConfig.safeBlockVertical * 100,
            );
          },
        ),
      )
    );
  }

  //builds the from elements
  List<Widget> _buildForm(context,CreateGroupState state){
    List<Widget> finalList = [];

    List<Widget> elements = [];

    //get the first 4 textboxes
    elements.add(PlainTextFormField("Group Name : ",
        value: state.name == "" ? groupBeingCreated["name"] : state.name ,
        actions: {
         "saved":(String name) => _bloc.add(SaveEvent(value : name,type : "name")),
         "valid":validateName,
         "changed":(String name) => groupBeingCreated["name"] = name
       },
    ));
    elements.add(PlainTextFormField("Group Purpose : ",
      value: state.purpose  == "" ?  groupBeingCreated["purpose"] : state.purpose,
      actions: {
        "saved":(String purpose) => _bloc.add(SaveEvent(value : purpose,type : "purpose")),
        "valid":(String purpose) => purpose.trim() ==   "" ?"Group purpose cannot be empty" : null,
        "changed":(String purpose) => groupBeingCreated["purpose"] = purpose
    },));

    elements.add(PlainTextFormField("Location :",iconData: Icons.location_on,
      value: state.location != "" ? state.location : groupBeingCreated["location"],
      actions: {
        "saved":(String location) => _bloc.add(SaveEvent(value : location,type : "location")),
        "valid":(String location) => location.trim() == "" ? "Location cannot be empty" : null,
        "changed":(String location) => groupBeingCreated["location"] = location
       },
     ));
     elements.add(PlainTextFormField("Target :",iconData: FontAwesomeIcons.bullseye,
         value: state.targetAmount == 0 ? groupBeingCreated["targetAmount"] : state.targetAmount.toString(),actions: {
        'saved':(String target) => _bloc.add(SaveEvent(value:target,type:"target")),
        "valid":(String target){
              if(target.trim() == "") return "Target cannot be empty" ;
              double amount =0;
              try{
                amount = double.parse(target);
              }catch(e){
                return "Target must be a number";
              }
              if(!(amount > 0))return "Target must be greater than zero";
              return null;
          },
      "changed":(String targetAmount) => groupBeingCreated["targetAmount"] = targetAmount
    },
        keyboardType : TextInputType.number
    ));

    elements.add(PlainTextFormField("Weekly Contributions (Minimum)",iconData: FontAwesomeIcons.calendarAlt,
      value: state.minimumContribution != "" ?  state.minimumContribution : groupBeingCreated["minimumContribution"],
        actions: {
         "saved":(String minimum) => _bloc.add(SaveEvent(value : minimum,type : "minimum")),
         "valid" : (String minimum) {
             if(minimum.trim() == "") return "Minimum cannot be empty";
             double num = 0;
             try{
                num = double.parse(minimum);
             }catch(e){
               return "Minimum must be a valid number";
             }
             if(!(num > 0))return "Minimum must be greater than zero";
             return null;
         },
          "changed":(String minimumContribution) => groupBeingCreated["minimumContribution"] = minimumContribution
      },
        keyboardType : TextInputType.number
    ));

    //setup penalties
    elements.add(setupPenalties(context,state));

    //loan terms
    elements.add(Padding(padding:EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2,bottom: SizeConfig.blockSizeVertical * 1) ,child: Text("Loan Terms :",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 4),),));
    elements.add(state.loanTermError == null ? Text(""):Padding(padding:EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 0.5) ,child: Text(state.loanTermError,style: TextStyle(color:Colors.red,fontSize: SizeConfig.blockSizeHorizontal * 3),),));
    //loan terms widget
    var data = state.loanTerms.length > 0 ? state.loanTerms : groupBeingCreated["loanTerms"];
    LoanTermsWidget _loanTerms = LoanTermsWidget(data,{
      "add":(String duration,String val){
         _bloc.add(AddLoanTermEvent(duration,val));
         if(!existsLoanTermInCreatedGroup(duration,val))
         groupBeingCreated["loanTerms"].add({
            "months": duration ,
            "interest":val
         });
      },
      "remove":(String duration){
          _bloc.add(RemoveLoanTermEvent(duration));
      },
    });

    elements.add(_loanTerms);

    //setup loan interest
    elements.add(Padding(padding: EdgeInsets.only(top:SizeConfig.blockSizeVertical * 2),));

    //first group
    finalList.add(paddedArea(elements));

    //add first break and constitution
    finalList.add(
      Container(
        color: Theme.of(context).primaryColor,
        width: SizeConfig.blockSizeHorizontal * 100,
        padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 5,top: SizeConfig.blockSizeVertical * 1,bottom: SizeConfig.blockSizeVertical * 1),
        child: Text("Group Consituition",style: TextStyle(color: Colors.white,fontSize: SizeConfig.blockSizeHorizontal * 5),),
      )
    );
    finalList.add(paddedArea(<Widget>[
      SizedBox(height: SizeConfig.blockSizeVertical * 2,),
      PlainTextFormField("Rules and Regulation for group",maxLines: 25,
         keyboardType: TextInputType.multiline,
         value: state.constitution != "" ? state.constitution : groupBeingCreated["constitution"],
         actions: {
          'saved':(String constitution) => _bloc.add(SaveEvent(value : constitution,type : "constitution")),
          'valid':(String constitution) => constitution.trim() == "" ? "Consitution cannot be empty" : null,
          'changed':(String constitution){
            _bloc.add(SaveEvent(value : constitution,type : "constitution"));
            groupBeingCreated["constitution"] = constitution;
           }
      },)
    ]));

    //final buttons
    finalList.add(paddedArea(<Widget>[
       Row(
         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
         children: <Widget>[
           createBtn("Preview",Color.fromARGB(255,158,158,158),()async{
             bool save = await Navigator.of(context).push(MaterialPageRoute(builder: (context) => GroupPreviewPage()));
             if(save != null){
               if(save){
                  print("Saved NIGGA");
               }
             }
           }),
           createBtn("Create group",Theme.of(context).primaryColor,(){
               _bloc.add(SubmitFormEvent());
            })
         ],
       ),
       SizedBox(
         height: SizeConfig.blockSizeVertical * 10,
       )
    ]));
    return finalList;
   }

   //set up the form area that has a padding
  Widget paddedArea(List<Widget> elements){
    return  Padding(
      padding:EdgeInsets.only(left:SizeConfig.blockSizeVertical * 3,right: SizeConfig.blockSizeVertical * 3 ,),
      child:Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: elements,
      ),
    );
  }

  //crete a rounded material btn
  Widget createBtn(String text,Color color,Function callback){
     return MaterialButton(
       minWidth: SizeConfig.blockSizeHorizontal * 35,
       height: SizeConfig.blockSizeVertical * 6,
       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
       color: color,
       child: Text(
         text,
         style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
       ),
       onPressed: callback,
     );
  }

   //setupPenalties
   Widget setupPenalties(context,CreateGroupState state){
       List<Widget> elements = [
         SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
         Text("Set up Penalties :",textAlign: TextAlign.left,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4,color: Theme.of(context).primaryColor),),
         state.penaltiesError == null  ? Text("") : Text(state.penaltiesError,textAlign: TextAlign.left,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 3,color: Colors.red),),
       ];

       penaltiesList.forEach((penalty){
         elements.addAll(
           [Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(penalty.description),
                Container(
                  width: SizeConfig.blockSizeHorizontal * 20,
                  height: SizeConfig.blockSizeVertical * 5,
                  child:TextFormField(
                    initialValue: getValueFromPenalty(penalty.description) ?? "",
                    keyboardType: TextInputType.number,
                    onSaved: (String val) => _bloc.add(SaveEvent(value :val,type:penalty.description)),
                    onChanged:  (String val){
                       if(!existsPenaltyInCreatedGroup(penalty.description,val))
                       groupBeingCreated["penalties"].add({
                         penalty.description : val
                       });

                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color.fromRGBO(244, 244, 244,1),
                        border:OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6.0),
                            borderSide: BorderSide(
                                width: 0.0,
                                style: BorderStyle.none
                            ),
                        ),
                    ),

                  ),
                ),
              ],
           ),
             SizedBox(height: SizeConfig.blockSizeVertical * 1,)
           ]
         );
       });

       return Column(
         crossAxisAlignment: CrossAxisAlignment.start,
         children: elements
       );
   }
}

//to represent a penalty
class Penalty{
  Penalty(this.description,this.name);

  String description;
  String name;
}

//penalties to display
List<Penalty> penaltiesList = [
  Penalty("Late Contribution Penalty","lateContribution"),
  Penalty("Absent Penalty","absentPenalty"),
  Penalty("Late to a meeting Penalty","lateToMeeting"),
  Penalty("Absent With Apology Penalty","absentWithApology"),
  Penalty("Absent Without Apology Penalty","absentWithoutApology"),
];

bool existsPenaltyInCreatedGroup(String dal,String val){
  bool found = false;
  groupBeingCreated["penalties"].forEach((map){
     if(map.keys.first == dal){
         groupBeingCreated["penalties"][groupBeingCreated["penalties"].indexOf(map)][map.keys.first] = val;
         found = true;
     }
  });
  return found;
}

bool existsLoanTermInCreatedGroup(String duration,String val){
  bool found = false;
  groupBeingCreated["loanTerms"].forEach((map){
    if(map["months"] == "duration"){
      groupBeingCreated["loanTerms"][groupBeingCreated["loanTerms"].indexOf(map)]["duration"] = val;
      found = true;
    }
  });
  return found;
}

String getValueFromPenalty(String penalty){
  String value;
  groupBeingCreated["penalties"].forEach((map){
    if(map.keys.first == penalty){
      value =  groupBeingCreated["penalties"][groupBeingCreated["penalties"].indexOf(map)][map.keys.first];
    }
  });
  return value;
}
