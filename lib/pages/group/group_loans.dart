import 'package:flutter/material.dart';
import 'package:senti/pages/group/group_loan_detail.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/functional/InfoTableWidget.dart';
import 'package:senti/widgets/my_info_bar.dart';

class GroupLoans extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: <Widget>[
            MyInfoBar( Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _getList(),
            ), context),
            Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    _buildTable(context),
                  ],
                )
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTable(BuildContext context){
    return InfoTableWidget(
      headings: ["Sr No.","Name","Loan Term","Amount"],
      data: [
        {
          "Name" : "Albert John Muinde",
          "Loan Term":"1 month",
          "Amount":"\$1500"
        },
      ],
      columnWidths: {0:FractionColumnWidth(.15),2:FractionColumnWidth(.2),3:FractionColumnWidth(.25),},
      action: () => Navigator.of(context).push(MaterialPageRoute(builder: (context)=> GroupLoanDetailPage())),
    );
  }

  List<Widget> _getList(){
    List<Widget> itemList = [];
    [
      {
        "title":"Total Amount of Loans" ,
        "value": "\$35000"
      },
      {
        "title":"Current NO. of Loans" ,
        "value": "11"
      },

    ].forEach((item){
      itemList.addAll([
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text("${item["title"]}: ",style: TextStyle(color:Colors.black54,fontSize: SizeConfig.blockSizeHorizontal* 4),),
            Text(item["value"],style: TextStyle(color:Colors.black54,fontSize: SizeConfig.blockSizeHorizontal* 5,fontWeight: FontWeight.bold),)
          ],
        ),
        SizedBox(height: SizeConfig.blockSizeVertical * 1,)
      ]);
    });

    return itemList;
  }

}
