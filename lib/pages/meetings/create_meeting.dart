import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senti/blocs/create_meetings_bloc/bloc.dart';
import 'package:senti/blocs/create_meetings_bloc/event.dart';
import 'package:senti/blocs/create_meetings_bloc/state.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/utils/validators.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/text/text_form_widgets.dart';
import 'package:intl/intl.dart';

class CreateMeetingPage extends StatefulWidget {
  @override
  _CreateMeetingPageState createState() => _CreateMeetingPageState();
}

class _CreateMeetingPageState extends State<CreateMeetingPage> {

  CreateMeetingBloc _bloc;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _bloc = CreateMeetingBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Create Meeting",context),
      body: BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext context,CreateMeetingState state){
          if(state.isSuccess){
            _bloc.add(SuccessEvent(context));
          }
          return Stack(
            children: <Widget>[
              Form(
                key: state.formKey,
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 4),
                      child: Column(
                        children: <Widget>[
                          PlainTextFormField("Meeting purpose : ",value: state.purpose,actions: {
                            "saved":(String purpose) => _bloc.add(SaveFieldEvent(type: "purpose",value: purpose)),
                            "valid":validateName
                          },errorText: state.purposeError,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                               Column(
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: <Widget>[
                                    Text("Date :",style: TextStyle(color: Theme.of(context).primaryColor),),
                                   _buildDisabledField(FontAwesomeIcons.calendarAlt,state.date,_showDatePicker,state.dateError),
                                 ],
                               ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("Time:",style: TextStyle(color: Theme.of(context).primaryColor),),
                                  _buildDisabledField(Icons.timer,state.time,_showTimePicker,state.timeError)
                                ],
                              )
                            ],
                          ),
                          PlainTextFormField("Location : ",value: state.location,actions: {
                            "saved":(String location) => _bloc.add(SaveFieldEvent(type: "location",value: location)),
                            "valid":validateName
                          },iconData: Icons.location_on,errorText: state.locationError,
                          ),
                          SizedBox(height: SizeConfig.blockSizeVertical * 3,),
                          MaterialButton(
                            minWidth: SizeConfig.blockSizeHorizontal * 40,
                            height: SizeConfig.blockSizeVertical * 6,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                            color: Theme.of(context).primaryColor,
                            child: Text(
                              "Create Meeting",
                              style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                            ),
                            onPressed: () => _bloc.add(SaveFormEvent()),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              state.isLoading ? buildLoader(context) : Text("")
            ],
          );
        },
      )
    );
  }

  Widget _buildDisabledField(IconData icon,String value,Function action,String error){
    return GestureDetector(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: SizeConfig.blockSizeVertical * 20,
            height: SizeConfig.blockSizeVertical * 7,
            padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 2),
            decoration: BoxDecoration(
                color:  Color.fromRGBO(244, 244, 244,1),
                borderRadius: BorderRadius.all(Radius.circular(6.0))
            ),
            child: Row(
              children: <Widget>[
                Icon(icon,color: Colors.grey,),
                SizedBox(width: SizeConfig.blockSizeHorizontal * 1,),
                VerticalDivider(color: Colors.grey,),
                Text(value,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4,),)
              ],
            ),
          ),
          Text(error,style: TextStyle(color: Colors.red,fontSize: SizeConfig.blockSizeHorizontal * 4),)
        ],
      ),
      onTap: action,
    );
  }

  void _showDatePicker() async{
     try{
       DateTime selectedDate =  await showDatePicker(
         context: context,
         initialDate: DateTime.now(),
         firstDate: DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day),
         lastDate: DateTime(DateTime.now().year + 20),
         builder: (BuildContext context, Widget child) {
           return child;
         },
       );

       _bloc.add(SaveFieldEvent(type: "date",value: "${selectedDate.year}/${selectedDate.month}/${selectedDate.day}"));
     }catch(e){
       print(e);
     }
  }

  void _showTimePicker() async{
     try{
       TimeOfDay selectedTime = await showTimePicker(
         initialTime: TimeOfDay.now(),
         context: context,
       );

       final now = DateTime.now();
       final dt = DateTime(now.year, now.month, now.day, selectedTime.hour, selectedTime.minute);
       List<String> str = DateFormat().add_jm().format(dt).split(" ");
       String time = str[0]+str[1];
       _bloc.add(SaveFieldEvent(type: "time",value: time));
     }catch(e){
       print(e);
     }
  }

}
