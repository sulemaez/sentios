
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/text/text_list.dart';
import 'package:intl/intl.dart';

class MeetingDetails extends StatefulWidget {
  final Map<String,dynamic> meeting;

  MeetingDetails({this.meeting});

  @override
  State<StatefulWidget> createState() => MeetingDetailsState();

}

class MeetingDetailsState extends State<MeetingDetails>{

  var meeting;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    meeting = widget.meeting;
    _getMeeting();
  }


  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Meeting Details",context),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          _setPadding(Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _rowIconText(FontAwesomeIcons.calendarAlt,DateFormat.yMMMMd("en_US").format(DateTime.parse(meeting["date"])),context),
              _rowIconText(Icons.timer,meeting["time"],context),
              _rowIconText(Icons.location_on,meeting["location"],context),
              SizedBox(height: SizeConfig.blockSizeVertical * 1,),
              Text("Keynotes"),
              SizedBox(height: SizeConfig.blockSizeVertical * 1,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 100,
                padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 2,right: SizeConfig.blockSizeHorizontal * 1,bottom: SizeConfig.blockSizeHorizontal * 2),
                child: ListText(textColor: Colors.grey,list: meeting["keynotes"] == null ? [""] : meeting["keynotes"].split("\n"),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: Color.fromRGBO(244, 244, 244,1),
                ),
              )
            ],
          )),
          Container(
            padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 1),
            color: Theme.of(context).primaryColor,
            child: Text("Attendance",style: TextStyle(color: Colors.white,fontSize: SizeConfig.safeBlockHorizontal * 5),textAlign: TextAlign.center,),
          )
        ],
      ),
    );
  }

  Widget _rowIconText(IconData icon,String text,context){
    return Padding(
      padding: EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 2),
      child: Row(
        children: <Widget>[
          Icon(icon,color: Color.fromARGB(255, 172, 147, 255),size: SizeConfig.blockSizeHorizontal * 4,),
          SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
          Text(text,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4,),),
        ],
      ),
    );
  }

  Widget _setPadding(widget){
    return Padding(
      padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 4),
      child: widget,
    );
  }

  void _getMeeting() async{
    try{
      Response response = await Http.doGet("$apiUrl/meeting/get/${Auth.user.groups[Auth.activeGroupIndex]["uuid"]}/${meeting["uuid"]}");

      if(response.statusCode == 200){
         setState(() {
            meeting = response.data["meeting"];
         });
      }
    }catch(e){

    }
  }
}
