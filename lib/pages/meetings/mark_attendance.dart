
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';

class MarkAttendancePage extends StatefulWidget {

  final String meetingUuid;

  MarkAttendancePage({this.meetingUuid});

  @override
  _MarkAttendancePageState createState() => _MarkAttendancePageState();
}

class _MarkAttendancePageState extends State<MarkAttendancePage> {
  List<String> _markedNames = [];
  List<Map<String,dynamic>> _membersList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Mark Attendance",context),
      body: Padding(
        padding: EdgeInsets.all(SizeConfig.blockSizeVertical * 3),
        child: FutureBuilder(
          builder: (BuildContext context,snapshot){
            if (snapshot.connectionState == ConnectionState.none &&
                snapshot.hasData == null || snapshot.data == null || snapshot.data.length == 0) {
              return buildLoader(context);
            }

            return ListView.builder(
              shrinkWrap: true,
              itemBuilder: (context,index){
                return _buildListItem(snapshot.data[index]);
              },
              itemCount: snapshot.data.length,
            ) ;
          },
          future:  _getRequest(),
          initialData: _membersList,
        ),
      ),
    );
  }

  Future<List> _getRequest() async{
    try{
      Response response = await Http.doGet("$apiUrl/group/members/${Auth.user.groups[Auth.activeGroupIndex]["uuid"]}");
      if(response.statusCode == 200)return response.data["data"];
    }catch(e){
       print("E");
    }
    return [];
  }

  Widget _buildListItem(data){

     return Padding(
       padding: EdgeInsets.only(bottom: SizeConfig.blockSizeHorizontal * 2),
       child: Stack(
         children: <Widget>[
           Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
               Row(
                 mainAxisAlignment: MainAxisAlignment.start,
                 children: <Widget>[
                   CircleAvatar(
                     backgroundImage:Image.network(
                       "$baseUrl${data["profile"]["profile_picture"]}",
                     ).image,radius: SizeConfig.blockSizeHorizontal * 5,
                   ),
                   SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
                   Text(data["name"],style: TextStyle(color: Colors.black54,fontSize: SizeConfig.blockSizeHorizontal * 4),),
                    SizedBox(width: SizeConfig.blockSizeHorizontal * 3,),
                   _markedNames.indexOf(data["name"]) > -1 ? Icon(Icons.check_box,color: Theme.of(context).primaryColor,) : Text("")
                 ],
               ),
               PopupMenuButton<String>(
                 onSelected: (String choice) => _markAttendance(choice,data["uuid"],data["name"]),
                 itemBuilder: (BuildContext context) {
                   return _attendanceChoices.map((String choice) {
                     return PopupMenuItem<String>(
                       value: choice,
                       child: Text(choice),
                     );
                   }).toList();
                 },
               )
             ],
           ),
         ],
       ),
     );
  }

  void _markAttendance(String choice,String userUuid,String name) async{
      try{
        Response response = await Http.doPost(url: "$apiUrl/meeting/attendance",data: {
           "uuid" : Auth.user.uuid,
           "user_uuid": userUuid,
           "group_uuid": Auth.user.groups[Auth.activeGroupIndex],
           "meeting_uuid" : widget.meetingUuid,
           "attendance_status":choice
         });
         print(response.data);
         print(response.statusMessage);

         if(response.statusCode == 200 || response.statusCode == 201){
           print("okay $name");
           setState(() {
             _markedNames.add(name);
           });

         }
      }catch(e){
         print(e);
      }
  }

  List<String> _attendanceChoices = [
    "Present","Absent","Late to meeting","Absent with Apology",
    "Absent Without Apology"
  ];


}
