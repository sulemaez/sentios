import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/alert_dialog.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/utils/validators.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/text/text_form_widgets.dart';

class KeynotesPage extends StatefulWidget {
  final String meetingUuid;

  const KeynotesPage({this.meetingUuid});

  @override
  _KeynotesPageState createState() => _KeynotesPageState();
}

class _KeynotesPageState extends State<KeynotesPage> {

  String keyNotes = "";
  bool isLoading = false;
  GlobalKey<FormState> formKey;
  String errorText = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    formKey = GlobalKey();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Keynotes",context),
      body: Stack(
            children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left:SizeConfig.safeBlockHorizontal * 4,right: SizeConfig.safeBlockHorizontal * 4),
                      child:  Form(
                        child:ListView(
                          shrinkWrap: true,
                          children: <Widget>[
                            PlainTextFormField(
                              "Description :",
                              maxLines : 20,
                              keyboardType: TextInputType.multiline,
                              errorText: errorText,
                              value: keyNotes,actions : {
                              "valid" : validateName,
                              "saved" : (String str){
                                setState(() {
                                  keyNotes = str;
                                });
                              }
                            },
                            ),
                            SizedBox(height: SizeConfig.blockSizeHorizontal * 3,),
                            MaterialButton(
                              minWidth: SizeConfig.blockSizeHorizontal * 50,
                              height: SizeConfig.blockSizeVertical * 6,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                              color: Theme.of(context).primaryColor,
                              child: Text(
                                "Save Keynotes",
                                style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                              ),
                              onPressed: saveForm,
                            )

                          ],
                        ),
                        key: formKey,
                      ),
                    ),
              isLoading ? Container(
                width: SizeConfig.blockSizeHorizontal * 100,
                height: SizeConfig.blockSizeVertical * 100,
                color: Colors.grey.withOpacity(0.1),
                child: Center(
                  child: CircularProgressIndicator(valueColor:AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor)),
                ),
              ) : Text("")
            ],
          ),
    );
  }

  void saveForm() async {
     try{
       if(formKey.currentState.validate()){

         setState(() {
           isLoading = true;
         });

         await formKey.currentState.save();

         Response resp  = await Http.doPut({
           "meeting_uuid" : widget.meetingUuid,
           "keynotes": keyNotes,
           "group_uuid" : Auth.user.groups[Auth.activeGroupIndex]["uuid"],
           "uuid":Auth.user.uuid
         },"$apiUrl/meeting/keynote/add");

         setState(() {
           isLoading = false;
         });

         if(resp.statusCode == 200 || resp.statusCode == 201){

             showCustomAlertDialog(context,message:"Keynotes added to meeting");

             setState(() {
                keyNotes = "";
             });

         }else{
           Fluttertoast.showToast(
               msg: "Check Errors !",
               textColor: Colors.white,
               backgroundColor: Colors.black,
               toastLength:Toast.LENGTH_LONG
           );

           if(resp.data["errors"]){
              setState(() {
                errorText = resp.data["errors"]["keynotes"][0];
              });
           }
         }
      }
     }catch(e){
       setState(() {
         isLoading = false;
       });
        Fluttertoast.showToast(
            msg: "Keynote not saved try again !",
            textColor: Colors.white,
            backgroundColor: Colors.black,
            toastLength:Toast.LENGTH_LONG
        );
        print("Error Saving");
     }

  }
}
