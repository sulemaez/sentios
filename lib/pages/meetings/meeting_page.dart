import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senti/blocs/meeting_page_bloc.dart';
import 'package:senti/pages/meetings/create_meeting.dart';
import 'package:senti/pages/meetings/mark_attendance.dart';
import 'package:senti/pages/meetings/meeting_details.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/globals/date.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/functional/meeting_calender.dart';

class MeetingPage extends StatefulWidget {

  @override
  _MeetingPageState createState() => _MeetingPageState();
}

class _MeetingPageState extends State<MeetingPage> {

  var isAdmin;
  MeetingPageBloc _bloc;
  var isGroupActive = false;
  var noGroup = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(Auth.user.groups.length > 0 && Auth.activeGroupIndex > -1){
      isAdmin = isGroupAdmin(Auth.user.groups[Auth.activeGroupIndex],Auth.user.uuid);
      isGroupActive = Auth.user.groups[Auth.activeGroupIndex]["activated"];
      _bloc = MeetingPageBloc();
      noGroup = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return  SafeArea(
      child: isAdmin == null ?
        joinGroupFirst(context):  Scaffold(
        body: isGroupActive ? BlocBuilder(
          bloc: _bloc,
          builder: (BuildContext context,MeetingPageState state){
             if(state.loadData){
               _bloc.add(LoadDataEvent());
             }
             return Column(
               children: <Widget>[
                 SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                 MeetingCalender(meetingPageState: state,onMonthChanged: (int month,int year){
                   _bloc.add(MonthChangedEvent(month:month,year: year));
                 },onCalenderToggle: (bool ans) => _bloc.add(ShowCalenderEvent(ans)),),
                 Expanded(
                   child: ListView(
                     shrinkWrap: true,
                     children: _buildMeetingRow(state),
                   ),
                 )
               ],
             );
          },
        ):buildGroupNotActive(context),
        floatingActionButton: (isAdmin && isGroupActive)? FloatingActionButton(
          backgroundColor: Theme.of(context).primaryColor,
          child: Icon(Icons.add,color: Colors.white,),
          //create meeting page
          onPressed: ()async {
            _bloc.add(ShowCalenderEvent(false));
             await Navigator.of(context).push(MaterialPageRoute(builder: (context) => CreateMeetingPage()));
             _bloc.add(LoadDataEvent());
          },
        ):Text(""),
      )
    );
  }

  List<Widget> _buildMeetingRow(MeetingPageState state){
    List<Widget> list = [];

    state.activeMeetings.forEach((meeting){
       DateTime date = DateTime.parse("${meeting["date"]} ${meeting["time"]}");
       bool isPast = date.isBefore(DateTime.now());
       list.add( Padding(
         padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
         child: GestureDetector(
           child: Container(
             width: SizeConfig.blockSizeHorizontal * 100,
             child: Card(
               child: Container(
                 padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
                 child: Row(
                   children: <Widget>[
                     Column(
                       children: <Widget>[
                         Text(date.day.toString(),style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 6),),
                         Text(getWeekDay(date.weekday,short:true),style: TextStyle(color: Colors.grey),),
                       ],
                     ),
                     SizedBox(width: SizeConfig.blockSizeHorizontal * 5,),
                     Container(
                       child: VerticalDivider(color:Colors.grey.withOpacity(0.5), width:0.2,),
                       height: SizeConfig.blockSizeVertical * 8,
                     ),
                     SizedBox(width: SizeConfig.blockSizeHorizontal * 5,),
                     Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         Text(meeting["purpose"],style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 5,fontWeight: FontWeight.bold),),
                         SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                         AutoSizeText("${meeting["location"]} at ${meeting["time"]}",style:TextStyle(color: Colors.grey)),
                         SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                         isAdmin ? Container(
                           width: SizeConfig.blockSizeHorizontal * 60,
                           child: Align(
                             alignment: Alignment.topRight,
                             child: GestureDetector(
                               child:  Text("Mark Attendance",style: TextStyle(color: Colors.orangeAccent,fontWeight: FontWeight.bold,fontStyle: FontStyle.italic,fontSize: SizeConfig.blockSizeHorizontal * 4,decoration: TextDecoration.underline)),
                               onTap: (){
                                 _bloc.add(ShowCalenderEvent(false));
                                 Navigator.of(context).push(MaterialPageRoute(builder: (context) => MarkAttendancePage(meetingUuid: meeting["uuid"],)));
                               },
                             ),
                           ),
                         ): Text("")
                       ],
                     )
                   ],
                 ),
                 decoration: BoxDecoration(
                     border: Border(
                         left: BorderSide(
                             color: isPast ? Color.fromARGB(255, 159, 183, 255) :Color.fromARGB(255, 246, 144, 33),
                             width:4
                         )
                     )
                 ),
               ),
             ),
           ),
           onTap: (){
             _bloc.add(ShowCalenderEvent(false));
             Navigator.of(context).push(MaterialPageRoute(builder: (context) => MeetingDetails(meeting:meeting)));
           },
         ),
       ),
       );
    });

    return list;
  }

}
