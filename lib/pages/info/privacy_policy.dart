
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:senti/widgets/appbar_all.dart';


class PrivacyPolicyPage extends StatelessWidget{
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
       appBar: MyBar("Privacy Policy", context),
       body: Container(
         color: Colors.white,
         child: Padding(
           padding: EdgeInsets.all(25),
           child: Column(
             children: <Widget>[
               Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Netus et malesuada fames ac. Convallis tellus id interdum velit laoreet id donec ultrices tincidunt. Tortor at auctor urna nunc id cursus. Interdum velit euismod in pellentesque. In massa tempor nec feugiat nisl pretium fusce id. Velit egestas dui id ornare. Dictum sit amet justo donec. Dolor sit amet consectetur adipiscing elit. Elit at imperdiet dui accumsan. Morbi tincidunt augue interdum velit euismod in pellentesque. Id diam maecenas ultricies mi eget mauris. Posuere sollicitudin aliquam ultrices sagittis. Tincidunt vitae semper quis lectus nulla at. Nibh sed pulvinar proin gravida. Suscipit tellus mauris a diam. Cursus eget nunc scelerisque viverra mauris."),
               Text("\n"),
               Text("Risus sed vulputate odio ut enim blandit. In metus vulputate eu scelerisque felis. Non quam lacus suspendisse faucibus interdum posuere lorem. Vulputate dignissim suspendisse in est ante in nibh mauris. Est velit egestas dui id ornare arcu odio. Vel elit scelerisque mauris pellentesque pulvinar pellentesque. Varius quam quisque id diam vel quam elementum pulvinar. Enim tortor at auctor urna. Lacus laoreet non curabitur gravida. Cras pulvinar mattis nunc sed. Cras pulvinar mattis nunc sed blandit libero volutpat sed. Morbi tincidunt augue interdum velit euismod in pellentesque."),
               Text("\n"),
               Text("Turpis egestas integer eget aliquet nibh praesent tristique. Augue interdum velit euismod in pellentesque massa placerat. Nibh tellus molestie nunc non blandit massa enim nec dui. Diam vulputate ut pharetra sit amet aliquam id diam. Mauris rhoncus aenean vel elit scelerisque. Pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus. Egestas pretium aenean pharetra magna ac. Dapibus ultrices in iaculis nunc sed augue lacus. Ornare arcu odio ut sem. Arcu bibendum at varius vel pharetra. Quam pellentesque nec nam aliquam sem et tortor consequat id.")
             ],
           ),
         )
       ),
    );
  }
}

