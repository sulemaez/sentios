
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/functional/InfoTableWidget.dart';

class DuesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Dues",context),
      body: Column(
        children: <Widget>[
          _buildTable(),
           Padding(
             padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
             child: Column(
               mainAxisAlignment: MainAxisAlignment.start,
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                  Text("Total Amount :  \$1652",style: TextStyle(color: Colors.black54,fontSize: SizeConfig.blockSizeHorizontal * 5,fontWeight: FontWeight.bold),),
                  SizedBox(height: SizeConfig.blockSizeVertical * 3,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Amount you want to pay",style: TextStyle(color: Colors.grey),),
                      Container(
                        width: SizeConfig.blockSizeHorizontal * 30,
                        height: SizeConfig.blockSizeVertical * 5,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          onSaved: (String val) {},
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Color.fromRGBO(244, 244, 244,1),
                            border:OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                  width: 0.0,
                                  style: BorderStyle.none
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                 SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                     Text("Amount due after pay",style: TextStyle(color: Colors.grey),),
                     Text("\$452",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold,fontSize: SizeConfig.blockSizeHorizontal * 5),)
                   ],
                 ),
                 SizedBox(height: SizeConfig.blockSizeVertical * 3,),
                 Align(
                   alignment: Alignment.topCenter,
                   child: MaterialButton(
                     minWidth: SizeConfig.blockSizeHorizontal * 30,
                     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                     color: Theme.of(context).primaryColor,
                     child: Text(
                       "Pay",
                       style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                     ),
                     onPressed: (){
                       Navigator.of(context).pop();
                     },
                   ),
                 )
               ],
             ),
           )
        ],
      ),
    );
  }

  Widget _buildTable(){
     return InfoTableWidget(
       headings: ["Sr No.","Type","Amount"],
       data: [
         {
           "Type":"Monthly Contribution",
           "Amount": "\$150"
         },
         {
           "Type":"Loan Installment",
           "Amount": "\$50"
         },
         {
           "Type":"Penalty",
           "Amount": "\$1500"
         },
       ],
       columnWidths: {0:FractionColumnWidth(.2),2:FractionColumnWidth(.3)},
     );
  }
}
