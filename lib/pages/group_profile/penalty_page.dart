import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/display/penalty_info.dart';


class PenaltyPage extends StatefulWidget {
  @override
  _PenaltyPageState createState() => _PenaltyPageState();
}

class _PenaltyPageState extends State<PenaltyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Penalty",context),
      body: Container(
        padding: EdgeInsets.only(left:SizeConfig.blockSizeVertical * 4,right: SizeConfig.blockSizeVertical * 4),
        height: SizeConfig.blockSizeVertical * 100,
        child: ListView(
           children: <Widget>[
             PenaltyInfoWidget(paid: false,),
             SizedBox(height: SizeConfig.blockSizeVertical * 4,),
             PenaltyInfoWidget(paid: true,)
           ],
        ),
      ),
    );
  }
}
