
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';

class PenaltyEmptyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Penalty",context),
      body: Container(
        width: SizeConfig.blockSizeHorizontal * 100,
        height: SizeConfig.blockSizeVertical * 100,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: SizeConfig.blockSizeHorizontal * 50,
                child: Image.asset("assets/penalty_empty.png",),
              ),
              SizedBox(height: SizeConfig.blockSizeVertical * 10,),
              Text("Congratulations",style: TextStyle(color: Theme.of(context).primaryColor,
                  fontSize: SizeConfig.blockSizeHorizontal * 5,fontWeight: FontWeight.bold),
              ),
              SizedBox(height: SizeConfig.blockSizeVertical * 1,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 70,
                child: AutoSizeText("You are very attentive and punctual \n for every meeting.You are not found guilty \n for any penalty",style: TextStyle(color: Colors.grey),textAlign: TextAlign.center,),
              ),
              SizedBox(height: SizeConfig.blockSizeVertical * 20,),
            ],
          ),
        ),
      ),
    );
  }
}
