
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:senti/models/user.dart';
import 'package:senti/pages/base_page.dart';
import 'package:senti/pages/profile/profile_page.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';

class ChangeGroupPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: MyBar("Switch Groups",context),
       backgroundColor: Colors.white,
       body:Column(
         children: <Widget>[
            Container(
              width: SizeConfig.safeBlockHorizontal * 100,
              child: Center(
                child: MaterialButton(
                  height: SizeConfig.blockSizeVertical * 5,
                  minWidth: SizeConfig.blockSizeHorizontal * 30,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    "Join/Create New",
                    style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                  ),
                  onPressed: (){
                    //remove active group
                    Auth.activeGroupIndex = -1;
                    //got to home page
                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeBase()), (route)=>false);
                  },
                ),
              ),
            ),
            Expanded(
              child: FutureBuilder(
                builder: (BuildContext context,AsyncSnapshot snapshot){
                  if (snapshot.connectionState == ConnectionState.none &&
                      snapshot.hasData == null || snapshot.data == null) {
                    return buildLoader(context);
                  }

                  if(snapshot.data.length < 1){
                    return Center(
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: SizeConfig.blockSizeVertical * 20,),
                          Icon(Icons.group,color: Theme.of(context).primaryColor,size: SizeConfig.safeBlockHorizontal * 15,),
                          SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                          Text("No Groups Joined Yet !",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.safeBlockHorizontal * 4),)
                        ],
                      ),
                    );
                  }

                  return Padding(
                    padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 4),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemBuilder: (context,index){
                        return _buildListItem(snapshot.data[index],index,context);
                      },
                      itemCount: snapshot.data.length,
                    ),
                  );
                },
                future: _getList(),
              ),
            )
         ],
       ) ,
    );
  }

  Widget _buildListItem(data,int index,context) {
    return Column(
      children: <Widget>[
        ProfileListItem(name:data["name"],callback: (){
          Auth.activeGroupIndex = index;
          //got to home page
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeBase()), (route)=>false);
        },),
        Divider(height: SizeConfig.blockSizeVertical * 2,)
      ],
    );
  }

  Future<List<dynamic>> _getList() async{
    try{
      //get the updated user details
      if(await checkNet()){

        Response resp = await Http.doGet("$apiUrl/user/me");
        if(resp.statusCode  == 200){
          //create an user
          Auth.user = User(
              email:resp.data["data"]['email'],
              uuid: resp.data["data"]["uuid"],
              name: resp.data["data"]['name'],
              contact: resp.data["data"]['profile']['contacts'][0]['contact'],
              description: resp.data["data"]['profile']['description'],
              pic: "$baseUrl/${resp.data["data"]['profile']['profile_picture']}",
              token: Auth.user.token,
              groups:resp.data["data"]['groups'],
              dob : resp.data["data"]['profile']['dob']
          );
        }
      }

      return Auth.user.groups;

    }catch(e){
      print("E $e");
    }
    return [];
  }
}
