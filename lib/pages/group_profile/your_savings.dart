import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/pages/group_profile/dues_page.dart';
import 'package:senti/pages/group_profile/penalty_page.dart';
import 'package:senti/pages/loans/loan_page.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/display/percentage_progress_linear.dart';

class YourSavingsPage extends StatefulWidget {
  @override
  _YourSavingsPageState createState() => _YourSavingsPageState();
}

class _YourSavingsPageState extends State<YourSavingsPage> {
  @override
  Widget build(BuildContext context) {
    if(Auth.user.groups.length == 0 || Auth.activeGroupIndex == -1)return joinGroupFirst(context);
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: ListView(
            children: <Widget>[
              SizedBox(height: SizeConfig.blockSizeVertical * 3,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 100,
                height: SizeConfig.blockSizeVertical * 100,
                padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3,right: SizeConfig.blockSizeHorizontal * 3),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _greyBox("Joined On","10-4-12"),
                        _greyBox("Monthly Contribution","\$150"),
                        _greyBox("Installements Paid","25")
                      ],
                    ),
                    SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                    Card(
                      elevation: 3,
                      child: Container(
                          padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 4),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Your Savings",style: TextStyle(color: Colors.grey)),
                              PercentageProgressIndicator(
                                height: SizeConfig.blockSizeVertical * 5,
                                mainColor: Color.fromARGB(255, 244, 244, 244),
                                indicatorColor: Colors.orange,
                                fullAmount: 3750,
                                amountReached: 1400,
                                overlay: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Amount Saved",style: TextStyle(color: Colors.white),),
                                    Text("\$3750",style: TextStyle(color: Colors.grey))
                                  ],
                                ),
                              )
                            ],
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                          )
                      ),
                    ),
                    SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                    _buildCard("Loan","View your loan history and loan requests from members of your group","assets/loan_empty.png",(){Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoanPage(valid: false,)));}),
                    _buildCard("Penalty","View the penalties you have been issued","assets/penalty_pic.png",(){Navigator.of(context).push(MaterialPageRoute(builder: (context) => PenaltyPage()));}),
                    _buildCard("Dues","View the your due  payments and pay","assets/watering.png",
                            (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => DuesPage()));}),
                  ],
                ),
              ),
            ],
          )
      ),
    );
  }

  Widget _greyBox(String title,String value){
    return Container(
      padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
      width: SizeConfig.blockSizeHorizontal * 30,
      height: SizeConfig.blockSizeHorizontal * 23,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Align(
            alignment: Alignment.topLeft,
            child: AutoSizeText(title,style: TextStyle(color: Colors.grey),),
          ),
          Align(
            alignment: Alignment.topRight,
            child: AutoSizeText(value,style: TextStyle(fontWeight: FontWeight.bold,fontSize: SizeConfig.blockSizeHorizontal * 5),),
          )
        ],
      ),
      decoration: BoxDecoration(
          color:  Color.fromARGB(255, 244, 244, 244),
          borderRadius: BorderRadius.all(Radius.circular(6)),
          border: Border.all(
              color: Colors.grey
          )
      ),
    );
  }

  Widget  _buildCard(String title,String content,String image,Function action){
    return Card(
      elevation: 3,
      child: Container(
          padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
             SizedBox(
               height: SizeConfig.blockSizeHorizontal * 30,
               width: SizeConfig.safeBlockHorizontal * 50,
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: <Widget>[
                   Text(title,style: TextStyle(color:Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 6),),
                   AutoSizeText(content,style: TextStyle(color: Colors.grey),),
                   GestureDetector(
                     child: Text("View More..",style: TextStyle(color: Colors.orange,fontStyle: FontStyle.italic,decoration: TextDecoration.underline),),
                     onTap: (){
                       action();
                     },
                   )
                 ],
               ),
             ),
             Image.asset(image,width: SizeConfig.blockSizeHorizontal * 30,height: SizeConfig.blockSizeHorizontal * 30,)
            ],
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(6)),
          )
      ),
    );
  }

}
