import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senti/pages/auth/change_password.dart';
import 'package:senti/pages/auth/login_register.dart';
import 'package:senti/pages/group/leave_group.dart';
import 'package:senti/pages/group_profile/change_group_list.dart';
import 'package:senti/pages/info/privacy_policy.dart';
import 'package:senti/pages/info/terms_page.dart';
import 'package:senti/pages/profile/edit_profile.dart';
import 'package:senti/pages/profile/profilebase_page.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/utils/size_config.dart';

//the page that shows user profiles logout and other linksk
class ProfilePage extends StatelessWidget {

  ProfilePage();

  //TODO set name and user deails
  @override
  Widget build(BuildContext context) {
    //profile bass id the common profile style
    return ProfilePageBase(
        children : [
          _CardContent(),
        ]
    );
  }

}

//the card contect that is in the profile page
class _CardContent extends StatefulWidget {

  _CardContent();

  @override
  State<StatefulWidget> createState() => _CardContentState();
}

class _CardContentState extends State<_CardContent>{

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 5,right: SizeConfig.blockSizeHorizontal * 5,bottom:SizeConfig.blockSizeHorizontal * 2 ),
      child:  Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox( height: SizeConfig.blockSizeVertical * 4,),
              Align(
                  alignment: Alignment.centerRight,
                  child: GestureDetector(
                    child: Icon(FontAwesomeIcons.pencilAlt,color: Colors.orangeAccent,),
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => EditProfilePage()));
                    },
                  )
              ),
              SizedBox(height: SizeConfig.blockSizeVertical * 2,),
              Text(Auth.user.name,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 7),),
              Text(Auth.user.email),
              Padding(
                padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2,),
                child: Divider(),
              ),
               ProfileListItem(name:"Leave Group", callback : () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => LeaveGroupPage()))),
               ProfileListItem(name:"Switch Group", callback : () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChangeGroupPage()))),
               ProfileListItem(name:"Change Password", callback : () => Navigator.of(context).push(MaterialPageRoute(builder : (context) => ChangePasswordPage()))),
               ProfileListItem(name:"Privacy Policy", callback : () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => PrivacyPolicyPage()))),
               ProfileListItem(name:"Terms and Conditions", callback : () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => TermsAndConditionsPage()))),
              isLoading ? Center(
                child: CircularProgressIndicator(valueColor:AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor)),
              ): ProfileListItem(name:"Logout", callback: () => _logoutUser(context),
              )
            ],
          ),

        ],
      ),
    );
  }

  void _logoutUser(BuildContext context) async{
      setState(() {
         isLoading = true;
      });

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => LoginRegister(),
          ),
              (Route<dynamic> r) => false
      );
      Auth.logout();
  }
}


class ProfileListItem extends StatelessWidget{
  final String name;
  final void Function() callback;

  ProfileListItem({this.name,this.callback});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      child:Container(
          color: Colors.white,
          padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 3,bottom: SizeConfig.blockSizeVertical * 2),
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(name,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),),
              Icon(Icons.arrow_forward_ios,size: SizeConfig.blockSizeHorizontal * 5),
            ],
          ) ,
      ),
      onTap: callback,
    );
  }

}