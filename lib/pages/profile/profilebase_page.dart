import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/utils/size_config.dart';

class ProfilePageBase extends StatefulWidget{

  final List<Widget> children;

  ProfilePageBase({this.children});

  @override
  State<StatefulWidget> createState() => ProfilePageBaseState();

}

class ProfilePageBaseState extends State<ProfilePageBase>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body:ListView(
            shrinkWrap: true,
            children: <Widget>[
              Container(
                color: Colors.white,
                child:  Stack(
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Container(
                      width: SizeConfig.blockSizeHorizontal * 100,
                      height: SizeConfig.blockSizeVertical * 35,
                      decoration: BoxDecoration(
                          gradient:LinearGradient(
                              begin:Alignment.topCenter ,
                              end:Alignment.bottomCenter,
                              colors: [
                                Color(0xff9600ff),
                                Color(0xff9300ff),
                              ]
                          ) ,
                          borderRadius: BorderRadius.only(bottomRight: Radius.circular(30),bottomLeft: Radius.circular(30))
                      ),
                    ),
                    //for edit page the first child will be the back button
                    widget.children.length > 1 ? widget.children[0]: Text(""),
                    //the card content
                    Positioned(
                      left: SizeConfig.blockSizeHorizontal * 10,
                      top: SizeConfig.blockSizeVertical * 16,
                      child: Card(
                        color: Colors.white,
                        elevation: 2.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        child: Container(
                          width: SizeConfig.blockSizeHorizontal * 80,
                          //for edit page the card will be second in array
                          child: widget.children.length > 1 ? widget.children[1]: widget.children[0],
                        ),
                      ),
                    ),
                    Positioned(
                      left: SizeConfig.blockSizeHorizontal * 35,
                      top: SizeConfig.blockSizeVertical * 8,
                      child: CircleAvatar(
                        radius: SizeConfig.blockSizeHorizontal * 17,
                        backgroundImage: Image.network(
                          Auth.user.pic,
                        ).image,
                      ),
                    ),
                    //the edit page will have three children back buttton , the card and the image icon to edit image
                    //the pic will be last
                    widget.children.length > 1 ? widget.children[2] : Text(""),
                    widget.children.length > 1 ? widget.children[3]:Text(""),
                    widget.children.length > 1 ? widget.children[4]:Text(""),
                  ],
                ),
                height: SizeConfig.blockSizeVertical * 100,
              ),
            ],

          ),
    );
  }
}