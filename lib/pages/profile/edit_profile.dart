import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:senti/models/user.dart';
import 'package:senti/pages/profile/profilebase_page.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/globals/images.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/utils/validators.dart';
import 'package:senti/widgets/text/text_form_widgets.dart';

class EditProfilePage extends StatefulWidget {

  EditProfilePage();

  @override
  State<StatefulWidget> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfilePage>{
  //validation
  GlobalKey<FormState> _formKey;
  //card contect
  _CardContent cardContent;
  String name = "";
  String contact = "";
  String description = "";
  String imagePath = "";

  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _formKey = GlobalKey<FormState>();
     cardContent = _CardContent(formKey:_formKey,save : _saveValues);
  }

  @override
  Widget build(BuildContext context) {
    return ProfilePageBase(children: <Widget>[
      Positioned(
        left: SizeConfig.blockSizeHorizontal * 2,
        top: SizeConfig.blockSizeVertical * 3,
        child: IconButton(
          icon: Icon(Icons.arrow_back_ios,color: Colors.white,),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      cardContent,
      //the edit image icon
      Positioned(
        top: SizeConfig.blockSizeVertical * 22,
        left: SizeConfig.blockSizeHorizontal * 36,
        child:  Container(
          width: SizeConfig.blockSizeHorizontal *30,
          decoration: BoxDecoration(
              color:Color(0xffAC93FF),
              shape:BoxShape.circle
          ),
          padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
          child: Center(
            child: GestureDetector(
              child:  Icon(Icons.camera_enhance,color: Colors.white,),
              onTap: () async{
                 String src = await chooseImageSource(context);
                 File file = await getImage(src);
                 setState(() {
                   imagePath = file.path;
                 });
              },
            ),
          ),
        ),
      ),
      Positioned(
        bottom: SizeConfig.blockSizeVertical * 4,
        left: SizeConfig.blockSizeHorizontal * 25,
        child: Container(
          child: MaterialButton(
            minWidth: SizeConfig.blockSizeHorizontal * 50,
            height: SizeConfig.blockSizeVertical * 7,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
            color: Theme.of(context).primaryColor,
            child : Text(
              "UPDATE",
              style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
            ),
            onPressed: (){
              //validate form
              if(_formKey.currentState.validate()){
                //send stuff to db
                setState(() {
                  isLoading = true;
                });
                _formKey.currentState.save();
              }
            },
          ),
          color: Colors.white,
        ),
      ),
      isLoading ? buildLoader(context) :Text("")
    ],
    );
  }

  void _sendData() async{
     try{
       FormData formData = new FormData.fromMap({
         "name": name,
         "description": description,
         "contact": contact,
         "profile_picture": imagePath == "" ? null : await MultipartFile.fromFile(imagePath,filename: "profilePic.jpg"),
         "_method":"put"
       });

       print(formData.fields);
       Dio dio = Dio();
       Response response = await dio.post("$apiUrl/profile/update",data:formData,options: Options(
           responseType: ResponseType.json,
           followRedirects: false,
           validateStatus: (status) { return status < 500; },
           headers: {
             'Accept': 'application/json',
             'Authorization':'Bearer ${Auth.user.token}'
           }));

       setState(() {
         isLoading = false;
       });

       print(response.data);
       print(response.statusCode);
       print(response.statusMessage);
       if(response.statusCode == 200 || response.statusCode == 201){
         Fluttertoast.showToast(
             msg: "Profile updated",
             backgroundColor: Theme.of(context).primaryColor,
             textColor: Colors.white,
             gravity:ToastGravity.CENTER
         );
         //update created user
         Auth.user = User(
             email:response.data["data"]['email'],
             uuid: response.data["data"]["uuid"],
             name: response.data["data"]['name'],
             contact: response.data["data"]['profile']['contacts'][0]['contact'],
             description: response.data["data"]['profile']['description'],
             pic: "$baseUrl/${response.data["data"]['profile']['profile_picture']}",
             token: Auth.user.token,
             groups:response.data["data"]['groups'],
             dob : response.data["data"]['profile']['dob']
         );

         //refresh base
         homeBaseState.setPages();

       }else{
         Fluttertoast.showToast(
             msg: "Profile not updated !",
             backgroundColor: Colors.black,
             textColor: Colors.white
         );
       }
     }catch(e){
       print(e);
       setState(() {
         isLoading = false;
       });
       Fluttertoast.showToast(
           msg: "Profile not updated !",
           backgroundColor: Colors.black,
           textColor: Colors.white
       );
     }
  }

  void _saveValues(String type,String value){
    switch(type){
      case "name":
        setState(() {
          name = value;
        });
        break;
      case "description":
        setState(() {
          description = value;
        });
        break;
      case "contact":
        setState(() {
          contact = value;
        });
        break;
    }

    if(name != "" && description != "" && contact != ""){
       _sendData();
    }

  }

}


class _CardContent extends StatefulWidget {

  final GlobalKey<FormState> formKey;
  final Function save;

  _CardContent({this.formKey,this.save});

  @override
  State<StatefulWidget> createState() => _CardContentState();
}


class _CardContentState extends State<_CardContent>{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
        padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 5,right: SizeConfig.blockSizeHorizontal * 5),
        child: Form(
          key:widget.formKey,
          child: Column(
            children: <Widget>[
               SizedBox(height: SizeConfig.blockSizeVertical * 11,),
               PlainTextFormField("Name",value: Auth.user.name,actions: {
                 "saved" : (String name){
                   widget.save("name",name);
                 },
                 "valid" : validateName
               },),
               PlainTextFormField("Contact",value: Auth.user.contact,actions: {
                 "saved" : (String contact){
                   widget.save("contact",contact);
                 },
                 "valid" :(String contact) => contact == "" ? "Contact cannot be empty" : null
               },),
               PlainTextFormField("Description",maxLines: 9,value: Auth.user.description,actions: {
                 "saved" : (String description){
                   widget.save("description",description);
                 },
                 "valid" :(String description) => description == "" ? "Description cannot be empty" : null
               },)
            ],
          ),
        )
    );
  }
}
