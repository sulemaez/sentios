import 'package:auto_size_text/auto_size_text.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:senti/pages/group/create_group_page.dart';
import 'package:senti/pages/group/group_preview.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/alert_dialog.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/functional/home_group_preview.dart';
import 'home_page.dart';

class NoGroupHomePageState extends State<HomePage> {

  List<Widget> _widgetList = [];
  String nextUrl = "$apiUrl/group/get/all";
  bool isLoading = false;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
     SizeConfig().init(context);
    _getNextGroups();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Center(child: Text("Home",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25,color: Theme.of(context).primaryColor),),),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: SizeConfig.blockSizeHorizontal * 3),
            child: Icon(Icons.notifications,color: Color.fromARGB(255, 121, 102, 254),size: SizeConfig.blockSizeHorizontal * 7,),
          )
        ],
      ),
      body: Stack(
         children: <Widget>[
           Container(
             color: Colors.white,
             child:Padding(
               padding: EdgeInsets.only( top: SizeConfig.blockSizeVertical * 3,left: SizeConfig.blockSizeHorizontal * 4,right: SizeConfig.blockSizeHorizontal * 4),
               child: ListView(
                   shrinkWrap: true,
                   children:<Widget>[
                     GestureDetector(
                       child: Stack(
                         children: <Widget>[
                           Container(
                             height:SizeConfig.blockSizeVertical * 13,
                             child: Center(
                               child: Container(
                                 height : MediaQuery.of(context).orientation == Orientation.portrait ? SizeConfig.blockSizeVertical * 9.6:
                                 SizeConfig.blockSizeVertical * 24,
                                 width: SizeConfig.blockSizeHorizontal * 90,
                                 decoration: BoxDecoration(
                                     color: Color.fromARGB(255,242, 242, 242),
                                     borderRadius: BorderRadius.only(
                                         topRight: Radius.circular(10.0),
                                         bottomRight: Radius.circular(10.0)
                                     )
                                 ),
                                 child: Padding(
                                   padding: SizeConfig.screenHeight > 330 ? EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3,top:SizeConfig.blockSizeVertical*1,right: SizeConfig.blockSizeVertical *  0.5):EdgeInsets.all(0),
                                   child:Row(
                                     mainAxisAlignment: MainAxisAlignment.start,
                                     children: <Widget>[
                                       SizedBox(
                                         width: SizeConfig.blockSizeHorizontal * 25,
                                       ),
                                       Flexible(
                                         child: Column(
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: <Widget>[
                                             Text("Create new group",textAlign: TextAlign.left, style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 5),),
                                             AutoSizeText("Lorem Ipsum dolor sit amet, consec adipiscing elit, set dlam",softWrap: true,style: TextStyle( fontSize: SizeConfig.blockSizeHorizontal * 0.5,color:Color.fromARGB(255, 145, 145, 145),),),
                                           ],
                                         ),
                                       )
                                     ],
                                   ),
                                 ),
                               ),
                             ),
                           ),
                           Card(
                             elevation: 2.0,
                             color:Color.fromARGB(255, 67, 37, 111) ,
                             child: Container(
                               width: SizeConfig.blockSizeHorizontal * 25,
                               height: MediaQuery.of(context).orientation == Orientation.portrait ? SizeConfig.blockSizeVertical * 12:
                               SizeConfig.blockSizeVertical * 30,
                               child: Center(
                                 child: Column(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                   children: <Widget>[
                                     Icon(Icons.people,color:Colors.white,),
                                     AutoSizeText("Create Group",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),textAlign: TextAlign.center,)
                                   ],
                                 ),
                               ),
                             ),
                           ),
                         ],
                       ),
                       onTap: (){
                         if(Auth.user.description != null){
                           Navigator.of(context).push(
                               MaterialPageRoute(builder:  (context) => CreateGroupPage())
                           );
                         }else{
                           showCustomAlertDialog(context,message: "Kindly write your description in the profile page to be able to create a group");
                         }
                       },
                     ),
                     Padding(padding: EdgeInsets.all(SizeConfig.blockSizeVertical * 1), child:  Text("Or",textAlign: TextAlign.center,style: TextStyle(color: Colors.grey),),),
                     AutoSizeText("You can join any group as per your convenience. Send request to any group and you will be added "
                         "to the group by the group leader once your request has been accepted.",style: TextStyle(color: Theme.of(context).primaryColor),),
                     Column(
                       children: _widgetList,
                     )
                   ]
               ) ,
             ),
           ),
           isLoading ? buildLoader(context) : Text("")
         ],
      ),
    );
  }

  //gets the list of groups
  void _getNextGroups() async{
     try{

       if(nextUrl != null){
         //remove the las button
         if(_widgetList.length > 0)setState(() {
           _widgetList.removeLast();
         });
         _widgetList.add(Padding(
           padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 4,),
           child: CircularProgressIndicator(valueColor:AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),),
         ));

         if(!await checkNet()){
           _getNoGroups();
           return;
         }

         //fetch the groups
         Response response =  await Http.doGet(nextUrl);

         if(response.statusCode == 200){

           //populate the list
           var data = response.data["data"];
           setState(() {
             nextUrl = response.data["links"]["next"];
           });
           if(data.length < 1 || data == null){
             _getNoGroups();
             return;
           }

           if(_widgetList.length > 0)_widgetList.removeLast();
           await data.forEach((map)=>{
             setState(() {
               _widgetList.add(GroupPreview(
                   groupName: map["name"],
                   location: map["location"],
                   minimum: map["active_target"]["minimum_contribution"],
                   target: map["active_target"]["target_amount"],
                   actionJoin: (){
                     _sendJoinRequest(map);
                   },
                   actionView: () => _createGroupView(map)
                   ,
               ));
             })
           });
         }

         //return button
         if(nextUrl != null)
           setState(() {
             _widgetList.add(_getLoaderButton());
           });
       }
     }catch(e){

     }
  }

   //
  void _getNoGroups(){
       setState(() {
      if(_widgetList.length > 0) _widgetList.removeLast();
      _widgetList.add(Column(
        children: <Widget>[
          Icon(Icons.group,size: SizeConfig.safeBlockHorizontal * 30,color: Theme.of(context).primaryColor,),
          Text("No groups available !",style: TextStyle(color: Theme.of(context).primaryColor),),
          SizedBox(height: SizeConfig.safeBlockVertical * 1,),
          _getLoaderButton()
        ],
      ));
    });
  }

  //
  Widget _getLoaderButton(){
    return  MaterialButton(
      minWidth: SizeConfig.blockSizeHorizontal * 20,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
      color: Theme.of(context).primaryColor,
      child: Text(
        "load more...",
        style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
      ),
      onPressed: _getNextGroups,
    );
  }

  _sendJoinRequest(group) async{
     bool proceed = await showCustomConfirmDialog(context,title: "Join Group ${group["name"]}",okMessage: "Yes",cancelMessage: "No");

     if(proceed){
       setState(() {
         isLoading = true;
       });

       Response response  =  await Http.doPost(url: "$apiUrl/group/join/request",data: {
          "uuid" : Auth.user.uuid,
          "group_uuid": group["uuid"]
        });

       setState(() {
         isLoading = false;
       });

       if(response.statusCode == 200 || response.statusCode == 201){
           Fluttertoast.showToast(
               msg: "Request Sent !",
               textColor: Colors.white,
               backgroundColor: Colors.black
           );

          showCustomAlertDialog(context,message: "Your Request to join the ${group["name"]} has been sent.Wait for the admin to approve your request");
       }
     }
  }

  //checks if the request to join group has been approved
  void refresh() async{
      setState(() {
        nextUrl = "$apiUrl/group/get/all";
      });
     _getNextGroups();
  }

  void _createGroupView(map) {
    groupBeingCreated = {
      "name":map["name"],
      "purpose":"${map["active_target"]["purpose"]}",
      "location":map["location"],
      "targetAmount":"${map["active_target"]["target_amount"]}",
      "minimumContribution":"${map["active_target"]["minimum_contribution"]}",
      "loanTerms": map["loan_terms"],
      "penalties": [{"Absent without apology":"4"},{"Absent without apology":"4"},{"Absent without apology":"4"},{"Absent without apology":"4"}] ,//map["penalties"],
      "constitution":map["constitution"],
      "admin":{
        "name":"${map["group_admin"]["name"]}",
        "email":"${map["group_admin"]["email"]}",
        "contact":"${map["group_admin"]["profile"]["contacts"][0]["contact"]}",
        "pic":"$baseUrl${map["group_admin"]["profile"]["profile_picture"]}",
        "description":"${map["group_admin"]["profile"]["description"]}"
       },
      "members":"${map["number_of_members"]}"
    };

    Navigator.of(context).push(MaterialPageRoute(builder: (context) => GroupPreviewPage(isHome: true,)));
  }

}
