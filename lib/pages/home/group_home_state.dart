import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senti/pages/base_page.dart';
import 'package:senti/pages/group/active_members.dart';
import 'package:senti/pages/group/add_new_member_base.dart';
import 'package:senti/pages/home/home_page.dart';
import 'package:senti/pages/group/group_earnings.dart';
import 'package:senti/pages/group/group_loans.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/display/circle_rectangular_label.dart';
import 'package:senti/widgets/display/percentage_progress_linear.dart';
import 'package:senti/widgets/text/text_list.dart';

class GroupHomePageState extends State<HomePage> {

  String message;
  var group;
  bool messageShown = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //todo used group 0
    if(Auth.user.groups[Auth.activeGroupIndex]["activated"] == false){
      message  = "Group ${Auth.user.groups[Auth.activeGroupIndex]["name"]} not yet activated";
    }else{
      message = Auth.user.groups[Auth.activeGroupIndex]["name"];
    }
    group = Auth.user.groups[Auth.activeGroupIndex];
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
     if(widget.newGroup == true && !messageShown){
         _showNewGroupModal(context);
         setState(() {
            messageShown = true;
         });
     }
     return Scaffold(
       backgroundColor: Colors.white,
       body: SafeArea(
         child: ListView(
           children: <Widget>[
             _buildHeader(context),
              _setPadding(Column(
               children: <Widget>[
                 //active members and monthly contribution
                 Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                       GestureDetector(
                         child: CircleThenRectangularLabel(
                           circleColor: Color.fromARGB(255, 121, 102, 254),
                           circleValue: "${group["number_of_members"] == null ? 1 : group["number_of_members"]}",
                           rectangleColor:  Color.fromARGB(255,237,236,255),
                           rectangleValue: "Members ",
                         ),
                         onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ActiveMembersPage())),
                           
                       ),
                       SizedBox(width: SizeConfig.blockSizeHorizontal * 1.5,),
                       CircleThenRectangularLabel(
                        circleColor: Color.fromARGB(255, 121, 102, 254),
                        circleValue: "${group["targets"][0]["minimum_contribution"]}",
                        rectangleColor:  Color.fromARGB(255,237,236,255),
                        rectangleValue: "Minimum Contribtution",
                      )
                    ],
                 ),
                 SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                 _buildProgressCard(),
                 SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                 //the three monetary cards in a row
                 Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _buildMonetaryReport("assets/earnings_home.png","300","Earnings",
                              () {Navigator.of(context).push(MaterialPageRoute(builder: (context) => GroupEarnings()));}),
                      _buildMonetaryReport("assets/loan1.png","300","Loans",
                              (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => GroupLoans())); }),
                      _buildMonetaryReport("assets/penalties_home.png","300","Penalties",
                          (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => GroupLoans()));}
                      )
                    ],
                 ),
                 SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                 Container(
                   width: SizeConfig.blockSizeHorizontal * 100,
                   padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal * 3,top:SizeConfig.blockSizeVertical * 1,bottom: SizeConfig.blockSizeVertical * 1),
                   child: Text.rich(TextSpan(
                     text: "Pending dues for this month ",
                     style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5,color: Colors.grey),
                     children: [
                       TextSpan(
                         text: "\$300",
                         style:TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 6,color: Colors.grey,fontWeight: FontWeight.bold),
                       )
                     ]
                   )),
                   decoration: BoxDecoration(
                      color: Color.fromARGB(255,237,236,255),
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      border: Border.all(color: Theme.of(context).primaryColor,width: SizeConfig.blockSizeHorizontal * 0.1)
                   ),
                 ),
                 SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                    Text("Location :",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal *4),),
                    Text(group["location"],style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal *4)),
                   ],
                 ),
                 SizedBox(height: SizeConfig.blockSizeVertical * 0.4,),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                     Text("Loan Terms (months) :",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal *4),),
                     Text(_getLoanTerms(),style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 4)),
                   ],
                 ),
                 SizedBox(height: SizeConfig.blockSizeVertical * 0.4,),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                     Text("No. of Members Confirmed : ",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 4),),
                     Text("${group["number_of_members"]}",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal *4)),
                   ],
                 )
               ],
             )),
             Container(
               width: SizeConfig.blockSizeHorizontal * 100,
               padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3,bottom:SizeConfig.blockSizeHorizontal * 1.5,top: SizeConfig.blockSizeHorizontal * 1.5),
               color: Color.fromARGB(255, 242,242,242),
               child: Text("Penalties Set Up",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 5),),
             ),
             _setPadding(Column(
               children: _setupPenalties(),
             ),top: 0.5),
             Container(
               width: SizeConfig.blockSizeHorizontal * 100,
               padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3,bottom:SizeConfig.blockSizeHorizontal * 1.5,top: SizeConfig.blockSizeHorizontal * 1.5),
               color: Theme.of(context).primaryColor,
               child: Text("Group Constitution",style: TextStyle(color: Colors.white,fontSize: SizeConfig.blockSizeHorizontal * 5),),
             ),
             _setPadding(AutoSizeText(
                 group["constitution"],
                 style: TextStyle(color: Colors.grey,height: 1.5),
             )),
             Container(
               width: SizeConfig.blockSizeHorizontal * 100,
               padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3,bottom:SizeConfig.blockSizeHorizontal * 1.5,top: SizeConfig.blockSizeHorizontal * 1.5),
               color: Theme.of(context).primaryColor,
               child: Text("Group Leader Information",style: TextStyle(color: Colors.white,fontSize: SizeConfig.blockSizeHorizontal * 5),),
             ),
             _setPadding(Column(
               mainAxisAlignment: MainAxisAlignment.start,
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                 Row(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                      Image(
                         image: Image.network("$baseUrl${group["group_admin"]["profile"]["profile_picture"]}").image,
                         width: SizeConfig.blockSizeHorizontal * 35,
                         height: SizeConfig.blockSizeHorizontal * 35,
                      ),
                     Padding(
                       padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 3),
                       child: Column(
                         mainAxisAlignment: MainAxisAlignment.start,
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: <Widget>[
                           Text(" ${group["group_admin"]["name"]}",style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 5),),
                           SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                           Row(
                             children: <Widget>[
                               Icon(FontAwesomeIcons.envelope,color: Colors.grey,size: SizeConfig.blockSizeHorizontal * 4,),
                               Text(" ${group["group_admin"]["email"]}",style: TextStyle(color: Colors.grey))
                             ],
                           ),
                           SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                           Row(
                             children: <Widget>[
                               Icon(Icons.local_phone,color: Colors.grey,size: SizeConfig.blockSizeHorizontal * 4),
                               Text(" ${group["group_admin"]["email"]}",style: TextStyle(color: Colors.grey))
                             ],
                           ),
                           SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                           Row(
                             children: <Widget>[
                               Icon(Icons.location_on,color: Colors.grey,size: SizeConfig.blockSizeHorizontal * 4),
                               Text(" ${group["location"]}",style: TextStyle(color: Colors.grey),)
                             ],
                           ),
                         ],
                       ),
                     )
                   ],
                 ),
                 SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                 AutoSizeText(
                   group["group_admin"]["profile"]["description"] ?? "None",
                   style: TextStyle(color: Colors.grey,height: 1.5,fontSize: SizeConfig.safeBlockHorizontal* 4),
                 )
               ],
             ),top : 2.0,)
           ],
         ),
       )
     );
  }


  //builds the top header part
  Widget _buildHeader(BuildContext context) {
    return Container(
        color: Color.fromARGB(255,245, 244, 250),
        width: SizeConfig.blockSizeHorizontal * 100,
        child: Padding(
           padding: EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 2,top:SizeConfig.blockSizeVertical * 2,left: SizeConfig.blockSizeVertical * 2,right: SizeConfig.blockSizeVertical * 2),
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
               Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[
                   Text("Hello, ${Auth.user.name}",style: TextStyle(color: Color.fromARGB(255, 79, 0, 115),fontSize: SizeConfig.blockSizeHorizontal * 6,fontWeight: FontWeight.bold),),
                   Icon(Icons.notifications ,color: Color.fromARGB(255, 121, 102, 254),size: SizeConfig.blockSizeHorizontal * 7)
                 ],
               ),
               SizedBox(height: SizeConfig.blockSizeVertical * 1,),
               Text(message,textAlign: TextAlign.left,),
               SizedBox(height: SizeConfig.blockSizeVertical * 1,),
               Auth.user.groups[Auth.activeGroupIndex]["activated"] && isGroupAdmin(group,Auth.user.uuid) ? Align(
                 alignment: Alignment.topRight,
                 child: Column(
                   children: <Widget>[
                       GestureDetector(
                         child:  Text("Add New Member",style: TextStyle(color: Colors.orangeAccent,fontWeight: FontWeight.bold,fontStyle: FontStyle.italic,fontSize: SizeConfig.blockSizeHorizontal * 4,decoration: TextDecoration.underline)),
                         onTap: (){ Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddNewMemberBase(groupUid: group["uuid"],) ));},
                       ),
                     ],
                 ),
               ):Text("")
             ],
           ),
        )
    );
  }

  Widget _buildProgressCard(){
    return Card(
      elevation: 3,
      child: Container(
        padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
        width: SizeConfig.blockSizeHorizontal * 200,
        child: Column(
           mainAxisAlignment: MainAxisAlignment.start,
           crossAxisAlignment: CrossAxisAlignment.start,
           children: <Widget>[
             Image.asset(
               "assets/watering.png",
               width: SizeConfig.blockSizeHorizontal * 30,
              ),
             SizedBox(height: SizeConfig.blockSizeVertical * 1,),
             Align(
               alignment: Alignment.topLeft,
               child: Text("Saving target",style: TextStyle(color: Colors.grey),),
             ),
             PercentageProgressIndicator(
               amountReached: 30000,
               fullAmount: 50000,
               indicatorColor: Color.fromARGB(255, 172, 147, 255),
               mainColor: Color.fromARGB(255, 228, 228, 228),
               height: SizeConfig.blockSizeVertical * 4,
               overlay: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[
                   Text("Amount collected",style: TextStyle(color: Colors.white),),
                   Text("\$50,000")
                 ],
               ),
             ),
             SizedBox(height: SizeConfig.blockSizeVertical * 1.5,),
             Align(
               alignment: Alignment.topLeft,
               child: Text("Your Savings",style: TextStyle(color: Colors.grey),),
             ),
             PercentageProgressIndicator(
               amountReached: 200,
               fullAmount: 500,
               indicatorColor: Color.fromARGB(255, 	246, 144, 33),
               mainColor: Color.fromARGB(255, 228, 228, 228),
               height: SizeConfig.blockSizeVertical * 4,
               overlay: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[
                   Text("Amount saved",style: TextStyle(color: Colors.white),),
                   Text("\$500")
                 ],
               ),
             ),
           ],
        ),
      ),
    );
  }

  //creates the penalties list
  List<Widget> _setupPenalties(){
    List<Widget> list = [
      ListText(list: [
        "Late Contribution Penalty",
        "Absent Penalty",
        "Late to a Meeting Penalty"
      ],
      textColor: Colors.grey,
      extras: [
        Text("\$5.00",style: TextStyle(fontWeight: FontWeight.bold),),
        Text("\$8.00",style: TextStyle(fontWeight: FontWeight.bold),),
        Text("\$5.00",style: TextStyle(fontWeight: FontWeight.bold),),
      ],),
    ];
    return list;
  }


  Widget _setPadding(Widget _widget,{top}){
     return Padding(
      padding: EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 2,top: top != null ? SizeConfig.blockSizeVertical * top : SizeConfig.blockSizeVertical * 2,left: SizeConfig.blockSizeVertical * 2,right: SizeConfig.blockSizeVertical * 2),
      child: _widget,
     );
  }


  //builds one of the three money display
  //loans, earnigs and penalties
  Widget   _buildMonetaryReport(String icon,amount,label,Function action){
    return GestureDetector(
      child: Container(
        width: SizeConfig.blockSizeHorizontal * 30,
        child:  Card(
            child: Padding(
              padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 2),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: Image.asset(icon,color: Theme.of(context).primaryColor,width: SizeConfig.blockSizeHorizontal * 7,height: SizeConfig.blockSizeHorizontal * 7,),
                  ),
                  SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                  Align(
                      alignment: Alignment.topRight,
                      child: Text("\$$amount",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold,fontSize: SizeConfig.blockSizeHorizontal * 6),)
                  ),
                  SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                  Align(
                      alignment: Alignment.topRight,
                      child: Text(label,)
                  ),
                ],
              ),
            )
        ),
      ),
      onTap: (){
        try{
          action();
        }catch(e){
          print(e);
        }
      },
    );
  }

  String _getLoanTerms(){
     String loans = "";

     if(group["loan_terms"] != null)group["loan_terms"].forEach((map){
       loans += "${map["months"]},";
     });
     return loans;
  }


  //shown when user joins group for the first time
  void _showNewGroupModal(BuildContext cont) async{
     Timer(Duration(milliseconds: 1000),() async{
       await showDialog(
           context: cont,
           builder: (BuildContext context){
         return AlertDialog(
           content: Container(
             width: SizeConfig.blockSizeHorizontal * 50,
             child: Column(
               mainAxisSize: MainAxisSize.min,
               children: <Widget>[
                 AutoSizeText("The Group has been created.Wait for the admin to activate group in less than 24hrs"),
                 MaterialButton(
                   minWidth: SizeConfig.blockSizeHorizontal * 35,
                   height: SizeConfig.blockSizeVertical * 6,
                   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                   color: Theme.of(context).primaryColor,
                   child: Text(
                     "Ok",
                     style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                   ),
                   onPressed: (){
                     Navigator.of(context).pop();
                   },
                 )
               ],
             ),
           )
         );
       }
       );
     });
  }

  //get the new details of group
  void refresh() async{
      //todo get group on refresh
    if(group["activated"] == false){
      Response response = await Http.doGet("$apiUrl/user/me");

      if(response.statusCode == 200){
        for(var grp in response.data["data"]["groups"]){
           if(grp["uuid"] == group["uuid"]){
             if(grp["activated"]){
               Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeBase()), (route) => false);
             }
             break;
           }
        }
      }
    }
  }

}


