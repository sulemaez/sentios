import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/pages/home/group_home_state.dart';
import 'package:senti/pages/home/no_group_state.dart';
import 'package:senti/services/auth.dart';


class HomePage extends StatefulWidget {

  bool newGroup;

  HomePage({bool newGroup}){
    this.newGroup = newGroup ?? false;
  }

  var _state;

   @override
  State<HomePage> createState(){
      if(Auth.user.groups.length > 0 && Auth.activeGroupIndex != -1) {
        _state = GroupHomePageState();
      }else{
        _state  = NoGroupHomePageState();
      }
      return _state;
   }

  void refreshPage() {
     if(_state is GroupHomePageState){
        GroupHomePageState st = _state;
        st.refresh();
     }else if(_state is NoGroupHomePageState){
        NoGroupHomePageState st = _state;
        st.refresh();
     }
  }
}



