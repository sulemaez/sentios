
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:pin_view/pin_view.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';


class LoanRequestsPage extends StatefulWidget {
  @override
  _LoanRequestsPageState createState() => _LoanRequestsPageState();
}

class _LoanRequestsPageState extends State<LoanRequestsPage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: FutureBuilder(
        builder: (BuildContext context,AsyncSnapshot snapshot){
          if (snapshot.connectionState == ConnectionState.none &&
              snapshot.hasData == null || snapshot.data == null) {
            return buildLoader(context);
          }

          if(snapshot.data.length < 1){
            return Center(
              child: Column(
                children: <Widget>[
                  SizedBox(height: SizeConfig.blockSizeVertical * 20,),
                  Icon(Icons.group,color: Theme.of(context).primaryColor,size: SizeConfig.safeBlockHorizontal * 15,),
                  SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                  Text("No Requests !",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.safeBlockHorizontal * 4),)
                ],
              ),
            );
          }

          return Padding(
            padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 4),
            child: ListView.builder(
              shrinkWrap: true,
              itemBuilder: (context,index){
                return _buildListItem(snapshot.data[index]);
              },
              itemCount: snapshot.data.length,
            ),
          );
        },
        future: _getList(),
      ),
    );
  }

  Future<List<dynamic>> _getList() async{
     return [
       {
         "message":"Julia is looking for your confirmation for oan amount of \$7500 for 3 months",
         "approvals": "She requires 2 more approvals",
       },
       {
         "message":"William is looking for your confirmation for oan amount of \$7500 for 3 months",
         "approvals": "She requires 1 more approvals",
       },
       {
         "message":"Jessica's loan has been approved already.Sill you can add your approval",
         "approvals": "",
       }
     ];
  }

  Widget _buildListItem(data) {
     return Container(
       width: SizeConfig.blockSizeHorizontal * 100,
       padding: EdgeInsets.only(left:SizeConfig.safeBlockHorizontal * 3,right: SizeConfig.safeBlockHorizontal * 1),
       child: Column(
         mainAxisAlignment: MainAxisAlignment.start,
         crossAxisAlignment: CrossAxisAlignment.start,
         children: <Widget>[
           ListTile(
             leading: Container(
               width: 5,
               height: 5,
               decoration: BoxDecoration(
                   color: Colors.orange,
                   shape: BoxShape.circle
               ),
             ),
             title:Align(
               alignment: Alignment(-(SizeConfig.blockSizeHorizontal * 80), 0),
               child: AutoSizeText(data["message"],style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4,),),
             ),
             contentPadding: EdgeInsets.all(0),
             dense: false,
           ),
           data["approvals"] == "" ? Text(""):
               Text(data["approvals"],style: TextStyle(color: Theme.of(context).primaryColor,fontStyle: FontStyle.italic,fontSize: SizeConfig.blockSizeHorizontal * 4),),
           Align(
             alignment: Alignment.topRight,
             child: GestureDetector(
               child: Text("Approve",style: TextStyle(color: Colors.orange,fontStyle: FontStyle.italic,fontSize: SizeConfig.blockSizeHorizontal * 4),),
               onTap: (){
                 _showApprovalDialog();
               },
             ),
           ),
           SizedBox(height: SizeConfig.blockSizeVertical * 1,),
           Divider(color: Colors.grey,height: 0.5,)
         ],
       ),
       decoration: BoxDecoration(
         color: data["approvals"] == "" ? Colors.white : Color.fromARGB(255, 244, 244, 244),
       ),
     );
  }

  void _showApprovalDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context){
      return AlertDialog(
        content: Container(
          width: SizeConfig.blockSizeHorizontal * 60,
          child: Center(
            heightFactor: 1,
            child:Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AutoSizeText("You must enter your 4 digit secret PIN in order to grant permission for loan approval",textAlign: TextAlign.center,),
                SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                PinView(
                  count: 4,
                  autoFocusFirstField: true,
                  obscureText: true,
                  submit: (String pin){
                    print("PIN $pin");
                  },
                ),
                SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                MaterialButton(
                  minWidth: SizeConfig.blockSizeHorizontal * 30,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    "Approve",
                    style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                  ),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                ),
                SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                GestureDetector(
                  child: Text("Cancel",style: TextStyle(color: Theme.of(context).primaryColor,decoration: TextDecoration.underline),),
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ),
        ),
      );
    });
  }
}
