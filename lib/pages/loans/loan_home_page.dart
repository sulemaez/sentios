
import 'package:flutter/material.dart';
import 'package:senti/pages/loans/loan_page.dart';
import 'package:senti/pages/loans/loan_requests.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';

class LoanHomePage extends StatefulWidget {

  LoanHomePage();

   @override
   createState() => _LoanPageState();
}

class _LoanPageState extends State<LoanHomePage> with SingleTickerProviderStateMixin{
  TabController _tabController ;
  LoanRequestsPage loanRequestsPage;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 2,vsync: this, initialIndex: 0);
    _tabController.addListener((){
      setState(() {
      });
    });
    loanRequestsPage = LoanRequestsPage();
  }

  void _loanTap(){
    setState(() {
      _tabController.index = 0;
    });
  }
  void _requestTap(){
    setState(() {
      _tabController.index = 1;
    });
  }


  @override
  Widget build(BuildContext context) {
    if(Auth.user.groups.length < 1 || Auth.activeGroupIndex == -1) return joinGroupFirst(context);
    if(!Auth.user.groups[Auth.activeGroupIndex]["activated"]) return buildGroupNotActive(context);
    
    return Column(
      children: <Widget>[
        Padding( padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 6),),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                    child: Container(
                        child:  Text("Loans",style: TextStyle(color: _tabController.index == 0 ? Theme.of(context).primaryColor : Theme.of(context).secondaryHeaderColor,fontSize: 20,fontWeight: FontWeight.bold),)
                    ),
                    onTap:_loanTap
                ),
                GestureDetector(
                    child:Container(
                        child:  Text("Requests",style: TextStyle(color: _tabController.index == 1 ? Theme.of(context).primaryColor : Theme.of(context).secondaryHeaderColor,fontSize: 20,fontWeight: FontWeight.bold),)
                    ),
                    onTap:_requestTap
                ),
              ],
            ),
        Expanded(
          child: TabBarView(
              controller: _tabController,
              children: [
                Padding(
                   child:  ListView(
                   children:_getChildren(context),
                ),
                padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 4,right: SizeConfig.blockSizeHorizontal * 4),
               ),
                loanRequestsPage
              ]),
        )
      ],
    );
  }

  List<Widget> _getChildren(BuildContext context){
      return getLoanContent(context);
  }
}
