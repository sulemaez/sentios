
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/pages/loans/loan_empty.dart';
import 'package:senti/pages/loans/request_loan.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/display/loan_info.dart';

class LoanPage extends StatefulWidget {
  final bool valid;

  LoanPage({this.valid});

  @override
  _LoanPageState createState() => _LoanPageState();
}

class _LoanPageState extends State<LoanPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Loans",context),
      body: Padding(
        child: ListView(
          children:!widget.valid ? [getLoanEmptyContent(context)]: getLoanContent(context),
        ),
        padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 4,right: SizeConfig.blockSizeHorizontal * 4),
      ),
    );
  }

}

List<Widget> getLoanContent(BuildContext context) {
  return [
    LoanInfoWidget(),
    SizedBox(height: SizeConfig.blockSizeVertical * 3,),
    LoanInfoWidget(paid: true,),
    SizedBox(height: SizeConfig.blockSizeVertical * 3,),
    LoanInfoWidget(approvedPending: true,),
    Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Auth.user.groups[Auth.activeGroupIndex]["group_admin"]["uuid"] == Auth.user.uuid ? MaterialButton(
            minWidth: SizeConfig.blockSizeHorizontal * 50,
            height: SizeConfig.blockSizeVertical * 6,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
            color: Colors.white,
            child: Text(
              "Request for others",
              style: TextStyle(color: Theme.of(context).primaryColor,fontWeight: FontWeight.bold),
            ),
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => RequestLoanPage(other: true,)));
            },
          ):Text(""),
          SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
          MaterialButton(
            minWidth: SizeConfig.blockSizeHorizontal * 30,
            height: SizeConfig.blockSizeVertical * 6,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
            color: Theme.of(context).primaryColor,
            child: Text(
              "Request",
              style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
            ),
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => RequestLoanPage()));
            },
          )
        ],
      ),
      padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
    )
  ];
}
