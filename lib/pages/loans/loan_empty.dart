
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:senti/pages/loans/request_loan.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';

class LoanEmptyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Loan",context),
      body: getLoanEmptyContent(context),
    );
  }

}

Widget getLoanEmptyContent(BuildContext context){
  return Container(
    width: SizeConfig.blockSizeHorizontal * 100,
    height: SizeConfig.blockSizeVertical * 100,
    child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: SizeConfig.blockSizeHorizontal * 50,
            child: Image.asset("assets/loan_empty.png",),
          ),
          Text("You haven't applied any loan yet",style: TextStyle(color: Theme.of(context).primaryColor,
              fontSize: SizeConfig.blockSizeHorizontal * 5,fontWeight: FontWeight.bold),
          ),
          SizedBox(height: SizeConfig.blockSizeVertical * 20,),
          MaterialButton(
            minWidth: SizeConfig.blockSizeHorizontal * 50,
            height: SizeConfig.blockSizeVertical * 6,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
            color: Theme.of(context).primaryColor,
            child: Text(
              "Request for Loan",
              style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
            ),
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => RequestLoanPage() ));
            },
          ),
        ],
      ),
    ),
  );
}
