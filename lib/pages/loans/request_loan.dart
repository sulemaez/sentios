
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/text/text_form_widgets.dart';

class RequestLoanPage extends StatefulWidget {

  final bool other;

  const RequestLoanPage({Key key, this.other = false}) : super(key: key);

  @override
  _RequestLoanPageState createState() => _RequestLoanPageState();
}

class _RequestLoanPageState extends State<RequestLoanPage> {

  //String nameValue = "Jessica";

  Map<String,String> loanTerms = {};
  String activeLoanTermKey = "Select Loan Term";
  double  interestAmount = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Auth.user.groups[Auth.activeGroupIndex]["loan_terms"].forEach((map){
      loanTerms["${map["months"]} Months"] = "${map["interest"]}";
    });

  }


  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: MyBar("Request Loan",context),
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          ListView(
            shrinkWrap: true,
            children: <Widget>[
               Padding(
                 padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 4),
                 child:  Column(
                   crossAxisAlignment: CrossAxisAlignment.stretch,
                   children: <Widget>[
                     widget.other?  Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                         Text("Name :",textAlign: TextAlign.left,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4,color: Theme.of(context).primaryColor),),
                         SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                         Container(
                           height: SizeConfig.blockSizeVertical * 7,
                           decoration: BoxDecoration(
                             color: Color.fromRGBO(244, 244, 244,1),
                             borderRadius: BorderRadius.circular(6.0),
                           ),
                           child:Center(
                             child:  DropdownButton<String>(
                               isDense: true,
                               value: "J",
                               isExpanded: true,
                               icon: Icon(Icons.keyboard_arrow_down),
                               iconSize: 24,
                               elevation: 16,
                               underline: Container(
                                 height: 30,
                                 width: SizeConfig.blockSizeHorizontal * 100,
                               ),
                               onChanged:(String s){},
                               items: ["Jessica","Tom","Collins"]
                                   .map<DropdownMenuItem<String>>((String value) {
                                 return DropdownMenuItem<String>(
                                   value: value,
                                   child: Text("  $value"),
                                 );
                               })
                                   .toList(),
                             ),
                           )
                         ),
                         SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                       ],
                     ): Text(""),
                   //  Text("${widget.other ? "$nameValue is":"You are"} eligible to apply for a maximum  loan of \$1500",style: TextStyle(color: Theme.of(context).primaryColor,fontStyle: FontStyle.italic),),
                     SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                     Row(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       mainAxisSize: MainAxisSize.min,
                       children: <Widget>[
                         Container(
                           width: SizeConfig.blockSizeHorizontal * 45,
                           child: PlainTextFormField(
                             "Loan Amount:",
                             keyboardType: TextInputType.number,
                             value: "",
                             errorText: "",
                             actions: {
                               "saved":(String loan){},
                               "valid":(String loan){},
                             },
                           ),
                         ),
                         Column(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: <Widget>[
                             SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                             Text("Select Loan Term :",textAlign: TextAlign.left,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4,color: Theme.of(context).primaryColor),),
                             SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                             Container(
                                 height: SizeConfig.blockSizeVertical * 7,
                                 width: SizeConfig.blockSizeHorizontal * 45,
                                 decoration: BoxDecoration(
                                   color: Color.fromRGBO(244, 244, 244,1),
                                   borderRadius: BorderRadius.circular(6.0),
                                 ),
                                 child:Center(
                                   child:  DropdownButton<String>(
                                     isDense: true,
                                     value: activeLoanTermKey,
                                     isExpanded: true,
                                     icon: Icon(Icons.keyboard_arrow_down),
                                     iconSize: 24,
                                     elevation: 16,
                                     underline: Container(
                                       height: 30,
                                       width: SizeConfig.blockSizeHorizontal * 100,
                                     ),
                                     onChanged:(String s){
                                       print(s);
                                       setState(() {
                                          activeLoanTermKey = s;
                                          interestAmount = double.parse(loanTerms[s]);
                                       });

                                     },
                                     items:_getLoanTerms()

                                   ),
                                 )
                             )
                           ],
                         )
                       ],
                     ),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: <Widget>[
                         Container(
                           width: SizeConfig.blockSizeHorizontal * 45,
                           child: PlainTextFormField(
                             "Rate of interest: ",
                             value: interestAmount.toString(),
                             enabled: false,
                           ),
                         ),
                         Container(
                           width: SizeConfig.blockSizeHorizontal * 45,
                           child: PlainTextFormField(
                             "EMI Amount:",
                             keyboardType: TextInputType.number,
                             value: "",
                           ),
                         ),
                       ],
                     ),
                   PlainTextFormField(
                         "Total:",
                         keyboardType: TextInputType.number,
                         value: "",
                         enabled: false,
                     ),
                    Center(
                      child: MaterialButton(
                        minWidth: SizeConfig.blockSizeHorizontal * 50,
                        height: SizeConfig.blockSizeVertical * 6,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          "Request",
                          style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                        ),
                        onPressed: (){

                        },
                      ),
                    )
                   ],
                 ),
               )
            ],
          )
        ],
      ),
    );
  }

  List<DropdownMenuItem<String>> _getLoanTerms(){
    List<DropdownMenuItem<String>> items = [
      DropdownMenuItem<String>(
        value: "Select Loan Term",
        child: Text("Select Loan Term"),
      )
    ];
    loanTerms.forEach((key,value){
      items.add(DropdownMenuItem<String>(
        value: key,
        child: Text("$key"),
      ));
    });

    return items;
  }
}
