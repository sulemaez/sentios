import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:senti/pages/auth/login_page.dart';
import 'package:senti/pages/auth/register_page.dart';
import 'package:senti/utils/size_config.dart';

class LoginRegister extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => LoginRegisterState();
}

class LoginRegisterState extends State<LoginRegister> with SingleTickerProviderStateMixin{

  TabController _tabController ;

  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    _tabController = TabController(length: 2, vsync: this,initialIndex: 0);
    _tabController.addListener((){
        setState(() {
        });
     });

  }

  void _registerTap(){
    setState(() {
      _tabController.index = 1;
    });
  }

  void _loginTap(){
    setState(() {
      _tabController.index = 0;
    });
  }


  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // TODO: implement build
    return  Scaffold(
        body: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding( padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 10),),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  GestureDetector(
                      child: Container(
                          child:  Text("Login",style: TextStyle(color: _tabController.index == 0 ? Theme.of(context).primaryColor : Theme.of(context).secondaryHeaderColor,fontSize: 20,fontWeight: FontWeight.bold),)
                      ),
                      onTap: _loginTap
                  ),
                  GestureDetector(
                      child:Container(
                          child:  Text("Register",style: TextStyle(color: _tabController.index == 1 ? Theme.of(context).primaryColor : Theme.of(context).secondaryHeaderColor,fontSize: 20,fontWeight: FontWeight.bold),)
                      ),
                      onTap: _registerTap
                  ),
                ],
              ),
              Expanded(
                child: TabBarView(
                    controller: _tabController,
                    children: [
                      LoginPage(_tabController),
                      RegisterPage(_tabController)
                    ]),
              )
            ],
          ),
        )
    );
  }
}



