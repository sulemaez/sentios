
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senti/blocs/signup_bloc/signup_page_bloc.dart';
import 'package:senti/blocs/signup_bloc/signup_page_events.dart';
import 'package:senti/blocs/signup_bloc/signup_page_state.dart';
import 'package:senti/pages/info/privacy_policy.dart';
import 'package:senti/pages/info/terms_page.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/functional/register_widget.dart';

class RegisterPage extends StatefulWidget {
  final TabController tabController;

  RegisterPage(this.tabController);

  @override
  State<StatefulWidget> createState() => RegisterPageState();
}

class RegisterPageState extends State<RegisterPage> {

  //recognizes taps on privacy text and terms and condition text
  TapGestureRecognizer _termsRecognizer;
  TapGestureRecognizer _privacyRecognizer;
  SignUpPageBloc _signUpPageBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
     //go to the terms and condition page
     _termsRecognizer  = TapGestureRecognizer()
       ..onTap = (){
         Navigator.of(context).push(MaterialPageRoute(builder: (context) => TermsAndConditionsPage()));
       };

    //go to the privacy policy page
    _privacyRecognizer = TapGestureRecognizer()
      ..onTap = (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => PrivacyPolicyPage()));
      };


     _signUpPageBloc = SignUpPageBloc(memberRegistration: false);

  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext con) {
    SizeConfig().init(con);
    // TODO: implement build
    return BlocBuilder(
        bloc: _signUpPageBloc,
       builder: (BuildContext context,SignUpPageState state){
         if(state.isSuccess)_signUpPageBloc.add(SuccessEvent(widget, context));
         return Stack(
           children: <Widget>[
             Container(
               color: Colors.white,
               child:ListView(
                 shrinkWrap: true,
                 children: <Widget>[
                   RegisterWidget(bloc: _signUpPageBloc,state: state,),
                   SizedBox(
                     height: SizeConfig.blockSizeVertical * 9,
                   ),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: <Widget>[
                       Text(
                         "Already have an account ?",
                         style:
                         TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5,color: Colors.black38,fontWeight: FontWeight.bold),
                       ),
                       SizedBox(
                         width: SizeConfig.blockSizeHorizontal * 2,
                       ),
                       GestureDetector(
                         onTap: (){
                           setState(() {
                             widget.tabController.index = 0;
                           });
                         },
                         child: Text(
                           "Sign In",
                           style: TextStyle(
                             fontSize: SizeConfig.blockSizeHorizontal * 5,
                             color: Colors.orangeAccent,
                           ),
                         ),
                       )
                     ],
                   ),
                   SizedBox(
                     height: SizeConfig.blockSizeVertical * 5,
                   ),
                   Padding(
                       padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 5,right : SizeConfig.blockSizeHorizontal * 5),
                       child: RichText(
                         textAlign: TextAlign.center,
                         text: TextSpan(
                           text: "By Signing in you are agreeing to the",
                           style: TextStyle(color: Colors.black38,),
                           children: <TextSpan>[
                             TextSpan(
                                 text : " Terms and Conditions", style: TextStyle(color: Colors.orangeAccent),
                                 recognizer: _termsRecognizer
                             ) ,
                             TextSpan(text :" and the"),
                             TextSpan(
                                 text : " Privacy Policy", style: TextStyle(color: Colors.orangeAccent),
                                 recognizer: _privacyRecognizer
                             )
                           ],
                         ),
                       )
                   ),
                   SizedBox( height: SizeConfig.blockSizeVertical * 8,)
                 ],
               ),
             ),
             state.isLoading ? buildLoader(context) : Text("")
           ],
         );
       },
    );
  }

}
