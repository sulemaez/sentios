
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';

class ForgotPasswordPage extends StatefulWidget {

  ForgotPasswordPage();

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
       backgroundColor: Colors.white,
       body: Container(
          height: SizeConfig.screenHeight,
          width: SizeConfig.screenWidth,
          padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 7),
          child: Form(
            child: Center(
              child:  Form(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: SizeConfig.blockSizeVertical * 4,),
                     Text("Forgot Password ?",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 6,fontWeight: FontWeight.bold)),
                     SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                     AutoSizeText("Enter Your email below to receive your \n Password reset instruction",textAlign: TextAlign.center,style: TextStyle(color: Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 1,),
                    _TextFieldWithInnerLabel(head:"Email"),
                    SizedBox(height: SizeConfig.blockSizeVertical * 3,),
                    MaterialButton(
                      minWidth: SizeConfig.blockSizeHorizontal * 50,
                      height: SizeConfig.blockSizeVertical * 6,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                      color: Theme.of(context).primaryColor,
                      child: Text(
                        "SEND PASSWORD",
                        style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                      ),
                      onPressed: (){
                        print("edit");
                      },
                    ),
                    SizedBox(height: SizeConfig.blockSizeVertical * 3,),
                    GestureDetector(
                      child: Text("Close",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 5,decoration:  TextDecoration.underline),),
                      onTap: () => Navigator.of(context).pop(),
                    ),
                    SizedBox(height: SizeConfig.blockSizeVertical * 20,),
                  ],
                ),
              ),
            ),
          )
        )
    );
  }
}

class _TextFieldWithInnerLabel extends StatefulWidget {
  final String head;

  _TextFieldWithInnerLabel({this.head});

  @override
  _TextFieldWithInnerLabelState createState() => _TextFieldWithInnerLabelState();
}

class _TextFieldWithInnerLabelState extends State<_TextFieldWithInnerLabel> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: SizeConfig.blockSizeVertical * 3,),
        Container(
          width: SizeConfig.blockSizeHorizontal * 100,
          padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 3),
         // labelText: widget.head,
          //labelStyle: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 4),

          child:Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 2),
                child:  Text(widget.head,style:TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 4),),
              ),
              TextFormField(
                style:new TextStyle(
                  height: SizeConfig.blockSizeVertical * 0.1,
                  fontSize: SizeConfig.blockSizeHorizontal * 5,
                ) ,
                decoration: InputDecoration(
                  filled: true,
                  hintText: "someone@gmail.com",
                  fillColor: Color.fromRGBO(244, 244, 244,1),
                  border:OutlineInputBorder(
                    borderSide: BorderSide(
                        style: BorderStyle.none,
                        width: 0.0
                    ),
                  ),
                ),
              )
            ],
          ),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(6)),
              color: Color.fromRGBO(244, 244, 244,1)
          ),
        )
      ],
    );
  }
}
