
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pin_view/pin_view.dart';
import 'package:senti/utils/size_config.dart';

class OtpPage extends StatefulWidget {
  @override
  State createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body:  ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              width: SizeConfig.blockSizeHorizontal * 100,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: SizeConfig.blockSizeVertical * 10,),
                    Text("Please enter the OTP",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 6),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 6,),
                    Container(
                      width: SizeConfig.blockSizeHorizontal * 69,
                      child: PinView(
                        count: 4,
                        autoFocusFirstField: true,
                        obscureText: true,
                        submit: (String pin){
                           print("PIN $pin");
                        },
                      )
                    ),
                    SizedBox(height: SizeConfig.blockSizeVertical * 10,),
                    MaterialButton(
                      minWidth: SizeConfig.blockSizeHorizontal * 50,
                      height: SizeConfig.blockSizeVertical * 6,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                      color: Theme.of(context).primaryColor,
                      child: Text(
                        "Confirm",
                        style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                      ),
                      onPressed: (){

                      },
                    ),
                    SizedBox(height: SizeConfig.blockSizeVertical * 3,),
                    Text("If you did not receive OTP",style: TextStyle(color: Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                    Text("Resend OTP",style: TextStyle(color: Theme.of(context).primaryColor,decoration: TextDecoration.underline,fontSize: SizeConfig.blockSizeHorizontal * 5),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 2,),
                    Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        SpinKitFadingCircle(
                          color: Theme.of(context).primaryColor,
                          size: SizeConfig.blockSizeHorizontal * 20,
                        ),
                        Text("35",style: TextStyle(fontWeight: FontWeight.bold,fontSize: SizeConfig.safeBlockHorizontal * 5),)
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        )
      ),
    );
  }

}
