import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/models/user.dart';
import 'package:senti/pages/auth/login_register.dart';
import 'package:senti/pages/base_page.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/shared_preferences.dart';

class SplashScreen  extends StatefulWidget{

  @override
  State<StatefulWidget> createState() => SplashScreenState();

}

class SplashScreenState extends State<SplashScreen>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
     //check if logged in
    _checkLoggedIn();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
       backgroundColor: Colors.white,
       body: Center(
          child: Image.asset("assets/logo.png"),
       ),
    );
  }

  void _checkLoggedIn() async{
    try{
      //check if user is saved in preferences
      String response = await getPreference("user");

      MaterialPageRoute route;
      Duration duration;
      //if not saved set route to login
      if(response == null || response == ""){
        print("HUKU");
        route = MaterialPageRoute(
          builder: (context) => LoginRegister(),
        );
        duration = Duration(seconds: 1);
      }else{
        //if set set the user and set route to homepage
        Auth.user = User.fromJson(jsonDecode(response));

        //get the updated user details
        if(await checkNet()){

          Response resp = await Http.doGet("$apiUrl/user/me");
          print("LOL");
          print(resp.data);
          print(resp.statusMessage);
          print(resp.statusCode);
          if(resp.statusCode  == 200){
            print("p");
            //create an user
            Auth.user = User(
                email:resp.data["data"]['email'],
                uuid: resp.data["data"]["uuid"],
                name: resp.data["data"]['name'],
                contact: resp.data["data"]['profile']['contacts'][0]['contact'],
                description: resp.data["data"]['profile']['description'],
                pic: "$baseUrl/${resp.data["data"]['profile']['profile_picture']}",
                token: Auth.user.token,
                groups:resp.data["data"]['groups'],
                dob : resp.data["data"]['profile']['dob']
            );
          }
        }

        route = MaterialPageRoute(
          builder: (context) => HomeBase(),
        );

        duration = Duration(milliseconds: 60);
      }
      //got to the specified page
      Timer(duration,()=>{
        Navigator.pushAndRemoveUntil( context, route, (Route<dynamic> r) => false)
      });
    }catch(e){
      print(e);
    }
  }
}