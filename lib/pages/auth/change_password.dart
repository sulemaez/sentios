
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senti/blocs/change_password_bloc/bloc.dart';
import 'package:senti/blocs/change_password_bloc/event.dart';
import 'package:senti/blocs/change_password_bloc/state.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/utils/validators.dart';
import 'package:senti/widgets/appbar_all.dart';
import 'package:senti/widgets/text/text_field_innerlabel.dart';

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {

  ChangePasswordBloc _bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc = ChangePasswordBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyBar("Change Password",context),
      body: BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext context,ChangePasswordState state){
           if(state.isSuccess)_bloc.add(SuccessEvent(context));
           return Form(
             key: state.formKey,
             child: Stack(
               children: <Widget>[
                 ListView(
                   children: <Widget>[
                     Container(
                       color: Colors.white,
                       padding: EdgeInsets.only(
                           right: SizeConfig.blockSizeHorizontal * 7,
                           left: SizeConfig.blockSizeHorizontal * 7
                       ),
                       width: SizeConfig.blockSizeHorizontal * 100,
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.center,
                         children: <Widget>[
                           TextFieldWithInnerLabel(head: "Old Password",
                             errorText: state.oldPasswordError,
                             value: state.oldPassword,
                             obscureText: true,
                             actions: {
                               "saved":(String pass) => _bloc.add(SaveFieldEvent(type: "oldPassword",value: pass)),
                               "valid" : validatePassword,
                             },
                           ),
                           TextFieldWithInnerLabel(head: "New Password",
                             errorText: state.newPasswordError,
                             value: state.newPassword,
                             obscureText: true,
                             actions: {
                               "saved":(String pass) => _bloc.add(SaveFieldEvent(type: "newPassword",value: pass)),
                               "valid" : validatePassword,
                               "changed" :(String pass) => _bloc.add(SaveFieldEvent(type:"newPassword",value: pass,formSave  : false))
                             },
                           ),
                           TextFieldWithInnerLabel(head: "Confirm  Password",
                               errorText: state.confirmPasswordError,
                               value: state.confirmPassword,
                               obscureText: true,
                               actions: {
                                 "saved":(String pass) => _bloc.add(SaveFieldEvent(type: "confirmPassword",value: pass)),
                                 "valid" : (String pass) => validatePasswordRepeat(pass, state.newPassword),
                               }
                           ),
                           SizedBox(height: SizeConfig.blockSizeVertical * 4,),
                           MaterialButton(
                             minWidth: SizeConfig.blockSizeHorizontal * 50,
                             height: SizeConfig.blockSizeVertical * 6,
                             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                             color: Theme.of(context).primaryColor,
                             child: Text(
                               "UPDATE",
                               style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                             ),
                             onPressed: () => _bloc.add(SaveFormEvent()),
                           ),
                           SizedBox(height: SizeConfig.blockSizeVertical * 3,),
                           GestureDetector(
                             child: Text("Cancel",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 5,decoration:  TextDecoration.underline),),
                             onTap: () => Navigator.of(context).pop(),
                           )
                         ],
                       ),
                     )
                   ],
                 ),
                 state.isLoading ? buildLoader(context) : Text("")
               ],
             ),
           );
        },
      ),
    );
  }
}


