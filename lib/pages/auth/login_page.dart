import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senti/blocs/login_bloc/login_bloc.dart';
import 'package:senti/blocs/login_bloc/login_event.dart';
import 'package:senti/blocs/login_bloc/login_state.dart';
import 'package:senti/utils/globals/widgets.dart';
import 'package:senti/utils/validators.dart';
import 'package:senti/pages/auth/forgot_password.dart';
import 'package:senti/utils/size_config.dart';

class LoginPage extends StatefulWidget{
  final TabController tabController;

  LoginPage(this.tabController);

  @override
  State<StatefulWidget> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> with TickerProviderStateMixin{

  LoginBloc _bloc;

  LoginPageState();

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    _bloc = LoginBloc();
  }

  @override
  Widget build(BuildContext contextMain) {
    SizeConfig().init(contextMain);
    // TODO: implement build
    return  Container(
        color: Colors.white,
        child: BlocBuilder(
          bloc: _bloc,
          builder: (context,LoginState state){
            if(state.isSuccess)_bloc.add(SuccessEvent(context));

            return  Stack(
               children: <Widget>[
                 ListView(
                   shrinkWrap: true,
                   children: <Widget>[
                     Stack(
                       alignment: Alignment.topCenter,
                       overflow: Overflow.visible,
                       children: <Widget>[
                         Card(
                           color: Color.fromRGBO(244, 244, 244,1),
                           elevation: 0.5,
                           shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(8.0),
                           ),
                           child: Container(
                               width: SizeConfig.blockSizeHorizontal * 80,
                               child: Padding(
                                 padding: EdgeInsets.only(
                                     top: SizeConfig.blockSizeVertical * 3,
                                     left: SizeConfig.blockSizeHorizontal * 5,
                                     bottom: SizeConfig.blockSizeVertical * 8,
                                     right: SizeConfig.blockSizeHorizontal * 5
                                 ),
                                 child: Form(
                                   key: state.formKey,
                                   child: Column(
                                     children: <Widget>[
                                       Row(
                                         children: <Widget>[
                                           Icon(Icons.email,color: Colors.orangeAccent,size: SizeConfig.blockSizeHorizontal * 4,)
                                           ,Padding( padding: EdgeInsets.only(left: 5),)
                                           ,Text("Email",style: TextStyle(color: Theme.of(context).primaryColor , fontSize: 16),),
                                         ],
                                       ),
                                       Container(
                                         width: SizeConfig.blockSizeHorizontal * 70,
                                         child: TextFormField(
                                           decoration:  InputDecoration(
                                             enabledBorder: UnderlineInputBorder(
                                                 borderSide: BorderSide(color: Colors.grey)
                                             ),
                                             focusedBorder: UnderlineInputBorder(
                                                 borderSide: BorderSide(color: Colors.grey)
                                             ),
                                             hintText:"someone@gmail.com",
                                             fillColor: Colors.white,
                                           ),
                                           validator: validateEmail,
                                           onSaved: (String email) => _bloc.add(SaveFieldEvent(type:"email",value:email)),
                                         ),
                                       ),
                                       Padding(
                                         padding: EdgeInsets.only(top: 20),
                                       ),
                                       Row(
                                         children: <Widget>[
                                           Icon(Icons.lock,color: Colors.orangeAccent,size: SizeConfig.blockSizeHorizontal * 4,)
                                           ,Padding( padding: EdgeInsets.only(left: 5),)
                                           ,Text("Password",style: TextStyle(color: Theme.of(context).primaryColor , fontSize: 16),),
                                         ],
                                       ),
                                       Container(
                                         width: SizeConfig.blockSizeHorizontal * 70,
                                         child: TextFormField(
                                           decoration:  InputDecoration(
                                             enabledBorder: UnderlineInputBorder(
                                                 borderSide: BorderSide(color: Colors.grey)
                                             ),
                                             focusedBorder: UnderlineInputBorder(
                                                 borderSide: BorderSide(color: Colors.grey)
                                             ),
                                             fillColor: Colors.white,
                                           ),
                                           obscureText:true,
                                           validator: validatePassword,
                                           onSaved: (String pass) => _bloc.add(SaveFieldEvent(type:"password",value:pass)),
                                         ),
                                       ),
                                       Padding( padding: EdgeInsets.only(top: 10),),
                                       Container(
                                         width: SizeConfig.blockSizeHorizontal * 70,
                                         child: GestureDetector(
                                           child: Text("Forgot Password ?",style: TextStyle( color: Theme.of(context).primaryColor,fontStyle: FontStyle.italic,fontSize: 16,fontWeight: FontWeight.bold,),textAlign: TextAlign.right,),
                                           onTap: (){
                                             try{
                                               Navigator.of(context).push(MaterialPageRoute(builder: (context) => ForgotPasswordPage()));
                                             }catch(e){
                                               print(e);
                                             }
                                           },
                                         ),
                                       )
                                     ],
                                   ),
                                 ),
                               )
                           ),
                         ),
                         Positioned(
                           bottom:-25,
                           child: Align(
                             alignment: Alignment.bottomCenter,
                             child:  MaterialButton(
                               minWidth: SizeConfig.blockSizeHorizontal * 50,
                               height: SizeConfig.blockSizeVertical * 6,
                               shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
                               color: Theme.of(context).primaryColor,
                               child: Text(
                                 "SIGN IN",
                                 style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                               ),
                               onPressed: () => _bloc.add(SaveFormEvent(context)),
                             ),
                           ),
                         ),
                       ],
                     ),
                     SizedBox( height: SizeConfig.blockSizeVertical * 9,),
                     Text("or Login with",textAlign: TextAlign.center,style: TextStyle(color: Colors.grey , fontSize: SizeConfig.blockSizeHorizontal * 4),),
                     SizedBox( height: SizeConfig.blockSizeVertical * 3,),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                         FlatButton(
                           padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 4),
                           shape: CircleBorder(
                             side: BorderSide(color: Theme.of(context).primaryColor),
                           ),
                           child: Icon(FontAwesomeIcons.facebookF,color: Theme.of(context).primaryColor,),
                           onPressed: () {},
                         ),
                         FlatButton(
                           padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 4),
                           shape: CircleBorder(
                               side: BorderSide(color: Theme.of(context).primaryColor)
                           ),
                           child: Icon(FontAwesomeIcons.googlePlusG,color: Theme.of(context).primaryColor,),
                           onPressed: () {},
                         )
                       ],
                     ),
                     SizedBox(height: SizeConfig.blockSizeVertical * 3,)
                     ,Row(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                         Text("Don't have an account ?",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5,fontWeight: FontWeight.bold,color: Colors.black38),),
                         SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
                         GestureDetector(
                           onTap: () {
                             setState(() {
                               widget.tabController.index = 1;
                             });
                           },
                           child: Text("Sign Up",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5,color: Colors.orangeAccent),),
                         )
                       ],
                     ),
                     SizedBox(height: SizeConfig.blockSizeVertical * 5 ,),
                   ],
                 ),
                 state.isLoading ? buildLoader(context) :Text("")
               ],
            );
          },
        ),
      width: SizeConfig.blockSizeVertical * 100,
    );
  }


}