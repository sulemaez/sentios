
import 'package:flutter/material.dart';
import 'package:senti/pages/group_profile/your_savings.dart';
import 'package:senti/pages/home/home_page.dart';
import 'package:senti/pages/loans/loan_home_page.dart';
import 'package:senti/pages/meetings/meeting_page.dart';
import 'package:senti/pages/profile/profile_page.dart';
import 'package:senti/utils/globals.dart';


class HomeBase extends StatefulWidget{

  final bool newGroup;

  HomeBase({this.newGroup});

  @override
  HomeBaseState createState() => HomeBaseState();
}

class HomeBaseState extends State<HomeBase>{

  int _currentIndex = 0;

  HomePage homePage;
  LoanHomePage loanPage;
  YourSavingsPage yourSavingsPage;
  MeetingPage meetingPage;
  //ChatPage  chatPage;
  ProfilePage profilePage;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    homeBaseState = this;
    setPages();
  }

  void setPages(){
    homePage = HomePage(newGroup: widget.newGroup);
    loanPage = LoanHomePage();
    meetingPage = MeetingPage();
    // chatPage = ChatPage();
    profilePage = ProfilePage();
    yourSavingsPage = YourSavingsPage();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body:SafeArea(
          top: false,
          child: IndexedStack(
            index: _currentIndex,
            children:<Widget>[
              homePage,
              loanPage,
             yourSavingsPage,
               meetingPage,
//              chatPage,
              profilePage
            ]
          ),
        ),

        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Color(0xf5f8ffff),
          showUnselectedLabels: true,
          iconSize:30,
          currentIndex: _currentIndex,
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
            });
            _handlePageActivities(index);
          },
          unselectedItemColor: Theme.of(context).secondaryHeaderColor,
          selectedItemColor: Theme.of(context).primaryColor,
          elevation: 0,
          items: allDestinations.map((Destination destination) {
            return BottomNavigationBarItem(
                backgroundColor: Color(0xf5f8ffff),
                icon: destination.icon != null ? Icon(destination.icon) :
                    Image.asset(
                      destination.image,
                      width: 35,
                      color: _currentIndex == allDestinations.indexOf(destination) ? Theme.of(context).primaryColor : Theme.of(context).secondaryHeaderColor,
                    ),
                title: Text(destination.title),
            );
          }).toList(),

        )
    );
  }

  void _handlePageActivities(int index){
     if(index == 0){
         //handle home page
         homePage.refreshPage();
     }
  }
}


List<Destination> allDestinations = <Destination>[
  Destination("Home",icon:Icons.home),
  Destination("Loan",image: "assets/loan1.png"),
  Destination("Savings",icon: Icons.monetization_on),
  Destination("Meeting",image: "assets/meet.png"),
  //Destination("Chat",image: "assets/chat.png"),
  Destination("Profile",icon:Icons.person)
];

class Destination {
  Destination(this.title,{this.icon,this.image});
  final String title;
  final IconData icon;
  String image;
}


