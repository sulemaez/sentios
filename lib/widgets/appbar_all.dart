
import 'package:flutter/material.dart';


class MyBar extends AppBar {

  MyBar(this.head,this.context);

  final context;
  final String head;

  @override
  // TODO: implement centerTitle
  bool get centerTitle => true;
  @override
  // TODO: implement elevation
  double get elevation => 0.0;

  @override
  // TODO: implement title
  Widget get title =>  Text(head,textAlign: TextAlign.center, style: TextStyle(color: Theme.of(context).primaryColor,fontWeight: FontWeight.bold),);

  @override
  // TODO: implement backgroundColor
  Color get backgroundColor => Colors.white;

  @override
  // TODO: implement leading
  Widget get leading => IconButton(
    icon: Icon(Icons.arrow_back_ios, color: Theme.of(context).primaryColor),
    onPressed: () => Navigator.of(context).pop(),
  );


}
