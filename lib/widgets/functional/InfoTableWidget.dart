
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';

class InfoTableWidget extends StatelessWidget {

  final List<String> headings;
  final List<Map<String,dynamic>> data;
  final Map<int,FractionColumnWidth> columnWidths;
  Function action;

  InfoTableWidget({this.headings,this.data,this.columnWidths,this.action});

  @override
  Widget build(BuildContext context) {
    List<TableRow> tableRows = [];

    //get heading
    tableRows.add(_getHeading(context));

    tableRows.addAll(_getData(context));

    return Table(
      children: tableRows,
      columnWidths: columnWidths ?? {},
      border: TableBorder.all(
        color: Colors.grey,
        width: 0.5
      ),
    );
  }

  //create table headings
  TableRow _getHeading(BuildContext context){
    List<TableCell> headingCells = [];
    headings.forEach((title){
       headingCells.add(TableCell(
         child: Padding(
           padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 2),
           child: Text(title,style: TextStyle(color: Colors.white),),
         ),
       ));
    });
    return TableRow(
      children: headingCells,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor
      )
    );
  }

  //creates the rows of data
  List<TableRow> _getData(BuildContext context){
    List<TableRow> dataRows = [];

    data.forEach((item){
       dataRows.add(TableRow(
         children: _getRowChildren(context,item),
         decoration: BoxDecoration(
           color: data.indexOf(item) % 2 != 0 ? Color.fromARGB(255, 244, 244, 244):Colors.white
         )
       ));
    });

    return dataRows;
  }

  //creates on row of data
  List<TableCell>_getRowChildren(BuildContext context,item){
    List<TableCell> tableCells = [];

    headings.forEach((head){
      var value;
      if(headings.indexOf(head) ==0)
        value = data.indexOf(item) + 1;
        else
        value = item[head];

      tableCells.add(TableCell(
        child: GestureDetector(
          child: Container(
            padding: action != null  ? EdgeInsets.all(SizeConfig.blockSizeHorizontal * 4) :
            EdgeInsets.all(SizeConfig.blockSizeHorizontal * 2),
            child: AutoSizeText(value.toString()),
            color: Colors.white,
          ),
          onTap: action != null ? action : (){},
        ),
      ));
    });

    return tableCells;
  }
}
