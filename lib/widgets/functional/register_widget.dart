
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senti/blocs/signup_bloc/images.dart';
import 'package:senti/blocs/signup_bloc/signup_page_bloc.dart';
import 'package:senti/blocs/signup_bloc/signup_page_events.dart';
import 'package:senti/blocs/signup_bloc/signup_page_state.dart';
import 'package:senti/utils/globals/images.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/utils/validators.dart';

class RegisterWidget extends StatefulWidget {

  final SignUpPageBloc bloc;
  final SignUpPageState state;

  const RegisterWidget({this.bloc,this.state});

  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
       child: Stack(
         alignment: Alignment.topCenter,
         overflow: Overflow.visible,
         children: <Widget>[
           Card(
             color: Color.fromRGBO(244, 244, 244, 1),
             elevation: 0.5,
             shape: RoundedRectangleBorder(
               borderRadius: BorderRadius.circular(8.0),
             ),
             child: Container(
                 width: SizeConfig.blockSizeHorizontal * 80,
                 child: Padding(
                   padding: EdgeInsets.only(
                       top: SizeConfig.blockSizeVertical * 3,
                       left: SizeConfig.blockSizeHorizontal * 5,
                       bottom: SizeConfig.blockSizeVertical * 8,
                       right: SizeConfig.blockSizeHorizontal * 5),
                   child: Form(
                     key:widget.state.formKey,
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         Row(
                           children: <Widget>[
                             Icon(
                               Icons.person,
                               color: Colors.orangeAccent,
                               size: SizeConfig.blockSizeHorizontal * 4,
                             ),
                             Padding(
                               padding: EdgeInsets.only(left: 5),
                             ),
                             Text(
                               "Name",
                               style: TextStyle(
                                   color: Theme.of(context).primaryColor,
                                   fontSize: 16),
                             ),
                           ],
                         ),
                         Container(
                           width: SizeConfig.blockSizeHorizontal * 70,
                           child: TextFormField(
                             decoration: InputDecoration(
                                 enabledBorder: UnderlineInputBorder(
                                     borderSide:
                                     BorderSide(color: Colors.grey)),
                                 focusedBorder: UnderlineInputBorder(
                                     borderSide:
                                     BorderSide(color: Colors.grey)),
                                 hintText: "John Doe",
                                 fillColor: Colors.white,
                                 errorText:widget.state.nameError == "" ? null: widget.state.nameError
                             ),
                             validator: validateName,
                             onSaved: (String name) => widget.bloc.add(SaveEvent(value : name,type:"name")),
                           ),
                         ),
                         Padding(
                           padding: EdgeInsets.only(top: 20),
                         ),
                         Row(
                           children: <Widget>[
                             Icon(
                               Icons.phone,
                               color: Colors.orangeAccent,
                               size: SizeConfig.blockSizeHorizontal * 4,
                             ),
                             Padding(
                               padding: EdgeInsets.only(left: 5),
                             ),
                             Text(
                               "Contact",
                               style: TextStyle(
                                   color: Theme.of(context).primaryColor,
                                   fontSize: 16),
                             ),
                           ],
                         ),
                         Container(
                           child: Row(
                             children: <Widget>[
                               Text("+254",style: TextStyle(color: Colors.black54,fontSize: SizeConfig.blockSizeHorizontal * 4),),
                               Container(
                                   width: SizeConfig.blockSizeHorizontal * 50,
                                   child:TextFormField(
                                     keyboardType: TextInputType.number,
                                     decoration: InputDecoration(
                                         enabledBorder: InputBorder.none,
                                         focusedBorder: InputBorder.none,
                                         hintText: "796789902",
                                         fillColor: Colors.white,
                                         errorText: widget.state.contactError == "" ? null : widget.state.contactError
                                     ),
                                     validator: validateContact,
                                     onSaved:  (String contact) => widget.bloc.add(SaveEvent(value : contact,type: "contact")),
                                   )),
                             ],
                           ),
                           width: SizeConfig.blockSizeHorizontal * 70,
                           decoration: BoxDecoration(
                               border: Border(
                                   bottom: BorderSide(
                                       color:Colors.grey
                                   )
                               )
                           ),
                         ),
                         Padding(
                           padding: EdgeInsets.only(top: 20),
                         ),
                         Row(
                           children: <Widget>[
                             Icon(
                               Icons.email,
                               color: Colors.orangeAccent,
                               size: SizeConfig.blockSizeHorizontal * 4,
                             ),
                             Padding(
                               padding: EdgeInsets.only(left: 5),
                             ),
                             Text(
                               "Email",
                               style: TextStyle(
                                   color: Theme.of(context).primaryColor,
                                   fontSize: 16),
                             ),
                           ],
                         ),
                         Container(
                           width: SizeConfig.blockSizeHorizontal * 70,
                           child: TextFormField(
                             decoration: InputDecoration(
                                 enabledBorder: UnderlineInputBorder(
                                     borderSide:
                                     BorderSide(color: Colors.grey)),
                                 focusedBorder: UnderlineInputBorder(
                                     borderSide:
                                     BorderSide(color: Colors.grey)),
                                 hintText: "someone@gmail.com",
                                 fillColor: Colors.white,
                                 errorText: widget.state.emailError == "" ? null : widget.state.emailError
                             ),
                             validator: validateEmail,
                             onSaved:  (String email) => widget.bloc.add(SaveEvent(value : email,type:"email")),
                           ),
                         ),
                         Padding(
                           padding: EdgeInsets.only(top: 20),
                         ),
                         Row(
                           children: <Widget>[
                             Icon(
                               FontAwesomeIcons.female,
                               color: Colors.orangeAccent,
                               size: SizeConfig.blockSizeHorizontal * 4,
                             ),
                             Padding(
                               padding: EdgeInsets.only(left: 5),
                             ),
                             Text(
                               "Gender",
                               style: TextStyle(
                                   color: Theme.of(context).primaryColor,
                                   fontSize: 16),
                             ),
                           ],
                         ),
                         Row(
                           children: <Widget>[
                             new Radio(
                               value: 0,
                               groupValue: widget.state.gender,
                               onChanged: (gender) => widget.bloc.add(GenderChangedEvent(0)),
                               activeColor: Colors.orangeAccent,
                             ),
                             new Text(
                               'Male',
                               style: new TextStyle(fontSize: 16.0),
                             ),
                             new Radio(
                               value: 1,
                               groupValue: widget.state.gender,
                               onChanged: (gender) => widget.bloc.add(GenderChangedEvent(1)),
                               activeColor: Colors.orangeAccent,
                             ),
                             new Text(
                               'Female',
                               style: new TextStyle(fontSize: 16.0),
                             ),
                           ],
                         ),
                         Padding(
                           padding: EdgeInsets.only(top: 10),
                         ),
                         //DOB
                         Row(
                           children: <Widget>[
                             Icon(
                               FontAwesomeIcons.calendarAlt,
                               color: Colors.orangeAccent,
                               size: SizeConfig.blockSizeHorizontal * 4,
                             ),
                             Padding(
                               padding: EdgeInsets.only(left: 5),
                             ),
                             Text(
                               "D.O.B.",
                               style: TextStyle(
                                   color: Theme.of(context).primaryColor,
                                   fontSize: 16
                               ),
                             ),
                           ],
                         ),
                         //build the DOB input with function
                         _buildDOB(widget.state),
                         //photo of ID
                         Padding(
                           padding: EdgeInsets.only(top: 20),
                         ),
                         Row(
                           children: <Widget>[
                             Icon(
                               Icons.person,
                               color: Colors.orangeAccent,
                               size: SizeConfig.blockSizeHorizontal * 4,
                             ),
                             Padding(
                               padding: EdgeInsets.only(left: 5),
                             ),
                             Text(
                               "Capture photo of ID",
                               style: TextStyle(
                                   color: Theme.of(context).primaryColor,
                                   fontSize: 16
                               ),
                             ),
                           ],
                         ),
                         Padding(
                           padding: EdgeInsets.only(top: 10),
                         ),
                         Row(
                           children: <Widget>[
                             Column(
                               children: <Widget>[
                                 buildImages(SignUpImage.FRONT,widget.state),
                                 Text("Front")
                               ],
                             ),
                             SizedBox(width: SizeConfig.blockSizeVertical * 3,),
                             Column(
                               children: <Widget>[
                                 buildImages(SignUpImage.BACK,widget.state),
                                 Text("Back")
                               ],
                             )
                           ],
                         ),
                         //photo of member
                         Padding(
                           padding: EdgeInsets.only(top: 20),
                         ),
                         Row(
                           children: <Widget>[
                             Icon(
                               Icons.person,
                               color: Colors.orangeAccent,
                               size: SizeConfig.blockSizeHorizontal * 4,
                             ),
                             Padding(
                               padding: EdgeInsets.only(left: 5),
                             ),
                             Text(
                               "Capture picture of member",
                               style: TextStyle(
                                   color: Theme.of(context).primaryColor,
                                   fontSize: 16
                               ),
                             ),
                           ],
                         ),
                         Padding(
                           padding: EdgeInsets.only(top: 10),
                         ),
                         //get photo of member
                         buildImages(SignUpImage.PIC,widget.state),
                         //Passwords
                         widget.bloc.memberRegistration ? Text("") :   Padding(
                           padding: EdgeInsets.only(top: 20),
                         ),
                         widget.bloc.memberRegistration? Text("") :Row(
                           children: <Widget>[
                             Icon(Icons.lock,color: Colors.orangeAccent,size: SizeConfig.blockSizeHorizontal * 4,),
                             Padding( padding: EdgeInsets.only(left: 5),),
                             Text("Password",style: TextStyle(color: Theme.of(context).primaryColor , fontSize: 16),),
                             Flexible(fit: FlexFit.tight, child: SizedBox()),
                             GestureDetector(
                               onTap: () => widget.bloc.add(ObscureTextEvent(type: "password")),
                               child: Icon(widget.state.obscurePassword ? Icons.visibility : Icons.visibility_off),
                             )
                           ],
                         ),
                         widget.bloc.memberRegistration  ? Text("") :Container(
                           width: SizeConfig.blockSizeHorizontal * 70,
                           child: TextFormField(
                             decoration:  InputDecoration(
                                 enabledBorder: UnderlineInputBorder(
                                     borderSide: BorderSide(color: Colors.grey)
                                 ),
                                 focusedBorder: UnderlineInputBorder(
                                     borderSide: BorderSide(color: Colors.grey)
                                 ),
                                 fillColor: Colors.white,
                                 errorText: widget.state.passwordError == "" ? null : widget.state.passwordError
                             ),
                             obscureText:widget.state.obscurePassword,
                             validator: validatePassword,
                             onChanged: (pass) => widget.bloc.add(PasswordChangedEvent(pass)),
                           ),
                         ),
                         widget.bloc.memberRegistration ? Text("") :  Padding(
                           padding: EdgeInsets.only(top: 20),
                         ),
                         widget.bloc.memberRegistration ? Text("") : Row(
                           children: <Widget>[
                             Icon(Icons.lock,color: Colors.orangeAccent,size: SizeConfig.blockSizeHorizontal * 4,)
                             ,Padding( padding: EdgeInsets.only(left: 5),)
                             ,Text("Confirm Password",style: TextStyle(color: Theme.of(context).primaryColor , fontSize: 16),),
                             Flexible(fit: FlexFit.tight, child: SizedBox()),
                             GestureDetector(
                               onTap: () => widget.bloc.add(ObscureTextEvent(type: "confirmed")),
                               child: Icon(widget.state.obscureConfirmPassword ? Icons.visibility : Icons.visibility_off),
                             )
                           ],
                         ),
                         //TODO containers with text set as widget to avoid repition
                         widget.bloc.memberRegistration ? Text("") : Container(
                           width: SizeConfig.blockSizeHorizontal * 70,
                           child: TextFormField(
                               decoration:  InputDecoration(
                                 enabledBorder: UnderlineInputBorder(
                                     borderSide: BorderSide(color: Colors.grey)
                                 ),
                                 focusedBorder: UnderlineInputBorder(
                                     borderSide: BorderSide(color: Colors.grey)
                                 ),
                                 fillColor: Colors.white,
                               ),
                               obscureText:widget.state.obscureConfirmPassword,
                               validator: (pass) => validatePasswordRepeat(pass, widget.state.password),
                               onSaved:  (String pass) => widget.bloc.add(SaveEvent(value : pass,type:"confirm"))
                           ),
                         ),
                         widget.bloc.memberRegistration ? Text("") :  SizedBox(
                           height: SizeConfig.blockSizeVertical * 5,
                         ),
                       ],
                     ),
                   ),
                 )
             ),
           ),
           Positioned(
             bottom: -25,
             child: Align(
               alignment: Alignment.bottomCenter,
               child: MaterialButton(
                 minWidth: SizeConfig.blockSizeHorizontal * 50,
                 height: SizeConfig.blockSizeVertical * 6,
                 shape: RoundedRectangleBorder(
                     borderRadius: BorderRadius.circular(22)),
                 color: Theme.of(context).primaryColor,
                 child: Text(
                   widget.bloc.memberRegistration ? "Register" : "SIGN UP",
                   style: TextStyle(
                       color: Colors.white, fontWeight: FontWeight.bold),
                 ),
                 onPressed: () => widget.bloc.add(SubmitFormEvent()),
               ),
             ),
           ),
         ],
       ),
      width: SizeConfig.blockSizeHorizontal * 100,
    );
  }

  Widget _buildDOB(SignUpPageState state) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        //day dropdown
        DropdownButton<String>(
          value: state.dayValue,
          icon: Icon(Icons.keyboard_arrow_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.deepPurple),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (String newValue) => widget.bloc.add(DayValueChangedEvent(newValue)),
          items: <String>[
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '21',
            '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'
          ].map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
        //month dropdown
        DropdownButton<String>(
          value: months[state.monthValue-1],
          icon: Icon(Icons.keyboard_arrow_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.deepPurple),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (String newValue) => widget.bloc.add(MonthValueChangedEvent(months.indexOf(newValue)+1)),
          items: months.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
        //year dropdown
        DropdownButton<String>(
          value: state.yearValue,
          icon: Icon(Icons.keyboard_arrow_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.deepPurple),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (String newValue) => widget.bloc.add(YearValueChangedEvent(newValue)),
          items: _getYears().map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ],
    );
  }


  //images holder
  Widget buildImages(SignUpImage image,SignUpPageState state){

    // get the specific selected image
    File imageFile;
    if(image == SignUpImage.FRONT){
      imageFile = state.frontImage;
    }else if(image == SignUpImage.BACK){
      imageFile = state.backImage;
    }else{
      imageFile = state.picImage;
    }

    return GestureDetector(
      child: DottedBorder(
        color: Colors.grey,
        strokeWidth: 1,
        child: Container(
          color: Colors.white,
          height: MediaQuery.of(context).orientation == Orientation.portrait ? SizeConfig.blockSizeVertical * 12 :SizeConfig.blockSizeVertical * 20,
          width: SizeConfig.blockSizeHorizontal * 22,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                imageFile == null ? Icon(Icons.camera_alt): Icon(Icons.check,color: Colors.green,),
                imageFile == null ? AutoSizeText("Add photo",textAlign: TextAlign.center,) : Text("")
              ],
            ),
          ),
        ),
      ),
      onTap: ()async{
        String src = await chooseImageSource(context);
        widget.bloc.add(GetImageEvent(image,src));
      },
    );
  }

  //get years to populate dropdown
  List<String> _getYears(){
    int first = 1920;
    int last = DateTime.now().year;

    var years = List<String>();

    for(var y = first; y <= last ; y++){
      years.add(y.toString());
    }
    return years;
  }

  //list of months
  List<String> months = [
    'Jan','Feb','March','April','May','June','July','Aug','Sept','Oct','Nov','Dec'
  ];

}
