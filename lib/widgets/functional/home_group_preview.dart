
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senti/utils/size_config.dart';

class GroupPreview extends StatelessWidget {
  final int minimum;
  final String groupName;
  final String location;
  final int target;
  final Function actionJoin;
  final Function actionView;

  GroupPreview({this.groupName,this.location,this.target,this.minimum,this.actionJoin,this.actionView});

  @override
  Widget build(BuildContext context) {
    return Padding(
        child: Card(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(groupName,textAlign: TextAlign.left,style: TextStyle(fontSize:SizeConfig.blockSizeHorizontal * 4.5 ,color: Colors.grey),),
                    Divider(color: Colors.grey,thickness: 0.4,),
                    Row(
                      children: <Widget>[
                          GestureDetector(
                            child: Container(
                              color: Colors.white,
                              width: SizeConfig.blockSizeHorizontal * 60,
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Icon(Icons.location_on,size: SizeConfig.blockSizeHorizontal * 4,color: Colors.grey),
                                      SizedBox(width:SizeConfig.blockSizeVertical * 1 ,),
                                      Text(location,style: TextStyle(color: Colors.grey,))
                                    ],
                                  ),
                                  SizedBox(height:SizeConfig.blockSizeVertical * 1 ,),
                                  Row(
                                    children: <Widget>[
                                      Icon(Icons.people,size: SizeConfig.blockSizeHorizontal * 4,color: Colors.grey,),
                                      SizedBox(width:SizeConfig.blockSizeVertical * 1 ,),
                                      Text("22 Members",style: TextStyle(color: Colors.grey),)
                                    ],
                                  ),
                                  SizedBox(height:SizeConfig.blockSizeVertical * 1 ,),
                                  Row(
                                      children: <Widget>[
                                        Text("Minimum contribution amount ",style: TextStyle(color: Colors.grey),),
                                        Text(minimum.toString(),style: TextStyle(color:Colors.black54,fontWeight: FontWeight.bold))
                                      ]
                                  )
                                ],
                              ),
                            ),
                            onTap: (){
                              actionView();
                            },
                          ),
                          Column(
                             children: <Widget>[
                                 //todo get bullseye from ai file
                                 Image.asset("assets/target_eye.png",color: Theme.of(context).primaryColor,width: SizeConfig.safeBlockHorizontal *10,),
                                 Text(target.toString(),style: TextStyle(color:Colors.black54,fontWeight: FontWeight.bold,fontSize: SizeConfig.blockSizeHorizontal * 6),)
                             ],
                          )
                      ],
                    )
                  ],
                ),
              ),
             GestureDetector(
               child:  Container(
                 color: Color.fromARGB(255, 240, 153, 0),
                 height: SizeConfig.blockSizeVertical * 4.5,
                 width: SizeConfig.blockSizeHorizontal * 100,
                 child: Center(child: Text("Join Group",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4.5,color: Colors.white),textAlign: TextAlign.center,),),
               ),
               onTap:actionJoin,
             )
            ],
          ),
          elevation: 3.0,
        ),
      padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2,bottom: SizeConfig.blockSizeVertical * 2),
    );
  }
}
