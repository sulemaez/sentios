
import 'package:flutter/material.dart';
import 'package:senti/blocs/meeting_page_bloc.dart';
import 'package:senti/utils/globals/date.dart';
import 'package:senti/utils/size_config.dart';
import 'package:simple_gesture_detector/simple_gesture_detector.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/intl.dart';

class MeetingCalender extends StatefulWidget {

   final MeetingPageState meetingPageState;
   final Function onMonthChanged;
   final Function onCalenderToggle;

   const MeetingCalender({Key key, this.meetingPageState, this.onMonthChanged,this.onCalenderToggle}) : super(key: key);

   @override
   State<MeetingCalender> createState() => _MeetingCalenderState();

}

class _MeetingCalenderState extends State<MeetingCalender> {
  CalendarController _controller;
  String month = "";
  String year= "";

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _controller = CalendarController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.blockSizeHorizontal * 100,
      color: Color.fromARGB(255,245, 246, 250),
      child: Column(
        children: <Widget>[
          _setPadding(Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(widget.meetingPageState.month,style: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 8,fontWeight: FontWeight.bold),),
                  SizedBox(width: SizeConfig.blockSizeHorizontal * 1,),
                  IconButton(
                    icon:  Icon(widget.meetingPageState.showCalender ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,color: Color.fromARGB(255, 150, 0, 255)),
                    iconSize: SizeConfig.blockSizeHorizontal * 10,
                    onPressed: (){
                       widget.onCalenderToggle(widget.meetingPageState.showCalender ? false : true);
                    },
                  )
                ],
              ),
              Text(widget.meetingPageState.year,style: TextStyle(color: Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 4.5),),
            ],
          )),
         widget.meetingPageState.showCalender ? Container(
           width: SizeConfig.blockSizeHorizontal * 100,
           child: TableCalendar(
             calendarController: _controller,
             initialCalendarFormat: CalendarFormat.month,
             formatAnimation: FormatAnimation.slide,
             startingDayOfWeek: StartingDayOfWeek.sunday,
             availableGestures: AvailableGestures.none,
             onVisibleDaysChanged: (DateTime start,DateTime end,CalendarFormat format){
               _monthChanged(start);
             },
             simpleSwipeConfig: SimpleSwipeConfig(
               swipeDetectionBehavior: SwipeDetectionBehavior.continuous,
               horizontalThreshold: 6,
             ),
             availableCalendarFormats: const {
               CalendarFormat.month: 'Month',
             },
             calendarStyle: CalendarStyle(
               weekdayStyle: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),
               weekendStyle: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),
               outsideStyle: TextStyle(color: Colors.grey.withAlpha(60)),
               unavailableStyle: TextStyle(color: Colors.grey),
               outsideWeekendStyle: TextStyle(color: Colors.grey.withAlpha(60)),
               todayColor: Colors.grey,
               selectedColor: Color.fromARGB(255,245, 246, 250),
               selectedStyle: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),
             ),
             daysOfWeekStyle: DaysOfWeekStyle(
               dowTextBuilder: (date, locale) {
                 return DateFormat.E(locale)
                     .format(date)
                     .substring(0, 3)
                     .toUpperCase();
               },
               weekdayStyle: TextStyle(color: Theme.of(context).primaryColor,fontWeight: FontWeight.bold),
               weekendStyle: TextStyle(color: Theme.of(context).primaryColor,fontWeight: FontWeight.bold),
             ),
             holidays: _meetingDays(),

             builders: CalendarBuilders(
               holidayDayBuilder: (context, date, _) {
                 return Container(
                   decoration: new BoxDecoration(
                     color: DateTime.now().compareTo(date) > 0 ? Color.fromARGB(255, 158, 183, 255) : Color.fromARGB(255, 246, 144, 33),
                     shape: BoxShape.circle,
                   ),
                   width: SizeConfig.blockSizeHorizontal * 10,
                   child: Center(
                     child: Text(
                       '${date.day}',
                       style: TextStyle(
                         color: Colors.white,
                       ),
                     ),
                   ),
                 );
               },
             ),

           ),
         ) : Text(""),
        ],
      )
    );
  }


  void _monthChanged(DateTime start){
    setState(() {
      month = getMonthFromInt(_controller.visibleDays[25].month);
      year = start.year.toString();
    });

    widget.onMonthChanged(_controller.visibleDays[25].month,_controller.visibleDays[25].year);
  }

  Widget _setPadding(Widget widget){
      return Padding(
        padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal * 4),
        child: widget,
    );
  }

   Map<DateTime, List> _meetingDays(){
    Map<DateTime,List> list = {};

    widget.meetingPageState.meetings.forEach((meeting){
       list[DateTime.parse(meeting["date"])] = ['Meeting'] ;
    });

    return list;
  }

}

