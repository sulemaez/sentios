
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/text/text_form_widgets.dart';

class LoanTermsWidget extends StatefulWidget {

  final List<Map<String,dynamic>> loanSpans;
  final Map<String,Function> actions;

  LoanTermsWidget(this.loanSpans,this.actions);

  @override
  _LoanTermsWidgetState createState() => _LoanTermsWidgetState();

}

class _LoanTermsWidgetState extends State<LoanTermsWidget> {

  var _localValue = "Select Loan Spans";

  //holds the loan span widgets
  List<_LoanSpanItem> _loanSpans;

  _LoanTermsWidgetState();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   _loanSpans = [];
    widget.loanSpans.forEach((element){
      _loanSpans.add(_LoanSpanItem("${element['months'].toString()} Months ","${element["interest"]}",
        parentCallbackRemove: _removeLoanSpanItem,
        parentCallbackAdd: widget.actions['add'],
      ),
      );
    });

  }

  @override
  Widget build(BuildContext context) {

    return Column(
       crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
         Container(
           width: SizeConfig.blockSizeHorizontal * 100,
           padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
           decoration: BoxDecoration(
               color: Color.fromARGB(255, 244, 244, 244),
               borderRadius: BorderRadius.circular(30.0),
            ),
           child:Center(
             child:  DropdownButton<String>(
               isDense: true,
               value: _localValue,
               isExpanded: true,
               icon: Icon(Icons.keyboard_arrow_down),
               iconSize: 24,
               elevation: 16,
               underline: Container(
                 height: 30,
                 width: SizeConfig.blockSizeHorizontal * 100,
               ),
               onChanged: _createLoanSpanItem,
               items: _spanList()
                   .map<DropdownMenuItem<String>>((String value) {
                 return DropdownMenuItem<String>(
                   value: value,
                   child: Text("  $value"),
                 );
               })
                   .toList(),
             ),
           )
         ),
        SizedBox(height: SizeConfig.blockSizeVertical * 2,),
        Column(
           children: _loanSpans,
        )
      ],
    );
  }

  //creates and appends new loan span item
  void _createLoanSpanItem(String newValue){
    _LoanSpanItem item = _getLoanItem(newValue);

    if(item == null)
    setState(() {
       _loanSpans.add(_LoanSpanItem(newValue,"", parentCallbackRemove: _removeLoanSpanItem, parentCallbackAdd: widget.actions['add'], ));
    });

  }

  //when new loan span added
  void _removeLoanSpanItem(String duration){
      var item = _getLoanItem(duration);

     if(item != null){
       setState(() {
         _loanSpans.remove(item);
       });
     }

     widget.actions['remove'](duration.split(" ")[0].trim());
  }

   //cheks if the loan spna has not been set and then creates it;
  _LoanSpanItem _getLoanItem(String duration){
    _LoanSpanItem item;
    _loanSpans.forEach((t){
      if(t.duration == duration){
        item = t;
      }
    });
    return item;
  }


  //lis of the spans to be displayed
  List<String> _spanList(){
    List<String> list = ["Select Loan Spans"];
    var lastInt = 30;
    for(var i = 1;i < lastInt;i++){
        if(i == 1){
          list.add("$i Month");
          continue;
        }
        list.add("$i Months");
    }
    return list;
  }
}


class _LoanSpanItem extends StatefulWidget{

  final String duration;
  final String value;
  final void Function(String value) parentCallbackRemove;
  final void Function(String duration,String value) parentCallbackAdd;

  _LoanSpanItem(this.duration,this.value,{this.parentCallbackRemove,this.parentCallbackAdd});

  @override
  State<StatefulWidget> createState() => _LoanSpanItemState();
}

class _LoanSpanItemState extends State<_LoanSpanItem> {

  _LoanSpanItemState();

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      child: Row(
        mainAxisAlignment:MainAxisAlignment.spaceBetween ,
        children: <Widget>[
          Container(
            width: SizeConfig.blockSizeHorizontal * 35,
            padding: EdgeInsets.only(top:SizeConfig.blockSizeVertical * 1,bottom: SizeConfig.blockSizeVertical * 1,left: SizeConfig.blockSizeHorizontal * 2,right:  SizeConfig.blockSizeHorizontal * 1),
            decoration: BoxDecoration(
                color: Color.fromRGBO(244, 244, 244, 1),
                borderRadius: BorderRadius.all(Radius.circular(50))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(widget.duration,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4),),
                SizedBox(width: SizeConfig.blockSizeHorizontal * 3,),
                GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(50))
                      ),
                      child: Icon(Icons.close,color: Colors.black38,),
                    ),
                    onTap: ()=>
                      //call the function to remove this child
                      widget.parentCallbackRemove(widget.duration)

                )
              ],
            ),
          ),
          Container(
            height: SizeConfig.blockSizeVertical * 5, 
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: SizeConfig.blockSizeHorizontal * 15
                  ,child:  TextFormField(
                    initialValue: widget.value != null ? widget.value : "",
                    keyboardType: TextInputType.number,
                    onChanged: (String str) =>  widget.parentCallbackAdd(widget.duration.split(" ")[0],str),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color.fromRGBO(244, 244, 244,1),
                      border:OutlineInputBorder(
                        borderSide: BorderSide(
                            width: 0.0,
                            style: BorderStyle.none
                        ),
                      ),
                    ),
                  ),
                ),
                Text("%",textAlign: TextAlign.left,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 6),)
              ],
            ),
            decoration: BoxDecoration(
                color: Color.fromRGBO(244, 244, 244,1),
                borderRadius: BorderRadius.all(Radius.circular(6))
            ),
          )
        ],
      ),
      padding: EdgeInsets.only(top:SizeConfig.blockSizeVertical * 2),
    );
  }
}
