
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/utils/size_config.dart';

class CircleThenRectangularLabel extends StatelessWidget {
  final Color rectangleColor;
  final Color circleColor;
  final String rectangleValue;
  final String circleValue;

  CircleThenRectangularLabel({this.circleColor,this.circleValue,this.rectangleColor,this.rectangleValue});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
           width: SizeConfig.blockSizeHorizontal * 30,
           margin: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 1,left: SizeConfig.blockSizeHorizontal * 7.5),
           padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal * 8,right: SizeConfig.blockSizeHorizontal * 1),
           height: SizeConfig.blockSizeVertical * 5,
           child: Center(
              child: Text(rectangleValue,style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 3.5),),
           ),
           decoration: BoxDecoration(
              color: rectangleColor,
              borderRadius: BorderRadius.all(Radius.circular(5.0))
           ),
         ),
         Container(
           padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 5),
           child: Center(
             child: Text(circleValue,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
           ),
           decoration: BoxDecoration(
              color: circleColor,
              shape: BoxShape.circle,
           ),
         )
      ],
    );
  }
}
