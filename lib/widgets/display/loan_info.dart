
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/display/info_widget.dart';

class LoanInfoWidget extends StatelessWidget {
  final bool paid;
  final bool approvedPending;

  LoanInfoWidget({this.paid,this.approvedPending});

  @override
  Widget build(BuildContext context) {
    return InfoWidget(
      headerColor:  approvedPending != null ? Colors.orange : Color.fromARGB(255, 150, 0, 255),
      headerText: "July",
      bodyContent: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Loan Amount",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("\$7500",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),)
                  ],
                ),
              ),
              SizedBox(width: SizeConfig.blockSizeHorizontal * 0.5,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Loan Term",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("3 Months",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),)
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: SizeConfig.blockSizeHorizontal * 3,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Started From",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("\$10th Jul, 2019",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),)
                  ],
                ),
              ),
              SizedBox(width: SizeConfig.blockSizeHorizontal * 0.5,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Rate of interest",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("5.5%",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),)
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: SizeConfig.blockSizeHorizontal * 3,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("NO. of Installements",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("3",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),)
                  ],
                ),
              ),
              SizedBox(width: SizeConfig.blockSizeHorizontal * 0.5,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Installement Amount",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("\$2500",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),)
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: SizeConfig.blockSizeHorizontal * 3,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Amount Paid",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("\$7500",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),)
                  ],
                ),
              ),
              SizedBox(width: SizeConfig.blockSizeHorizontal * 0.5,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Amount Pending",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("\$2500",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),)
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: SizeConfig.blockSizeHorizontal * 3,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Approved By",style: TextStyle(color:Colors.grey),),
              SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
              Text("Elizabeth, Patrick, John",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 5),)
            ],
          )
        ],
      ),
      footerContent: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Status",style: TextStyle(color:Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 5),),
          paid != null ? Text("Paid",style: TextStyle(color: Colors.green,fontSize: SizeConfig.blockSizeHorizontal * 4,fontWeight: FontWeight.bold)):
          approvedPending != null ?
              Text("Approval Pending",style: TextStyle(color: Colors.red,fontSize: SizeConfig.blockSizeHorizontal * 4,fontWeight: FontWeight.bold),)
              :Text("1 Installment Remaining",style: TextStyle(color: Colors.red,fontSize: SizeConfig.blockSizeHorizontal * 4,fontWeight: FontWeight.bold),)

        ],
      ),
    );
  }
}
