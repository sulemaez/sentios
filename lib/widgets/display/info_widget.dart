
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/utils/size_config.dart';

class InfoWidget extends StatelessWidget {
  final Widget bodyContent;
  final Color headerColor;
  final Widget footerContent;
  final String headerText;

  const InfoWidget({Key key,this.headerText,this.bodyContent, this.headerColor, this.footerContent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.blockSizeHorizontal * 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: SizeConfig.blockSizeHorizontal * 100,
            padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
            child: Text(headerText,style: TextStyle(color: Colors.white,fontSize: SizeConfig.blockSizeHorizontal * 4),),
            decoration: BoxDecoration(
              color: headerColor,
              borderRadius: BorderRadius.only(topRight: Radius.circular(5),topLeft: Radius.circular(5)),
            ),
          ),
          Container(
            padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
            child: bodyContent,
          ),
          footerContent == null ? Text("") :
          Container(
            width: SizeConfig.blockSizeHorizontal * 100,
            padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 3),
            child: footerContent,
            color: Color.fromARGB(255, 244, 244, 244),
          )
        ],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(6)),
        border: Border.all(
          color: Colors.grey
        )
      ),
    );
  }
}
