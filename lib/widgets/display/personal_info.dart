
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/display/info_widget.dart';

class PersonalInfoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoWidget(
      headerColor: Theme.of(context).primaryColor,
      headerText: "Personal Info",
      bodyContent: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Name",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("Julianna Omollo ",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4),)
                  ],
                ),
              ),
              SizedBox(width: SizeConfig.blockSizeHorizontal * 0.5,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 40,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Contact",style: TextStyle(color:Colors.grey),),
                    SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
                    AutoSizeText("+254723293609",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4),)
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: SizeConfig.blockSizeHorizontal * 3,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Email",style: TextStyle(color:Colors.grey),),
              SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
              Text("test@gmail.com",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4),)
            ],
          )
        ],
      ),
    );
  }
}
