
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';

class PercentageProgressIndicator extends StatelessWidget {

  final double amountReached;  //amount that has been achieved
  final Color mainColor;  //the color the main container
  final Color indicatorColor; //the color of the indicator
  final double fullAmount;  //the target amount
  final overlay;  //widget to be placed on top of the indicator
  final double height;

  PercentageProgressIndicator({this.mainColor,this.indicatorColor,this.fullAmount,this.amountReached,this.overlay,this.height});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: SizeConfig.blockSizeVertical * 1,),
        Container(
          width: SizeConfig.blockSizeHorizontal * 100,
          height: height,
          child: Stack(
            children: <Widget>[
              Container(
                width: SizeConfig.blockSizeVertical * 100,
                height: height,
                decoration: BoxDecoration(
                    color: mainColor,
                    borderRadius: BorderRadius.all(Radius.circular(15))
                ),
              ),
              Container(
                width: _getProgressWidth(),
                height: height,
                decoration: BoxDecoration(
                    color: indicatorColor,
                    borderRadius: BorderRadius.all(Radius.circular(15))
                ),
              ),
              Center(
                child: Padding(
                 padding: EdgeInsets.only(left :SizeConfig.blockSizeHorizontal * 3,right: SizeConfig.blockSizeHorizontal * 3),
                  child: overlay ?? Text(""),
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  double _getProgressWidth(){
      return (amountReached * (SizeConfig.blockSizeHorizontal * 100)) / fullAmount;
  }
}
