
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';
import 'package:senti/widgets/display/info_widget.dart';

class PenaltyInfoWidget extends StatelessWidget {
  final bool paid;

  PenaltyInfoWidget({this.paid});

  @override
  Widget build(BuildContext context) {
    return InfoWidget(
      headerColor: Color.fromARGB(255, 150, 0, 255),
      headerText: "21 Jul, 2019",
      bodyContent: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
           Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
               Text("Late Contribution Penalty",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4),),
               Text("\$5.00",style:TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4))
             ],
           ),
          SizedBox(height: SizeConfig.blockSizeVertical * 2,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Absent Penalty",style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4),),
              Text("\$5.00",style:TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4))
            ],
          )
        ],
      ),
      footerContent: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Status",style: TextStyle(color:Colors.grey,fontSize: SizeConfig.blockSizeHorizontal * 5),),
          paid ? Text("Paid",style: TextStyle(color: Colors.green,fontSize: SizeConfig.blockSizeHorizontal * 4,fontWeight: FontWeight.bold),)
          :Text("Pending",style: TextStyle(color: Colors.red,fontSize: SizeConfig.blockSizeHorizontal * 4,fontWeight: FontWeight.bold),)
        ],
      ),
    );
  }
}
