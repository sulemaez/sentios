
import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';


class MyInfoBar extends StatelessWidget {

  final Widget content;
  final context;

  MyInfoBar(this.content,this.context);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.grey.withOpacity(0.1),
      width: SizeConfig.blockSizeHorizontal * 100,
      padding: EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 1,top: SizeConfig.blockSizeVertical * 2),
      child:Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Theme.of(context).primaryColor),
            onPressed: () => Navigator.of(context).pop(),
          ),
         content
        ],
      ),
    );
  }



}
