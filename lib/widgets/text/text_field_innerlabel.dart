import 'package:flutter/material.dart';
import 'package:senti/utils/size_config.dart';

class TextFieldWithInnerLabel extends StatefulWidget {
  final String head;
  final String value;
  final Map<String,Function> actions;
  final String errorText;
  final bool obscureText;

  TextFieldWithInnerLabel({this.head,this.value,this.actions,this.errorText,this.obscureText});

  @override
  _TextFieldWithInnerLabelState createState() => _TextFieldWithInnerLabelState();
}

class _TextFieldWithInnerLabelState extends State<TextFieldWithInnerLabel> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: SizeConfig.blockSizeVertical * 3,),
        Container(
          width: SizeConfig.blockSizeHorizontal * 100,
          child:TextFormField(
            style:new TextStyle(
              height: SizeConfig.blockSizeVertical * 0.1,
              fontSize: SizeConfig.blockSizeHorizontal * 5,
            ) ,
            decoration: InputDecoration(
              labelText: widget.head,
              labelStyle: TextStyle(color: Theme.of(context).primaryColor,fontSize: SizeConfig.blockSizeHorizontal * 4),
              filled: true,
              errorText: widget.errorText ?? "",
              fillColor: Color.fromRGBO(244, 244, 244,1),
              border:OutlineInputBorder(
                borderSide: BorderSide(
                    style: BorderStyle.none,
                    width: 0.0
                ),
              ),
            ),
            obscureText: widget.obscureText ?? false,
            onSaved:  (widget.actions != null && widget.actions['saved'] != null) ? widget.actions['saved'] : (String str){},
            validator: (widget.actions != null && widget.actions['valid'] != null) ? widget.actions['valid'] : (String str) => null,
            onChanged: (widget.actions != null && widget.actions['changed'] != null) ? widget.actions['changed']:(String str){} ,
          ),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: Color.fromRGBO(244, 244, 244,1)
          ),
        )
      ],
    );
  }
}
