import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:senti/utils/size_config.dart';

class PlainTextFormField extends StatelessWidget {
 //TODO onsaved action
  final String head;
  final IconData iconData;
  final int maxLines;
  final String value;
  final Map<String,Function> actions;
  final String errorText;
  final enabled;
  final keyboardType;

  PlainTextFormField(this.head,{this.iconData,this.maxLines,this.value,this.actions,this.errorText,this.enabled,this.keyboardType});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return  Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: SizeConfig.blockSizeVertical * 0.5,),
        Text(head,textAlign: TextAlign.left,style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 4,color: Theme.of(context).primaryColor),),
        SizedBox(height: SizeConfig.blockSizeVertical * 1,),
        Container(
          padding:EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 2) ,
          width: SizeConfig.blockSizeHorizontal * 100,
          alignment: Alignment.center,
          child: maxLines != null ? GreyTextFormField(keyboardType: keyboardType,enabled: enabled, maxLines : maxLines,value: this.value,actions: this.actions,errorText: errorText).getField() :
          GreyTextFormField(keyboardType: keyboardType,enabled: enabled, iconData:iconData,value: value,actions: this.actions,errorText:errorText ).getField(),
        ),
      ],
    );
  }
}


//create the grey textfield with given iconData and maximum lines
class GreyTextFormField {

  IconData iconData;
  int maxLines;
  final String value;
  final Map<String,Function> actions;
  final String errorText;
  final bool enabled;
  final keyboardType;

  GreyTextFormField({this.iconData,this.maxLines,this.value,this.actions,this.errorText,this.enabled,this.keyboardType});

  Widget getField() {
    return TextFormField(
        keyboardType: keyboardType ?? TextInputType.text,
        initialValue: value,
        enabled: enabled ?? true,
        style:new TextStyle(
          height: SizeConfig.blockSizeVertical * 0.1,
          fontSize: SizeConfig.blockSizeHorizontal * 5,
        ) ,
        maxLines: maxLines != null ? maxLines : 1,
        decoration: InputDecoration(
          prefixIcon: this.iconData != null ?
          Container(
            width: 3,
            padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 2),
            child: Icon(this.iconData),
            decoration: BoxDecoration(
              border: Border(
                right: BorderSide(color: Colors.grey,width: 0.4),
              ),
            ),
          ):null,
          filled: true,
          fillColor: Color.fromRGBO(244, 244, 244,1),
          errorText: this.errorText ?? "",
          border:OutlineInputBorder(
              borderRadius: BorderRadius.circular(6.0),
              borderSide: BorderSide(
                  width: 0.0,
                  style: BorderStyle.none
              )
          ),

        ),
      onSaved:  (actions != null && actions['saved'] != null) ? actions['saved'] : (String str){},
      validator: (actions != null && actions['valid'] != null) ? actions['valid'] : (String str) => null,
      onChanged: (actions != null && actions['changed'] != null) ? actions['changed']:(String str){} ,
    );
  }

}

