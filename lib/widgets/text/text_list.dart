
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senti/utils/size_config.dart';

class ListText extends StatelessWidget {

  final list;
  final textColor;
  final extras;

  ListText({this.list,this.textColor,this.extras});

  @override
  Widget build(BuildContext context){
    return Column(
       mainAxisAlignment: MainAxisAlignment.start,
       children: _getItems(context),
    );
  }

 List<Widget> _getItems(context){
    List<Widget> widgetList = [];

     list.forEach((str){
       if(extras == null) widgetList.add(Padding(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: SizeConfig.blockSizeHorizontal * 2,
                height: SizeConfig.blockSizeHorizontal * 2,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  shape: BoxShape.circle,
                ),
              ),
              SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
              Container(
                child: AutoSizeText(str,style: TextStyle(color: textColor,fontSize: SizeConfig.blockSizeHorizontal * 2)),
                width: SizeConfig.safeBlockHorizontal * 85,
              ),
            ],
          ),
         padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2),
       ));
      else
      widgetList.add( Padding(
        padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: SizeConfig.blockSizeHorizontal * 2,
                  height: SizeConfig.blockSizeHorizontal * 2,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    shape: BoxShape.circle,
                  ),
                ),
                SizedBox(width: SizeConfig.blockSizeHorizontal * 2,),
                AutoSizeText(str,style: TextStyle(color: textColor,fontSize: SizeConfig.blockSizeHorizontal * 4)),
              ],
            ),
            extras[list.indexOf(str)]
          ],
        )

      ));
    });
    return widgetList;
  }
}
