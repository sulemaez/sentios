
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:senti/blocs/create_meetings_bloc/event.dart';
import 'package:senti/blocs/create_meetings_bloc/state.dart';
import 'package:senti/pages/meetings/keynotes_page.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/alert_dialog.dart';
import 'package:senti/utils/globals.dart';

class CreateMeetingBloc extends Bloc<CreateMeetingEvent, CreateMeetingState> {

  @override
  CreateMeetingState get initialState => CreateMeetingState.initial();

  String createdMeetingUuid = "";

  @override
  Stream<CreateMeetingState> mapEventToState( CreateMeetingEvent event ) async* {
    if(event is SaveFormEvent){
      yield await _submitForm();
    }else if(event is SaveFieldEvent){
      yield _saveField(event.type,event.value);
    }else if(event is IsLoadingEvent){
      print("IS LOADING !");
      yield state.copyWith(isLoading: true);
    }else if(event is LoadingDoneEvent){
      print("LOADING DONE!");
       yield state.copyWith(isLoading: false);
    }else if(event is SuccessEvent){
        yield await _onSuccessEvent(event.context);
    }else if(event is AllFieldsSavedEvent){
        yield await _sendCreateMeetingData();
    }
  }


  //handles action of sign in button
  Future<CreateMeetingState> _submitForm() async{
      bool invalid = false;
      CreateMeetingState _state;

      //clear errors
      _state = state.copyWith(dateError: "");
      _state = _state.copyWith(timeError: "");
      try{
        if(_state.date  == ""){
          _state = _state.copyWith(dateError: "Select Date");
          invalid = true;
        }

        if(_state.time  == ""){
          _state = _state.copyWith(timeError: "Select Time");
          invalid = true;
        }

        if(_state.date != ""  && _state.time != ""){
          //heck if meeeting is later on
          final now = DateTime.now();
          DateTime date = DateTime(int.parse(state.date.split("/")[0]),int.parse(state.date.split("/")[1]),int.parse(state.date.split("/")[2]));
          DateTime time = DateTime(date.year,date.month,date.day,int.parse("${state.time.split(":")[0]}"),int.parse("${state.time.split(":")[1].substring(0,2) }"));
          if(time.isBefore(now)){
            _state.copyWith(timeError: "Time passed !");
            invalid = true;
          }
        }

        if(invalid){
          _state.formKey.currentState.validate();
          return _state;
        }

        this.add(IsLoadingEvent());

        if(_state.formKey.currentState.validate()){
          //shw loader
          _state.formKey.currentState.save();
        }else{
          this.add(LoadingDoneEvent());
          Fluttertoast.showToast(
              msg: "Please fill in all fields correclty !",
              textColor: Colors.white,
              backgroundColor: Colors.black
          );
        }
      }catch(e){
        print(e);
      }
      return _state.copyWith();
  }

  //svaes a value from a field
  CreateMeetingState _saveField(String type,String value){
    CreateMeetingState _state;

    switch(type){
      case 'purpose':
        _state = state.copyWith(purpose: value);
        break;
      case 'date':
         _state =  state.copyWith(date: value);
         break;
      case 'time':
        _state = state.copyWith(time: value);
        break;
      case 'location':
        _state =  state.copyWith(location: value);
        break;
    }

    if(_state.isValid)this.add(AllFieldsSavedEvent());

    return _state;
  }


  //send the data to api and checks the response
  Future<CreateMeetingState> _sendCreateMeetingData() async{
    try{
        Response resp = await Http.doPost(url: "$apiUrl/meeting/create",data: {
          "uuid":Auth.user.uuid,
          "group_uuid":Auth.user.groups[Auth.activeGroupIndex]["uuid"],
          "purpose": state.purpose,
          "location":state.location,
           "time":state.time,
           "date":state.date
        });

        this.add(LoadingDoneEvent());
        if(resp.statusCode  == 200 || resp.statusCode  == 200){
          print(resp.data);
          createdMeetingUuid = resp.data["uuid"];
          CreateMeetingState _state = CreateMeetingState.initial();
          return _state.copyWith(isSuccess: true);
        }else{
          Fluttertoast.showToast(msg: "Meeting not created !",backgroundColor: Colors.black,textColor: Colors.white);
          if(resp.data["errors"] != null)return  _showApiErrors(resp.data["errors"]);
        }

    }catch(e){
       this.add(LoadingDoneEvent());
       print('${e.toString()} in CreateMeeting attempt ');
    }
    return state.copyWith();
  }

  CreateMeetingState _showApiErrors(errors){
    CreateMeetingState _state = state.copyWith();
    //check if the error exists
    if(errors['purpose'] != null){
      _state = _state.copyWith(purposeError:  errors['purpose'][0]);
    }
    if(errors['date'] != null){
      _state = _state.copyWith(dateError:  errors['date'][0]);
    }
    if(errors['time'] != null){
      _state = _state.copyWith( timeError:  errors['time'][0]);
    }
    if(errors['location'] != null){
      _state = _state.copyWith(locationError:  errors['location'][0]);
    }
    print(_state.dateError);
    return _state;
  }

   Future<CreateMeetingState> _onSuccessEvent(BuildContext context)async{
      Navigator.of(context).pop();
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => KeynotesPage(meetingUuid: createdMeetingUuid,)));
      return state.copyWith(isSuccess: false);
  }
}

