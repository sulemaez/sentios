import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class CreateMeetingState extends Equatable {

  //form values
  final String purpose;
  final String date;
  final String time;
  final String location;
  final String purposeError;
  final String dateError;
  final String timeError;
  final String locationError;
  final bool isLoading;
  final bool isSuccess;
  final GlobalKey<FormState> formKey;


  const CreateMeetingState({
    @required this.isSuccess,
    @required this.formKey,
    @required this.isLoading,
    @required this.purpose,
    @required this.date,
    @required this.time,
    @required this.location,
    @required this.purposeError,
    @required this.dateError,
    @required this.timeError,
    @required this.locationError
  });

  factory CreateMeetingState.initial() {
    return CreateMeetingState(
        isSuccess: false,
        isLoading: false,
        formKey: GlobalKey<FormState>(),
        purpose : "",
        date : "",
        time : "",
        location : "",
        dateError:"",
        locationError: "",
        purposeError: "",
        timeError:""
    );
  }

   CreateMeetingState copyWith({
     bool isLoading,
     bool isSuccess,
     GlobalKey<FormState> formKey,
     String purpose,
     String date,
     String time,
     String location,
     String dateError,
     String locationError,
     String purposeError,
     String timeError,
   }) {
    return CreateMeetingState(
        formKey: formKey ?? this.formKey,
        isLoading: isLoading ?? this.isLoading,
        isSuccess: isSuccess ?? this.isSuccess,
        purpose : purpose  ?? this.purpose,
        date : date ?? this.date,
        time : time ?? this.time,
        location : location ?? this.location,
        purposeError: purposeError ?? this.purposeError,
        locationError: locationError ?? this.locationError,
        dateError: dateError ?? this.dateError,
        timeError: timeError ?? this.timeError
    );
  }

  get isValid => this.purpose != "" && this.location != "" && this.time != "" && this.date != "";

  @override
  List<Object> get props => [
     formKey,isLoading,isSuccess,purpose,
     date, time, location,locationError,purposeError,timeError,dateError
  ];


}