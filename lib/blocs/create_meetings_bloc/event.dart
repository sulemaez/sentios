import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class CreateMeetingEvent extends Equatable {
  const CreateMeetingEvent();

  @override
  List<Object> get props => [];
}

class SaveFormEvent extends CreateMeetingEvent{

  SaveFormEvent();

  @override
  List<Object> get props => [];
}

class SaveFieldEvent extends CreateMeetingEvent{
  final String type;
  final String value;

  SaveFieldEvent({this.type,this.value});

  @override
  List<Object> get props => [type,value];
}

class IsLoadingEvent extends CreateMeetingEvent{
  IsLoadingEvent();
}

class LoadingDoneEvent extends CreateMeetingEvent{
     LoadingDoneEvent();
}

class SuccessEvent extends CreateMeetingEvent{
  final BuildContext context;

  SuccessEvent(this.context);
}
class AllFieldsSavedEvent extends CreateMeetingEvent{
     AllFieldsSavedEvent();
}