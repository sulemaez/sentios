import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class CreateGroupState extends Equatable {

  //form values
  final String name;
  final String purpose;
  final double targetAmount;
  final String location;
  final bool isSuccess;
  final bool isLoading;
  final List<Map<String,dynamic>> penalties;
  final List<Map<String,dynamic>> loanTerms;
  final String constitution;
  final GlobalKey<FormState> formKey;
  final String minimumContribution;
  final String penaltiesError;
  final String loanTermError;

  const CreateGroupState({
    @required this.penaltiesError,
    @required this.penalties,
    @required this.loanTerms,
    @required this.constitution,
    @required this.name,
    @required this.purpose,
    @required this.targetAmount,
    @required this.location,
    @required this.formKey,
    @required this.isLoading,
    @required this.isSuccess,
    @required this.minimumContribution,
    @required this.loanTermError
  });

  factory CreateGroupState.initial() {
    return CreateGroupState(
        loanTermError: null,
        isSuccess: false,
        formKey: GlobalKey<FormState>(),
        isLoading: false,
        name: "",
        constitution: "",
        loanTerms:[],
        penalties: [],
        location: "",
        purpose: "",
        targetAmount: 0,
        minimumContribution: "",
        penaltiesError: null
    );
  }

  CreateGroupState copyWith({
    bool isLoading,
    bool isSuccess,
    GlobalKey<FormState> formKey,
    String name,
    String constitution,
    List<Map<String,dynamic>> loanTerms,
    List<Map<String,dynamic>> penalties,
    String location,
    String purpose,
    double targetAmount,
    String minimumContribution,
    String penaltiesError,
    String loanTermError
  }) {
    return CreateGroupState(
        formKey: formKey ?? this.formKey,
        isLoading: isLoading ?? this.isLoading,
        isSuccess: isSuccess ?? this.isSuccess,
        name: name ?? this.name,
        targetAmount : targetAmount ?? this.targetAmount,
        purpose: purpose ?? this.purpose,
        location:  location ?? this.location,
        constitution: constitution ?? this.constitution,
        loanTerms: loanTerms ?? this.loanTerms,
        penalties: penalties ?? this.penalties,
        minimumContribution: minimumContribution ?? this.minimumContribution,
        penaltiesError: penaltiesError ?? this.penaltiesError,
        loanTermError: loanTermError ?? this.loanTermError
    );
  }

  get isValid => name != "" && purpose != "" && targetAmount != 0 && location != ""
      && constitution != "" && minimumContribution != "" && penalties.length == 5 && penaltiesError == null
      && (loanTermError == null  || loanTermError == "");

  @override
  List<Object> get props => [
   formKey,isLoading,isSuccess,name,purpose,targetAmount,
   location,penalties,loanTerms,constitution,minimumContribution,penaltiesError,loanTermError
  ];


}