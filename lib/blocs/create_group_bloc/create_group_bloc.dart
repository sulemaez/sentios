
import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals.dart';
import 'package:senti/utils/validators.dart';
import 'create_group_event.dart';
import 'create_group_state.dart';

class CreateGroupBloc extends Bloc<CreateGroupEvent, CreateGroupState> {

  @override
  CreateGroupState get initialState => CreateGroupState.initial();

  bool actionOnGoing = false;
  bool done = false;

  @override
  Stream<CreateGroupState> mapEventToState( CreateGroupEvent event ) async* {
    if(event is SaveEvent){
       yield await _saveFormField(event.type,event.value);
    }else if(event is SubmitFormEvent){
       yield  _onSubmitForm();
    }else if(event is IsLoadingEvent){
       yield state.copyWith(isLoading:  true);
    }else if(event is LoadingDoneEvent){
      yield state.copyWith(isLoading: false);
    }else if(event is FormSavedEvent){
       yield await _allFieldsSaved();
    }else if(event is AddLoanTermEvent){
       yield await _addLoanTerm(event.duration,event.value);
    }else if(event is RemoveLoanTermEvent){
       yield await _removeLoanTerm(event.duration);
    }

  }

  //called by on save  of a p textformfield
 Future<CreateGroupState> _saveFormField(String type, String value) async{
      CreateGroupState _state;
      switch(type){
        case "name":
          _state = state.copyWith(name: value);
          break;
        case "purpose":
          _state = state.copyWith(purpose: value);
          break;
        case "location":
          _state = state.copyWith(location: value);
          break;
        case "target":
          _state = state.copyWith(targetAmount: double.parse(value));
          break;
        case "minimum":
          _state = state.copyWith(minimumContribution: value);
          break;
        case "constitution":
          _state = state.copyWith(constitution: value);
          break;
        default:

          String error = validateNumber(value);

          if(error == null){
             List<Map<String,dynamic>> list = state.penalties;
             int index = -1;
             for(Map<String,dynamic> item in list){
               if(item["name"] == type){
                 index = list.indexOf(item);
                 break;
               }
             }

             if(index >= 0){
               list[index]['amount'] = double.parse(value);
             }else{
               list.add({ "name" : type, "amount" : double.parse(value) });
             }

            _state = state.copyWith(penaltiesError: null,penalties: list);
          }else{

              Fluttertoast.showToast(
                  msg: "Penalties $error",
                  textColor: Colors.white,
                  backgroundColor: Colors.black
              );
             _state = state.copyWith(penaltiesError: "Penalties $error",isLoading: false);
          }

      }

      if(_state.isValid){
        this.add(FormSavedEvent());
        _state =  _state.copyWith(isLoading: true);
      }else{
        _state = _state.copyWith(isLoading: false);
        actionOnGoing = false;
        if(!(_state.loanTermError == null ||  _state.loanTermError == "")){
          Fluttertoast.showToast(
              msg: "Loan Terms are invalid",
              backgroundColor: Colors.black,
              textColor: Colors.white
          );
        }
      }

      return _state;
  }

  //submit button clicked
  CreateGroupState _onSubmitForm(){
     if(actionOnGoing)return state.copyWith(penaltiesError: null,isSuccess: false);
     actionOnGoing = true;
     if(state.formKey.currentState.validate()){
        state.formKey.currentState.save();
        return state.copyWith(penaltiesError: null,isSuccess: false,isLoading: true);
     }else{
       actionOnGoing = false;
       Fluttertoast.showToast(
           msg: "Please fill all fields correctly !",
           textColor: Colors.white,
           backgroundColor: Colors.black
       );
       return state.copyWith(penaltiesError: null,isSuccess: false,isLoading: false);
     }

  }

  //all field items saved
  Future<CreateGroupState> _allFieldsSaved() async{
     if(done)return state.copyWith();
     print("set is loading all fields saved");

      CreateGroupState _state;
      if(state.loanTerms.length > 0 && state.penalties.length > 0){
         return await _sendData();
      }else{
        actionOnGoing = false;
        print("set is loading done all fields saved failed");
        if(!state.isSuccess) Fluttertoast.showToast(
            msg: "Please set loan terms ",
            textColor: Colors.white,
            backgroundColor: Colors.black
         );
         return state.copyWith(isLoading: false);
      }
  }

  //send the data to the backend
  Future<CreateGroupState> _sendData() async{
    if(done)return state.copyWith();
    CreateGroupState _state = state.copyWith();
    try{
      var data = {
        "uuid": Auth.user.uuid,
        "name":state.name,
        "purpose":state.purpose,
        "target_amount":state.targetAmount,
        "minimum_contribution":state.minimumContribution,
        "location":state.location,
        "penalties":state.penalties,
        "loan_terms":state.loanTerms,
        "constitution":state.constitution
      };

      //get response from auth
      Response response = await Http.doPost(data: data,url: "$apiUrl/group/create");

      if(response.statusCode == 200 || response.statusCode == 201){
        done = true;
        //added created group to group details
        Auth.user.groups.add(response.data["data"]);
        //to do when successfully
        _state = CreateGroupState.initial();
        _state  = _state.copyWith(isSuccess: true);
        Fluttertoast.showToast(
            msg: "Group created SuccessFully !",
            textColor: Colors.white,
            backgroundColor: Colors.deepPurple,
            gravity: ToastGravity.CENTER,
        );
      }else{
        //TODO display the errors encountered
        Fluttertoast.showToast(
            msg: "Group creation not Successful \n Check errors !",
            textColor: Colors.white,
            backgroundColor: Colors.black
        );
        //show
        //TODO show errors from api
      }
      //todo when failed

    }catch(e){
      print('$e in sending create request');
    }
    actionOnGoing = false;
    print("set is loading done send data done");
    this.add(LoadingDoneEvent());
    return _state;
  }

  //add loan term
 Future<CreateGroupState> _addLoanTerm(int duration,String value) async{
   String errorMessage = validateNumber(value);

   bool found = false;
   bool errorFound = false;

   List<Map<String,dynamic>> list = state.loanTerms;
   await list.forEach((element){
      if(element['months'] == duration){
         //if the input is invalid set the field to error
         //this is to enable easy search of invalid fields
         element['interest'] = errorMessage == null ? double.parse(value) : "error";
         found = true;
         //if value is invalid retain errorMessage
         if(element['interest'] == "error")errorFound = true;
      }
   });

   if(!found){
     list.add({
       "months":duration,
       "interest":errorMessage == null ? double.parse(value) : "error"
     });
   }

   //if error found retain error message
   if(errorFound || errorMessage != null){
      if(state.loanTermError != null)errorMessage = "${state.loanTermError}\n$errorMessage";
      return state.copyWith(loanTermError: errorMessage,loanTerms: list);
   }

   return state.copyWith(loanTerms: list,loanTermError: "");
 }

  Future<CreateGroupState> _removeLoanTerm(int duration) async{
    List<Map<String,dynamic>> list = state.loanTerms;
    Map<String,dynamic> item = list.firstWhere((element) => element['months'] == duration);
    if(item != null)list.remove(item);
    return state.copyWith(loanTerms: list);
  }
}

