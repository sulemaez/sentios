import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class CreateGroupEvent extends Equatable {
  const CreateGroupEvent();

  @override
  List<Object> get props => [];
}

class SaveEvent extends CreateGroupEvent{
  final String type;
  final String value;

  SaveEvent({this.value,this.type});
}

class SubmitFormEvent extends CreateGroupEvent{
}

class IsLoadingEvent extends CreateGroupEvent{
}

class AddLoanTermEvent extends CreateGroupEvent{
  final int duration;
  final String value;

  AddLoanTermEvent(duration,this.value):
      this.duration = int.parse(duration);
}


class RemoveLoanTermEvent extends CreateGroupEvent{
  final int duration;

    RemoveLoanTermEvent(duration):
        this.duration = int.parse(duration);
}

class LoadingDoneEvent extends CreateGroupEvent{

}

class FormSavedEvent extends CreateGroupEvent{

}
