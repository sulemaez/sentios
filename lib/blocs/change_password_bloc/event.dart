import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ChangePasswordEvent extends Equatable {
  const ChangePasswordEvent();

  @override
  List<Object> get props => [];
}

class SaveFormEvent extends ChangePasswordEvent{
  SaveFormEvent();
}

class SaveFieldEvent extends ChangePasswordEvent{
  final String type;
  final String value;
  final bool formSave;

  SaveFieldEvent({this.type,this.value,this.formSave = true});

  @override
  List<Object> get props => [type,value,formSave];
}

class IsLoadingEvent extends ChangePasswordEvent{
  IsLoadingEvent();
}

class LoadingDoneEvent extends ChangePasswordEvent{
     LoadingDoneEvent();
}

class SuccessEvent extends ChangePasswordEvent{
  final BuildContext context;

  SuccessEvent(this.context);
}
class AllFieldsSavedEvent extends ChangePasswordEvent{
     AllFieldsSavedEvent();
}