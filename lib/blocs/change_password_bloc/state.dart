import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class ChangePasswordState extends Equatable {

  //form values
  final String oldPassword;
  final String newPassword;
  final String confirmPassword;
  final String oldPasswordError;
  final String newPasswordError;
  final String confirmPasswordError;
  final bool isLoading;
  final bool isSuccess;
  final GlobalKey<FormState> formKey;


  const ChangePasswordState({
    @required this.isSuccess,
    @required this.oldPassword,
    @required this.newPassword,
    @required this.confirmPassword,
    @required this.formKey,
    @required this.isLoading,
    @required this.oldPasswordError,
    @required this.newPasswordError,
    @required this.confirmPasswordError
  });

  factory ChangePasswordState.initial() {
    return ChangePasswordState(
        isSuccess: false,
        formKey: GlobalKey<FormState>(),
        isLoading: false,
        confirmPassword: "",
        newPassword: "",
        oldPassword: "",
        confirmPasswordError: "",
        newPasswordError: "",
        oldPasswordError: ""
    );
  }

   ChangePasswordState copyWith({
     String oldPassword,
     String newPassword,
     String confirmPassword,
     bool isLoading,
     bool isSuccess,
     GlobalKey<FormState> formKey,
     String confirmPasswordError,
     String newPasswordError,
     String oldPasswordError
   }) {
    return ChangePasswordState(
        confirmPassword: confirmPassword ?? this.confirmPassword,
        newPassword:  newPassword ?? this.newPassword,
        oldPassword: oldPassword ?? this.oldPassword,
        formKey: formKey ?? this.formKey,
        isLoading: isLoading ?? this.isLoading,
        isSuccess: isSuccess ?? this.isSuccess,
        oldPasswordError: oldPasswordError ?? this.oldPasswordError,
        newPasswordError: newPasswordError ?? this.newPasswordError,
        confirmPasswordError: confirmPasswordError ?? this.confirmPasswordError
    );
  }

  get isValid => this.newPassword != "" && this.oldPassword != "" && (this.confirmPassword != "" && this.newPassword == this.confirmPassword);

  @override
  List<Object> get props => [
      formKey,isLoading,isSuccess,newPassword,oldPassword,confirmPassword,
      newPasswordError,oldPasswordError,confirmPasswordError
  ];


}