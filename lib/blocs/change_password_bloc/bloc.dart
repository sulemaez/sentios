
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:senti/blocs/change_password_bloc/event.dart';
import 'package:senti/blocs/change_password_bloc/state.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/alert_dialog.dart';
import 'package:senti/utils/globals.dart';


class ChangePasswordBloc extends Bloc<ChangePasswordEvent, ChangePasswordState> {

  @override
  ChangePasswordState get initialState => ChangePasswordState.initial();

  @override
  Stream<ChangePasswordState> mapEventToState( ChangePasswordEvent event ) async* {
    if(event is SaveFormEvent){
      yield await _submitForm();
    }else if(event is SaveFieldEvent){
      yield _saveField(event.type,event.value,formSave: event.formSave);
    }else if(event is IsLoadingEvent){
      print("IS LOADING !");
      yield state.copyWith(isLoading: true);
    }else if(event is LoadingDoneEvent){
      print("LOADING DONE!");
       yield state.copyWith(isLoading: false);
    }else if(event is SuccessEvent){
        yield _onSuccessEvent(event.context);
    }else if(event is AllFieldsSavedEvent){
        yield await _sendChangePasswordData();
    }
  }


  //handles action of sign in button
  Future<ChangePasswordState> _submitForm() async{
      this.add(IsLoadingEvent());

      if(state.formKey.currentState.validate()){
         //shw loader
          state.formKey.currentState.save();
      }else{
        this.add(LoadingDoneEvent());
        Fluttertoast.showToast(
            msg: "Please fill in all fields correclty !",
            textColor: Colors.white,
            backgroundColor: Colors.black
        );
      }

      return state.copyWith();
  }

  //svaes a value from a field
  ChangePasswordState _saveField(String type,String value,{bool formSave = true}){
    ChangePasswordState _state;

    switch(type){
      case 'oldPassword':
        _state = state.copyWith(oldPassword: value);
        break;
      case 'newPassword':
         _state =  state.copyWith(newPassword: value);
          break;
      case 'confirmPassword':
         _state = state.copyWith(confirmPassword:value);
         break;
    }

    print("${_state.isValid}   == $formSave");
    if(_state.isValid && formSave)this.add(AllFieldsSavedEvent());

    return _state;
  }


  //send the logom dtata to api and checks the response
  //handles action of sign in button
  Future<ChangePasswordState> _sendChangePasswordData() async{
      try{
        Response resp = await Http.doPost(url: "$apiUrl/auth/changepassword",data: {
          "uuid":Auth.user.uuid,
          "old_password":state.oldPassword,
          "password" : state.newPassword,
          "password_confirmation": state.confirmPassword
        });

        print(resp.data);
        print(resp.statusMessage);
        print(resp.statusCode);
        this.add(LoadingDoneEvent());
        if((resp.statusCode  == 200 || resp.statusCode  == 200) && (resp.data["message"] != "")){
          print("OKAY GUYS");
          ChangePasswordState _state = ChangePasswordState.initial();
          return _state.copyWith(isSuccess: true);
        }else{
          print("NOT OKAY GUYS");
          Fluttertoast.showToast(msg: "Password not changed !",backgroundColor: Colors.black,textColor: Colors.white);
          if(resp.data["errors"] != null)return  _showApiErrors(resp.data["errors"]);
        }

      }catch(e){
        Fluttertoast.showToast(msg: "Password not changed !",backgroundColor: Colors.black,textColor: Colors.white);
        print('$e in ChangePassword attempt ');
      }
    this.add(LoadingDoneEvent());
    return state.copyWith();
  }

  ChangePasswordState _onSuccessEvent(BuildContext context){
    //if logged in go to home page
    showCustomAlertDialog(context,message: "Password changed successfully !");
    return state.copyWith(isSuccess: false);
  }

  ChangePasswordState _showApiErrors(errors){
    print(errors);
    ChangePasswordState _state = state.copyWith(isSuccess: false);

    //check if the error exists
    if(errors["old_password"] != null){
      _state = _state.copyWith(oldPasswordError:  errors["old_password"][0]);
    }
    if(errors['password'] != null){
      _state = _state.copyWith(newPasswordError:  errors['date'][0]);
    }
    if(errors["password_confirmation"] != null){
      _state = _state.copyWith( confirmPasswordError:  errors["password_confirmation"][0]);
    }

    return _state;
  }

}

