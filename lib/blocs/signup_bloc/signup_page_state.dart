import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class SignUpPageState extends Equatable {

  //form values
  final String email;
  final int gender;
  final String name;
  final String contact;
  final String password;
  final String confirmPassword;
  final File frontImage;
  final File backImage;
  final File picImage;

  //calender values
  final String dayValue;
  final int monthValue ;
  final String yearValue;


  //miscellinious varibales used within app
  final bool obscurePassword ;
  final bool obscureConfirmPassword;
  final GlobalKey<FormState> formKey;

  //show loader
  final bool isLoading;
  final bool isSuccess;

  //errors
  final String nameError;
  final String emailError;
  final String passwordError;
  final String contactError;
  final bool memberRegistration;

  const SignUpPageState({
    @required this.nameError,
    @required this.emailError,
    @required this.passwordError,
    @required this.contactError,
    @required this.email,
    @required this.contact,
    @required this.name,
    @required this.gender,
    @required this.password,
    @required this.confirmPassword,
    @required this.frontImage,
    @required this.backImage,
    @required this.picImage,
    @required this.dayValue,
    @required this.monthValue,
    @required this.yearValue,
    @required this.obscurePassword,
    @required this.obscureConfirmPassword,
    @required this.formKey,
    @required this.isLoading,
    @required this.isSuccess,
    @required this.memberRegistration
  });

  factory SignUpPageState.initial({memberRegistration}) {
    return SignUpPageState(
      memberRegistration : memberRegistration,
      email: '',
      gender : 0,
      dayValue : "1",
      monthValue : 1,
      yearValue : "1990",
      obscurePassword: true,
      obscureConfirmPassword:true,
      contact: "",
      name: "",
      picImage: null,
      frontImage: null,
      confirmPassword: "",
      backImage: null,
      formKey: GlobalKey<FormState>(),
      password: "",
      isLoading: false,
      isSuccess: false,
      contactError: "",
      emailError: "",
      nameError: "",
      passwordError: ""
    );
  }

  SignUpPageState copyWith({
    String email,
    String password,
    int gender,
    String dayValue,
    int monthValue,
    String yearValue,
    bool obscurePassword,
    bool obscureConfirmPassword,
    String contact,
    String name,
    File picImage,
    File frontImage,
    String confirmPassword,
    File backImage,
    GlobalKey formKey,
    bool isLoading,
    bool isSuccess,
    String nameError,
    String emailError,
    String contactError,
    String passwordError,
    bool memberRegistration
  }) {
    return SignUpPageState(
      email: email ?? this.email,
      password: password ?? this.password,
      gender : gender ?? this.gender,
      dayValue : dayValue ?? this.dayValue,
      monthValue : monthValue ?? this.monthValue,
      yearValue : yearValue ?? this.yearValue,
      obscurePassword: obscurePassword ?? this.obscurePassword,
      obscureConfirmPassword: obscureConfirmPassword ?? this.obscureConfirmPassword,
      contact: contact ?? this.contact,
      name: name ?? this.name,
      picImage: picImage ?? this.picImage,
      frontImage: frontImage ?? this.frontImage,
      confirmPassword: confirmPassword ?? this.confirmPassword,
      backImage: backImage ?? this.backImage,
      formKey: formKey ?? this.formKey,
      isLoading: isLoading ?? this.isLoading,
      isSuccess : isSuccess ?? this.isSuccess,
      nameError: nameError ?? this.nameError,
      passwordError: passwordError ?? this.passwordError,
      emailError: emailError ?? this.emailError,
      contactError: contactError ?? this.contactError,
      memberRegistration: this.memberRegistration
    );
  }

  get isValid => email.trim() != "" && name.trim() != "" && contact.trim() != "" &&
      (memberRegistration ? true : (password.trim() != "" && confirmPassword.trim() != ""));


  @override
  List<Object> get props => [
    email,gender,name,contact,password,
    confirmPassword,frontImage,backImage,
    picImage,dayValue, monthValue,
    yearValue, obscurePassword,isSuccess,
    obscureConfirmPassword,isLoading,nameError,
    emailError,contactError,passwordError,
    memberRegistration
  ];


}