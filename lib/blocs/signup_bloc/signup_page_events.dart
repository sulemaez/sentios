
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:senti/blocs/signup_bloc/images.dart';

@immutable
abstract class SignUpPageEvent extends Equatable {
  const SignUpPageEvent();

  @override
  List<Object> get props => [];
}

//handles when gender radi clcicked
class GenderChangedEvent extends SignUpPageEvent{
  final int gender;

  GenderChangedEvent(this.gender);

  @override
  List<Object> get props => [gender];
}

//saves password when password is being keyed in
class PasswordChangedEvent extends SignUpPageEvent{
  final String password;

  PasswordChangedEvent(this.password);

  @override
  List<Object> get props => [password];
}

//saves the date when selected
class DayValueChangedEvent extends SignUpPageEvent{
  final String dayValue;

  DayValueChangedEvent(this.dayValue);

  @override
  List<Object> get props => [dayValue];
}

//saves month when selected
class MonthValueChangedEvent extends SignUpPageEvent{
  final int monthValue;

 MonthValueChangedEvent(this.monthValue);

  @override
  List<Object> get props => [monthValue];
}

//saves year when selected
class YearValueChangedEvent extends SignUpPageEvent{
  final String yearValue;

  YearValueChangedEvent(this.yearValue);

  @override
  List<Object> get props => [yearValue];
}

//initiates loader to shown
class IsLoadingEvent extends SignUpPageEvent{
  IsLoadingEvent();
}
//loader to be hidden
class LoadingDoneEvent extends SignUpPageEvent{
  LoadingDoneEvent();
}

//obsucure text in password filed
class ObscureTextEvent extends SignUpPageEvent{
  final String type;

  ObscureTextEvent({this.type});

  @override
  List<Object> get props => [type];
}

//when submit button pressed
class SubmitFormEvent extends SignUpPageEvent{
}

//initiates  image picker to pick image
class GetImageEvent extends SignUpPageEvent{
  final SignUpImage image;
  final String source;

  GetImageEvent(this.image,this.source);

  @override
  List<Object> get props => [image];
}


//event to initiate savinfg of form fields
class SaveEvent extends SignUpPageEvent{
  final String  type;
  final String value;

  SaveEvent({this.value,this.type});

  @override
  List<Object> get props => [type,value];
}

//when all form fileds are saved
class FormSavedEvent extends SignUpPageEvent{
    FormSavedEvent();
}

class SuccessEvent extends SignUpPageEvent{
  final BuildContext context;
  final widget;
  SuccessEvent(this.widget,this.context);
}