import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:senti/blocs/signup_bloc/images.dart';
import 'package:senti/blocs/signup_bloc/signup_page_events.dart';
import 'package:senti/blocs/signup_bloc/signup_page_state.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/utils/globals/images.dart';
import 'package:senti/utils/size_config.dart';


class SignUpPageBloc extends Bloc<SignUpPageEvent, SignUpPageState> {

  final bool memberRegistration;
  bool sentRequest = false;

  SignUpPageBloc({this.memberRegistration = false});

  @override
  SignUpPageState get initialState => SignUpPageState.initial(memberRegistration : memberRegistration);

  @override
  Stream<SignUpPageState> mapEventToState( SignUpPageEvent event ) async* {

    if(event is GenderChangedEvent){
        //gender toggle
        yield state.copyWith( gender: event.gender);
    }else if( event is DayValueChangedEvent){
         //dob day
         yield state.copyWith( dayValue: event.dayValue);
    }else if(event is MonthValueChangedEvent){
         //dob month
         yield state.copyWith(monthValue: event.monthValue);
    }else if(event is YearValueChangedEvent){
        //dob year
        yield state.copyWith(yearValue: event.yearValue);
    }else if(event is ObscureTextEvent){
        //obscure password
        yield _obscureText(event.type);
    }else if(event is SubmitFormEvent){
        //form submitted
        yield await _submitForm();
    }else if(event is PasswordChangedEvent){
       //password changed
       yield state.copyWith(password: event.password);
    }else if(event is GetImageEvent){
       yield await _getImage(event.image,event.source);
    }else if(event is SaveEvent){
        //on save for a particular field
        yield _saveFormElement(event.type,event.value);
    }else if(event is IsLoadingEvent){
       yield state.copyWith(isLoading: true);
    }else if(event is LoadingDoneEvent){
       yield state.copyWith(isLoading: false);
    } else if(event is FormSavedEvent){
      //when all fields have been saved
      yield await _allFieldsSaved();
    }else if(event is SuccessEvent){
       _onSuccessEvent(event.widget,event.context);
       yield state.copyWith(isSuccess: false);
    }

  }


  //function to obscure password
  SignUpPageState _obscureText(String type) {
    bool obscure;
    if(type == "password"){
       //obsucre password
       obscure = state.obscurePassword ? false : true;
       return state.copyWith(obscurePassword: obscure);
    }else{
      //obsucre password confirmation
      obscure = state.obscureConfirmPassword ? false : true;
      return state.copyWith(obscureConfirmPassword: obscure);
    }
  }
  
  //gets a image
  //TODo add get image from camera
  Future<SignUpPageState> _getImage(SignUpImage imageType,String source) async{
    var image = await getImage(source);

    if(image == null)return state.copyWith();
    //save the image
    switch(imageType){
      case SignUpImage.FRONT :
         return state.copyWith(frontImage: image);
         break;
      case SignUpImage.BACK :
        return state.copyWith( backImage: image);
        break;
      case SignUpImage.PIC :
        return state.copyWith(picImage: image);
        break;
      default:
        return state.copyWith();
    }

  }
  
  
  //function called on sign up button pressed
  Future<SignUpPageState> _submitForm()async {
    if(sentRequest)return state.copyWith();
    sentRequest = true;

    this.add(IsLoadingEvent());
    //validate fields
    if(state.formKey.currentState.validate()){
      //save the fields
      //after all fields saved the form will be subitted
      state.formKey.currentState.save();
    }else{
      sentRequest = false;
      this.add(LoadingDoneEvent());
      Fluttertoast.showToast(
          msg: "Please fill all fields correctly !",
          textColor: Colors.white,
          backgroundColor: Colors.black
      );
    }

    return state.copyWith();
  }


  //called after all the fields are set
  Future<SignUpPageState> _allFieldsSaved() async{

    SignUpPageState _state;
    //check if the images are set
    if(state.picImage  != null && state.backImage != null && state.frontImage != null){
      _state = await _sendRegisterData();
    }else{
      sentRequest = false;
      Fluttertoast.showToast(
          msg: "Please add all images !",
          textColor: Colors.white,
          backgroundColor: Colors.black
      );
      _state = state.copyWith();
    }

    this.add(LoadingDoneEvent());
    return _state;
  }

 //send the login details to the api using auth srvice
  Future<SignUpPageState> _sendRegisterData() async {

      SignUpPageState _state = state.copyWith();
      try{
        Map<String,dynamic> created;
        if(!memberRegistration){
          //get response from auth
          created = await Auth.signUp(state);
        }else{
          //get response from auth
          created = await Auth.signUp(state,registerMember: true);
        }
        if(created['created']){
          //to do when successfully

          _state = SignUpPageState.initial();
          _state  = _state.copyWith(isSuccess: true);
        }else{
           //TODO display the errors encountered
          Fluttertoast.showToast(
              msg: "Registration not successful check errors !",
              textColor: Colors.white,
              backgroundColor: Colors.black
          );
          //show
          _state = _showApiErrors(created['data']["errors"]);
        }
        //to do when failed
      }catch(e){
         print('$e in sending register request');
      }

      sentRequest = false;
      return _state;
    }


  //saves the form elements n validation passed
  SignUpPageState _saveFormElement(String type, String value) {

    SignUpPageState _state;
     switch(type){
       case "name":
         _state = state.copyWith(name: value);
          break;
       case "contact":
         _state = state.copyWith(contact: value);
         break;
       case "email":
         _state = state.copyWith(email: value);
         break;
       case "confirm":
         _state = state.copyWith(confirmPassword: value);
         break;
       default:
         _state = state.copyWith();
         break;
     }

     if(_state.isValid)this.add(FormSavedEvent());

     return _state;
  }

  void _onSuccessEvent(widget,context){
    Fluttertoast.showToast(
        msg: "Registration Successfull ",
        gravity: ToastGravity.CENTER,
        backgroundColor: Theme.of(context).primaryColor,
        textColor: Colors.white,
        fontSize:SizeConfig.blockSizeHorizontal * 5,
        toastLength: Toast.LENGTH_LONG
    );

    try{
        if(!memberRegistration)widget.tabController.index = 0;
    }catch(e){
      print(e);
    }
  }

  SignUpPageState _showApiErrors(Map<String,dynamic> errors) {
    SignUpPageState _state = state.copyWith();
    //check if the error exists
    if(errors['name'] != null){
      _state = _state.copyWith(nameError:  errors['name'][0]);
    }
    if(errors['email'] != null){
      _state = _state.copyWith(emailError:  errors['email'][0]);
    }
    if(errors['password'] != null){
      _state = _state.copyWith( passwordError :  errors['password'][0]);
    }
    if(errors['phone_number'] != null){
      _state = _state.copyWith(contactError:  errors['phone_number'][0]);
    }

    String message = "";
    if(errors['profile_picture'] != null){
      message = errors['profile_picture'][0];
    }

    if(errors['id_image_front'] != null){
      message = "$message\n${errors['id_image_front'][0]}";
    }

    if(errors['id_image_back'] != null){
      message = "$message\n${errors['id_image_back'][0]}";
    }

    if(message != "")Fluttertoast.showToast(
        msg: message,
        gravity: ToastGravity.CENTER,
        backgroundColor:Colors.black,
        textColor: Colors.white,
        toastLength: Toast.LENGTH_LONG
    );
    return _state;
  }
}


