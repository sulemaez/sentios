
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:senti/blocs/login_bloc/login_event.dart';
import 'package:senti/blocs/login_bloc/login_state.dart';
import 'package:senti/pages/base_page.dart';
import 'package:senti/services/auth.dart';


class LoginBloc extends Bloc<LoginEvent, LoginState> {

  @override
  LoginState get initialState => LoginState.initial();

  @override
  Stream<LoginState> mapEventToState( LoginEvent event ) async* {
    if(event is SaveFormEvent){
      yield await _submitForm();
    }else if(event is SaveFieldEvent){
      yield _saveField(event.type,event.value);
    }else if(event is IsLoadingEvent){
      print("IS LOADING !");
      yield state.copyWith(isLoading: true);
    }else if(event is LoadingDoneEvent){
      print("LOADING DONE!");
       yield state.copyWith(isLoading: false);
    }else if(event is SuccessEvent){
        yield _onSuccessEvent(event.context);
    }else if(event is AllFieldsSavedEvent){
        yield await _sendLoginData();
    }
  }


  //handles action of sign in button
  Future<LoginState> _submitForm() async{
      this.add(IsLoadingEvent());
      if(state.formKey.currentState.validate()){
         //shw loader
          state.formKey.currentState.save();
      }else{
        this.add(LoadingDoneEvent());
        Fluttertoast.showToast(
            msg: "Please fill in all fields correclty !",
            textColor: Colors.white,
            backgroundColor: Colors.black
        );
      }

      return state.copyWith();
  }

  //svaes a value from a field
  LoginState _saveField(String type,String value){
    LoginState _state;

    switch(type){
      case 'email':
        _state = state.copyWith(email: value);
        break;
      case 'password':
         _state =  state.copyWith(password: value);
         break;
    }

    if(_state.isValid)this.add(AllFieldsSavedEvent());

    return _state;
  }


  //send the logom dtata to api and checks the response
  //handles action of sign in button
  Future<LoginState> _sendLoginData() async{
      try{
         print("OLOO");
         Map<String,dynamic> loggedIn = await Auth.login(state.email,state.password);
         this.add(LoadingDoneEvent());
         if(!loggedIn['internet']) return state.copyWith();

         if(loggedIn['logged']) {
           return state.copyWith(isSuccess : true);
         }
         print("Here");
         //if not logged in
         Fluttertoast.showToast(
             msg: "Invalid Email or Password !",
             textColor: Colors.white,
             backgroundColor: Colors.black,
             gravity: ToastGravity.CENTER
         );
      }catch(e){
        print('$e in Login attempt ');
      }

    return state.copyWith();
  }

  LoginState _onSuccessEvent(BuildContext context){
    //if logged in go to home page
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) => HomeBase(),
        ),
            (Route<dynamic> r) => false
    );

    return state.copyWith();
  }
}

