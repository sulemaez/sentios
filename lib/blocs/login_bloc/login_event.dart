import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class SaveFormEvent extends LoginEvent{

  final BuildContext  context;

  SaveFormEvent(this.context);

  @override
  List<Object> get props => [context];
}

class SaveFieldEvent extends LoginEvent{
  final String type;
  final String value;

  SaveFieldEvent({this.type,this.value});

  @override
  List<Object> get props => [type,value];
}

class IsLoadingEvent extends LoginEvent{
  IsLoadingEvent();
}

class LoadingDoneEvent extends LoginEvent{
     LoadingDoneEvent();
}

class SuccessEvent extends LoginEvent{
  final BuildContext context;

  SuccessEvent(this.context);
}
class AllFieldsSavedEvent extends LoginEvent{
     AllFieldsSavedEvent();
}