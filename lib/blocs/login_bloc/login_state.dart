import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class LoginState extends Equatable {

  //form values
  final String email;
  final String password;
  final bool isLoading;
  final bool isSuccess;
  final GlobalKey<FormState> formKey;


  const LoginState({
    @required this.isSuccess,
    @required this.email,
    @required this.password,
    @required this.formKey,
    @required this.isLoading
  });

  factory LoginState.initial() {
    return LoginState(
        isSuccess: false,
        email: '',
        password: '',
        formKey: GlobalKey<FormState>(),
        isLoading: false
    );
  }

   LoginState copyWith({
     String email,
     String password,
     bool isLoading,
     bool isSuccess,
     GlobalKey<FormState> formKey
   }) {
    return LoginState(
        email: email ?? this.email,
        password: password ?? this.password,
        formKey: formKey ?? this.formKey,
        isLoading: isLoading ?? this.isLoading,
        isSuccess: isSuccess ?? this.isSuccess
    );
  }

  get isValid => this.email != "" && this.password != "";

  @override
  List<Object> get props => [
   email,password,formKey,isLoading,isSuccess
  ];


}