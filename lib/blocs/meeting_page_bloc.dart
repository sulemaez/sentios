import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:senti/services/auth.dart';
import 'package:senti/services/http.dart';
import 'package:senti/utils/globals/date.dart';


class MeetingPageBloc extends Bloc<MeetingPageEvent,MeetingPageState>{

  @override
  // TODO: implement initialState
  MeetingPageState get initialState => MeetingPageState.initial();

  @override
  Stream<MeetingPageState> mapEventToState(MeetingPageEvent event)async* {
    if(event is ShowCalenderEvent){
      yield state.copyWith(showCalender: event.show);
    }else if(event is MonthChangedEvent){
      yield await _monthChangedGetMeetings(event.month,event.year);
    }else if(event is LoadDataEvent){
       yield await _fetchMeetings();
    }

  }

  Future<MeetingPageState> _monthChangedGetMeetings(month,year) async{
    MeetingPageState _state = state.copyWith(month: getMonthFromInt(month),year:year.toString());
    List<Map<String,dynamic>> list = await _getMonthMeetings(meetings: state.meetings,state: _state);
    return _state.copyWith(activeMeetings: list);
  }

  Future<MeetingPageState> _fetchMeetings() async{
    print("Fetching");
    MeetingPageState _state;
    try{
       Response response = await Http.doGet("$apiUrl/meeting/all/${Auth.user.groups[Auth.activeGroupIndex]["uuid"]}");

      if(response.statusCode == 200){
        _state = state.copyWith(meetings:response.data["meetings"]);
        _state = _state.copyWith(activeMeetings:  await _getMonthMeetings(meetings: response.data["meetings"],state: state));
      }

    }catch(e){
       print("Error getting");
    }
    if(_state != null) return _state.copyWith(loadData: false);
    return  state.copyWith(loadData: false);
  }

  Future < List<Map<String,dynamic>> > _getMonthMeetings({meetings,state}) async{
    meetings = meetings ?? state.activeMeetings;
    List<Map<String,dynamic>> meetingsActive = [];

    await meetings.forEach((meeting){
       DateTime date = DateTime.parse(meeting["date"]);
       if(date.year.toString() == state.year && getMonthFromInt(date.month) == state.month){
         meetingsActive.add(meeting);
       }
    });

    return meetingsActive;
  }

}


//state
class MeetingPageState extends Equatable{

  final List<dynamic> meetings;
  final String month;
  final String year;
  final List<dynamic> activeMeetings;
  final bool loadData;
  final bool showCalender;

  MeetingPageState({
    @required this.meetings,
    @required this.month,
    @required this.year,
    @required this.activeMeetings,
    @required this.loadData,
    @required this.showCalender
  });

  factory MeetingPageState.initial(){
    return MeetingPageState(
      year: DateTime.now().year.toString(),
      month: getMonthFromInt(DateTime.now().month),
      meetings: [],
      activeMeetings: [],
      loadData: true,
      showCalender: false
    );
  }

  MeetingPageState copyWith({
    List<dynamic> meetings,
    String month,
    String year,
    List<dynamic> activeMeetings,
    bool loadData,
    bool showCalender
   }){
    return MeetingPageState(
         activeMeetings: activeMeetings ?? this.activeMeetings,
         meetings: meetings ?? this.meetings,
         month:month ?? this.month,
         year:  year ?? this.year,
         loadData: loadData ?? this.loadData ,
         showCalender: showCalender ?? this.showCalender
      );
  }
  @override
  // TODO: implement props
  List<Object> get props => [meetings,month,year,activeMeetings,loadData,showCalender];

}


//EVENTS
class MeetingPageEvent extends Equatable{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class MonthChangedEvent extends MeetingPageEvent{

  final int month;
  final int year;

  MonthChangedEvent({this.month,this.year});
  @override
  // TODO: implement props
  List<Object> get props => [month];
}
class ShowCalenderEvent extends MeetingPageEvent{
  final bool show;

  ShowCalenderEvent(this.show);
  @override
  // TODO: implement props
  List<Object> get props => [show];
}

class LoadDataEvent extends MeetingPageEvent{

}